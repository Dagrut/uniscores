import { fixupConfigRules, fixupPluginRules } from '@eslint/compat';
import react from 'eslint-plugin-react';
import globals from 'globals';
import path from 'node:path';
import { fileURLToPath } from 'node:url';
import js from '@eslint/js';
import { FlatCompat } from '@eslint/eslintrc';

import prettierConfig from './prettier.config.js';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const compat = new FlatCompat({
	baseDirectory: __dirname,
	recommendedConfig: js.configs.recommended,
	allConfig: js.configs.all,
});

export default [
	...fixupConfigRules(
		compat.extends(
			'eslint:recommended',
			'plugin:react/recommended',
			'plugin:prettier/recommended',
			'plugin:react-hooks/recommended'
		)
	),
	{
		files: ['**/*.{js,jsx,mjs,cjs,ts,tsx}'],
		plugins: {
			react: fixupPluginRules(react),
		},

		languageOptions: {
			globals: {
				...globals.browser,
				...globals.node,
			},
			ecmaVersion: 'latest',
			sourceType: 'module',
			parserOptions: {
				ecmaFeatures: {
					jsx: true,
				},
			},
		},

		rules: {
			'prettier/prettier': ['error', prettierConfig],
			'react-hooks/rules-of-hooks': 'error',
			'react-hooks/exhaustive-deps': 'warn',
			'no-unused-vars': 'warn', // Add this line to warn about unused variables
			'react/jsx-uses-vars': 'error', // Add this line to prevent false positives with React components
			'react/jsx-uses-react': 'error', // Add this line if you're not using React 17+ with the new JSX transform
		},
	},
	{
		files: ['**/.eslintrc.{js,cjs}'],

		languageOptions: {
			globals: {
				...globals.node,
			},

			ecmaVersion: 5,
			sourceType: 'commonjs',
		},
	},
];
