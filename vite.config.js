import path from 'path';
import react from '@vitejs/plugin-react';

const SRC_DIR = path.resolve(__dirname, './client');
const PUBLIC_DIR = path.resolve(__dirname, './public');
const BUILD_DIR = path.resolve(__dirname, './www');

const { NODE_ENV } = process.env;

export default async () => {
	return {
		plugins: [react()],
		root: SRC_DIR,
		base: '/',
		publicDir: PUBLIC_DIR,
		build: {
			outDir: BUILD_DIR,
			assetsInlineLimit: 0,
			emptyOutDir: true,
			rollupOptions: {
				treeshake: false,
			},
			sourcemap: NODE_ENV === 'development',
		},
		resolve: {
			alias: {
				'@': SRC_DIR,
			},
		},
		server: {
			host: true,
		},
		envPrefix: 'CLI_UNISCORES_',
	};
};
