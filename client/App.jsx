/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { useMemo } from 'react';

import { App, View, LoginScreen } from 'framework7-react';

import AppRightPanel from './components/AppRightPanel';
import ComponentsStack from './components/ComponentsStack';
import ErrorBoundary from './components/ErrorBounary';
import LoginPage from './pages/LoginPage';
import SetUserPasswordPopup from './components/popups/SetUserPasswordPopup';
import { AppConfigContextProvider } from './contexts/AppConfigContext';
import { SessionContextProvider } from './contexts/SessionContext';

import routes from './js/routes';
import store from './js/store';
import ThemeColorSetter from './components/ThemeColorSetter';
import { UserProfileFieldsContextProvider } from './contexts/UserProfileFieldsContext';
import { useTranslation } from 'react-i18next';
import useFetch from './js/useFetch';
import { useOnMount } from './js/reactHooks';

const MyApp = () => {
	const { t } = useTranslation('common');

	// Framework7 Parameters
	const f7params = useMemo(
		() => ({
			name: 'Uniscores', // App name
			theme: 'auto', // Automatic theme detection
			colors: {
				primary: '#1482f4',
			},
			dialog: {
				buttonOk: t('app_config.dialog.button_ok'),
				buttonCancel: t('app_config.dialog.button_cancel'),
			},

			// App store
			store: store,
			// App routes
			routes: routes,
		}),
		[t]
	);

	const [, internalSetRtc] = useFetch();

	useOnMount(() => {
		if (import.meta.env.CLI_UNISCORES_UNRELIABLE_RTC === 'true') {
			internalSetRtc('/api/rtc/set', {
				method: 'POST',
				body: {
					rtcTimestamp: Date.now(),
				},
			});
		}
	});

	return (
		<App {...f7params}>
			<ErrorBoundary>
				<ComponentsStack
					components={[AppConfigContextProvider, SessionContextProvider, UserProfileFieldsContextProvider]}
				>
					<ThemeColorSetter />

					<AppRightPanel />

					<View browserHistory={true} browserHistorySeparator="" main className="safe-areas" url="/" />

					<SetUserPasswordPopup />

					<LoginScreen id="app-login-screen">
						<View>
							<LoginPage />
						</View>
					</LoginScreen>
				</ComponentsStack>
			</ErrorBoundary>
		</App>
	);
};
export default MyApp;
