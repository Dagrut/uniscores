/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { createContext, useCallback, useContext, useMemo, useState } from 'react';
import PropTypes from 'prop-types';

import useFetch from '../js/useFetch';
import { useOnChange, useOnTrue } from '../js/reactHooks';
import SessionContext from './SessionContext';

const defaultContext = {
	refresh: () => {},
	fields: {
		list: [],
		assoc: {},
	},
};

const UserProfileFieldsContext = createContext(defaultContext);

export function UserProfileFieldsContextProvider(props) {
	const { children } = props;
	const [fields, setFields] = useState([]);

	const sessionContext = useContext(SessionContext);

	const [userProfileFields, internalRefresh] = useFetch('/api/userProfileFields');

	const refresh = useCallback(() => internalRefresh(), [internalRefresh]);

	useOnTrue(userProfileFields.fulfilled, () => {
		setFields(userProfileFields.json);
	});

	useOnChange(sessionContext.connected, internalRefresh);

	const context = useMemo(() => {
		const list = fields;
		const assoc = fields.reduce((acc, field) => {
			acc[field.key] = {
				name: field.name,
				values: field.values.reduce((acc2, value) => {
					acc2[value.key] = value.name;
					return acc2;
				}, {}),
			};
			return acc;
		}, {});

		return {
			refresh,
			fields: {
				list,
				assoc,
			},
		};
	}, [fields, refresh]);

	return <UserProfileFieldsContext.Provider value={context}>{children}</UserProfileFieldsContext.Provider>;
}

UserProfileFieldsContextProvider.propTypes = {
	children: PropTypes.node.isRequired,
};

export default UserProfileFieldsContext;
