/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { createContext, useCallback, useRef, useState } from 'react';
import PropTypes from 'prop-types';

import useFetch from '../js/useFetch';
import { useOnChange, useOnMount, useOnTrue, useOnUnmount } from '../js/reactHooks';

const defaultContext = {
	appThemeColor: '#1482f4',
	compIsOpen: true,
	compMode: 'point_based',
	compName: 'Uniscores',
	displayResultsAfter: true,
	displayResultsDuring: true,
	skipZeroPoints: true,
	displayStats: true,
	endDate: null,
	startDate: null,
};

const AppConfigContext = createContext(defaultContext);

export function AppConfigContextProvider(props) {
	const { children } = props;
	const [context, setContext] = useState({ ...defaultContext, loading: true });
	const timeoutId = useRef(null);
	const intervalId = useRef(null);

	const [appConfig, internalReloadAppConfig] = useFetch('/api/app/config');

	const reloadAppConfig = useCallback(() => {
		internalReloadAppConfig();
	}, [internalReloadAppConfig]);

	useOnMount(() => {
		intervalId.current = setInterval(reloadAppConfig, 60 * 1000);
	});

	useOnUnmount(() => {
		if (timeoutId.current) {
			clearTimeout(timeoutId.current);
			timeoutId.current = null;
		}
		if (intervalId.current) {
			clearInterval(intervalId.current);
			intervalId.current = null;
		}
	});

	useOnChange(appConfig.json, () => {
		if (appConfig.fulfilled) {
			updateContext();
		}
	});

	useOnChange(
		context,
		() => {
			const dates = [context.startDate, context.endDate];
			const now = new Date();
			const closestNextDate = dates
				.filter((date) => date)
				.filter((date) => date.getTime() > now.getTime())
				.reduce((acc, date) => {
					if (!acc || date.getTime() < acc.getTime()) {
						return date;
					}
					return acc;
				}, null);

			if (closestNextDate) {
				if (timeoutId.current) {
					clearTimeout(timeoutId.current);
				}
				timeoutId.current = setTimeout(
					() => {
						timeoutId.current = null;
						updateContext();
					},
					1 + closestNextDate.getTime() - now.getTime()
				);
			}
		},
		false
	);

	useOnTrue(appConfig.rejected, () => {
		setTimeout(() => {
			reloadAppConfig();
		}, 1000);
	});

	const updateContext = useCallback(() => {
		const newContext = {
			...defaultContext,
			...(appConfig.json || { loading: true }),
		};

		const now = new Date();
		if (newContext.endDate) {
			newContext.endDate = new Date(newContext.endDate);
		}
		if (newContext.startDate) {
			newContext.startDate = new Date(newContext.startDate);
		}

		newContext.compIsStarted = true;
		if (newContext.startDate) {
			if (now < newContext.startDate.getTime()) {
				newContext.compIsStarted = false;
			}
		}
		if (newContext.endDate) {
			if (now >= newContext.endDate.getTime()) {
				newContext.compIsStarted = false;
			}
		}

		newContext.refresh = reloadAppConfig;

		setContext(newContext);
	}, [appConfig.json, reloadAppConfig]);

	return <AppConfigContext.Provider value={context}>{children}</AppConfigContext.Provider>;
}

AppConfigContextProvider.propTypes = {
	children: PropTypes.node.isRequired,
};

export default AppConfigContext;
