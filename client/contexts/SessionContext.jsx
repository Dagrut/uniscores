/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { createContext, useMemo, useRef } from 'react';
import PropTypes from 'prop-types';

import useFetch from '../js/useFetch';
import { useOnTrue } from '../js/reactHooks';

const defaultContext = {
	connected: false,
	loading: false,
};

const SessionContext = createContext(defaultContext);

export function SessionContextProvider(props) {
	const { children } = props;
	const doneCb = useRef(null);

	const [sessionStatus, checkSession] = useFetch('/api/auth/status');

	const context = useMemo(() => {
		const ret = { ...defaultContext };

		if (sessionStatus.pending || sessionStatus.init) {
			ret.loading = true;
		} else if (sessionStatus.fulfilled) {
			ret.connected = true;
			ret.user = sessionStatus.json.user;
			ret.isAdmin = sessionStatus.json.isAdmin;
		}

		ret.refresh = (cb = null) => {
			doneCb.current = cb;
			checkSession();
		};

		return ret;
	}, [checkSession, sessionStatus.fulfilled, sessionStatus.init, sessionStatus.json, sessionStatus.pending]);

	useOnTrue(sessionStatus.fulfilled || sessionStatus.rejected, () => {
		setTimeout(() => {
			if (doneCb.current) {
				doneCb.current(sessionStatus.fulfilled);
				doneCb.current = null;
			}
		}, 0);
	});

	return <SessionContext.Provider value={context}>{children}</SessionContext.Provider>;
}

SessionContextProvider.propTypes = {
	children: PropTypes.node.isRequired,
};

export default SessionContext;
