/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'framework7-react';

import './ErrorMessage.less';
import { useErrorTranslator } from '../js/reactHooks';

function ErrorMessage(props) {
	const { centered, error, text } = props;
	const errorTs = useErrorTranslator();

	const errorMessage = useMemo(() => {
		const displayStr = text || (error && error.message) || error;
		return errorTs(displayStr);
	}, [error, errorTs, text]);

	return (
		<div className={`ErrorMessage ${centered ? 'centered' : ''}`}>
			<Icon f7="exclamationmark_triangle" />
			&nbsp;
			{errorMessage}
		</div>
	);
}

ErrorMessage.propTypes = {
	centered: PropTypes.bool,
	error: PropTypes.object,
	text: PropTypes.string,
};

ErrorMessage.defaultProps = {
	centered: false,
	error: null,
	text: '',
};

export default ErrorMessage;
