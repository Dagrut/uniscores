import { Icon, Link, ListInput } from 'framework7-react';
import React, { useCallback, useState } from 'react';

import './PasswordInput.less';

function PasswordInput(props) {
	const [passwordVisible, setPasswordVisible] = useState(false);

	const toggleVisibility = useCallback(() => {
		setPasswordVisible((oldValue) => !oldValue);
	}, []);

	return (
		<ListInput {...props} type={passwordVisible ? 'text' : 'password'} className="uniscores-password-input">
			<Link className="password-eye-container" onClick={toggleVisibility} slot="content-end">
				{passwordVisible ? <Icon f7="eye" /> : <Icon f7="eye_slash" />}
			</Link>
		</ListInput>
	);
}

PasswordInput.displayName = 'f7-list-input';

export default PasswordInput;
