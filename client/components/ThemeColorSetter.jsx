/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import { useContext, useEffect } from 'react';
import AppConfigContext from '../contexts/AppConfigContext';
import { f7 } from 'framework7-react';

function ThemeColorSetter() {
	const { appThemeColor } = useContext(AppConfigContext);

	useEffect(() => {
		f7.setColorTheme(appThemeColor);
	}, [appThemeColor]);

	return false;
}

export default ThemeColorSetter;
