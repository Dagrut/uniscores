/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { useCallback, useContext } from 'react';
import { Button } from 'framework7-react';

import useFetch from '../js/useFetch';
import { useOnTrue } from '../js/reactHooks';
import SessionContext from '../contexts/SessionContext';
import { useTranslation } from 'react-i18next';

function LogoutButton(props) {
	const [logoutResponse, internalLogout] = useFetch();
	const { refresh } = useContext(SessionContext);
	const { t } = useTranslation('common');

	const logout = useCallback(() => {
		internalLogout('/api/auth/logout', { method: 'PUT' });
	}, [internalLogout]);

	useOnTrue(logoutResponse.fulfilled, refresh);

	return (
		<Button {...props} fill loading={logoutResponse.pending} onClick={logout}>
			{t('disconnect_button')}
		</Button>
	);
}

export default LogoutButton;
