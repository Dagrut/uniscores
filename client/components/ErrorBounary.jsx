/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React from 'react';
import PropTypes from 'prop-types';
import { Translation } from 'react-i18next';

/* Inspired from https://stackoverflow.com/a/56253797 */
class ErrorBoundary extends React.Component {
	static propTypes = {
		children: PropTypes.node.isRequired,
	};

	state = {
		error: false,
		info: false,
	};

	componentDidCatch(error, info) {
		console.error('Caught error :', error, info);

		this.setState({
			error,
			info,
		});
	}

	render() {
		const { children } = this.props;
		const { error } = this.state;

		if (error) {
			return (
				<>
					<h1>
						<Translation ns="common">{(t) => t('something_went_wrong')}</Translation>
					</h1>
					<p>{error.message}</p>
				</>
			);
		}

		return children;
	}
}

export default ErrorBoundary;
