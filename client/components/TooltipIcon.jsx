/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'framework7-react';
import Tooltip from './Tooltip';

function TooltipIcon(props) {
	const { text } = props;

	return <Tooltip component={Icon} f7="question_circle_fill" tooltipText={text} />;
}

TooltipIcon.propTypes = {
	text: PropTypes.string.isRequired,
};

export default TooltipIcon;
