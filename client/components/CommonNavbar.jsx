/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import { Link, NavRight, NavTitle, Navbar } from 'framework7-react';
import PropTypes from 'prop-types';
import React from 'react';

function CommonNavbar(props) {
	const { backLink, title, menu } = props;

	return (
		<Navbar backLink={backLink}>
			<NavTitle>{title}</NavTitle>
			{menu && (
				<NavRight>
					<Link iconIos="f7:menu" iconMd="material:menu" panelOpen="right" />
				</NavRight>
			)}
		</Navbar>
	);
}

CommonNavbar.propTypes = {
	title: PropTypes.string.isRequired,
	backLink: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
	menu: PropTypes.bool,
};

CommonNavbar.defaultProps = {
	backLink: true,
	menu: true,
};

export default CommonNavbar;
