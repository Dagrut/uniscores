/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import {
	Block,
	BlockTitle,
	Button,
	List,
	ListInput,
	ListItem,
	Navbar,
	Page,
	Panel,
	Preloader,
	View,
} from 'framework7-react';
import React, { useContext } from 'react';
import { Trans, useTranslation } from 'react-i18next';

import SessionContext from '../contexts/SessionContext';
import LogoutButton from './LogoutButton';
import { languages } from '../i18n/config';
import PropTypes from 'prop-types';

import { version } from '../../package.json';

function AppRightPanelWrapper({ children }) {
	const { i18n, t } = useTranslation(['AppRightPanel', 'common']);

	return (
		<Panel right reveal dark>
			<View>
				<Page>
					<Navbar title={t('panel_title')} />
					{children}
					<BlockTitle>{t('change_language')}</BlockTitle>
					<List>
						<ListInput
							label={t('language')}
							type="select"
							value={i18n.language}
							onChange={(e) => i18n.changeLanguage(e.target.value)}
						>
							{languages.map((language) => (
								<option key={`AppRightPanel-language-${language}`} value={language}>
									{t(`languages.${language}`, { ns: 'common' })}
								</option>
							))}
						</ListInput>
					</List>

					<div
						style={{
							position: 'absolute',
							bottom: '10px',
							right: '10px',
							fontSize: '0.9em',
						}}
					>
						{version}
					</div>
				</Page>
			</View>
		</Panel>
	);
}

AppRightPanelWrapper.propTypes = {
	children: PropTypes.node.isRequired,
};

function AppRightPanel() {
	const { isAdmin, connected, loading, user } = useContext(SessionContext);
	const { t } = useTranslation('AppRightPanel');

	if (loading) {
		return (
			<AppRightPanelWrapper>
				<Preloader />
			</AppRightPanelWrapper>
		);
	}

	if (!connected) {
		return (
			<AppRightPanelWrapper>
				<Block>
					<Button fill panelClose loginScreenOpen="#app-login-screen">
						{t('login_button')}
					</Button>
				</Block>
			</AppRightPanelWrapper>
		);
	}

	return (
		<AppRightPanelWrapper>
			{isAdmin && <Block>{t('connected_as_administrator')}</Block>}
			{!isAdmin && (
				<>
					<BlockTitle>{t('connected_as')}</BlockTitle>
					<List>
						<ListItem>
							<span>
								<Trans ns="AppRightPanel" i18nKey="firstname_xxx" values={{ firstname: user.profile.firstname }}>
									Firstname: <b>{user.profile.firstname}</b>
								</Trans>
							</span>
						</ListItem>
						<ListItem>
							<span>
								<Trans ns="AppRightPanel" i18nKey="lastname_xxx" values={{ lastname: user.profile.lastname }}>
									Lastname: <b>{user.profile.lastname}</b>
								</Trans>
							</span>
						</ListItem>
					</List>
				</>
			)}
			<Block>
				<LogoutButton />
			</Block>
		</AppRightPanelWrapper>
	);
}

export default AppRightPanel;
