/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { useOnMount, useOnUnmount } from '../js/reactHooks';
import { f7 } from 'framework7-react';

function Tooltip(props) {
	const { children, component: Component, tooltipText, tooltipExtraParams, ...extraProps } = props;

	const targetRef = useRef(null);
	const tooltipRef = useRef(null);

	useOnMount(() => {
		tooltipRef.current = f7.tooltip.create({
			targetEl: targetRef.current.el || targetRef.current,
			text: tooltipText,
			...tooltipExtraParams,
		});
	});

	useOnUnmount(() => {
		if (tooltipRef.current) {
			tooltipRef.current.destroy();
			tooltipRef.current = null;
		}
	});

	useEffect(() => {
		if (tooltipRef.current && targetRef.current) {
			tooltipRef.current.setText(tooltipText);
		}
	}, [tooltipText]);

	return (
		<Component ref={targetRef} {...extraProps}>
			{children}
		</Component>
	);
}

Tooltip.propTypes = {
	children: PropTypes.node,
	component: PropTypes.elementType,
	tooltipText: PropTypes.string.isRequired,
	tooltipExtraParams: PropTypes.object,
};

Tooltip.defaultProps = {
	children: null,
	component: 'span',
	tooltipExtraParams: {},
};

export default Tooltip;
