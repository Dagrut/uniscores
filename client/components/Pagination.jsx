/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Icon, Stepper } from 'framework7-react';

import './Pagination.less';

export function usePaginationState(count, { perPage: defaultPerPage = 50, page: defaultPage = 0 } = {}) {
	const [perPage, setPerPage] = useState(defaultPerPage);
	const [page, setPage] = useState(defaultPage);

	const offset = useMemo(() => page * perPage, [page, perPage]);

	useEffect(() => {
		if (offset + 1 >= count && count > 0) {
			setPage(Math.floor((count - 1) / perPage));
		}
	}, [count, offset, perPage]);

	const ret = useMemo(
		() => ({
			count,
			offset,
			page,
			perPage,
			setPerPage,
			setPage,
		}),
		[count, offset, page, perPage]
	);

	return ret;
}

function Pagination(props) {
	const {
		state: { count, offset, page, perPage, setPerPage, setPage },
		max,
		min,
		step,
	} = props;

	const prevPage = useCallback(() => {
		setPage((x) => x - 1);
	}, [setPage]);

	const nextPage = useCallback(() => {
		setPage((x) => x + 1);
	}, [setPage]);

	const pageCount = useMemo(() => (count === 0 ? 0 : 1 + Math.floor((count - 1) / perPage)), [count, perPage]);

	const lastItemNum = useMemo(() => offset + perPage, [offset, perPage]);

	const thisPageItemsCount = useMemo(() => (lastItemNum >= count ? count : lastItemNum), [count, lastItemNum]);

	return (
		<div className="data-table-footer">
			<div className="data-table-rows-select">
				<Stepper fill small wraps value={perPage} min={min} max={max} step={step} onStepperChange={setPerPage} />
				&nbsp; par page
			</div>
			<div className="data-table-pagination">
				<span className="data-table-pagination-label">{`${offset + 1}-${thisPageItemsCount} sur ${pageCount}`}</span>
				<Button disabled={page <= 0} onClick={prevPage}>
					<Icon f7="chevron_left" />
				</Button>
				<Button disabled={lastItemNum >= count} onClick={nextPage}>
					<Icon f7="chevron_right" />
				</Button>
			</div>
		</div>
	);
}

Pagination.propTypes = {
	state: PropTypes.shape({
		count: PropTypes.number.isRequired,
		page: PropTypes.number.isRequired,
		perPage: PropTypes.number.isRequired,
		offset: PropTypes.number.isRequired,
		setPerPage: PropTypes.func.isRequired,
		setPage: PropTypes.func.isRequired,
	}).isRequired,
	min: PropTypes.number,
	max: PropTypes.number,
	step: PropTypes.number,
};

Pagination.defaultProps = {
	min: 10,
	max: 500,
	step: 10,
};

export default Pagination;
