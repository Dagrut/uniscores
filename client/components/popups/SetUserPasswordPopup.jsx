/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { useCallback, useContext, useState } from 'react';
import { Block, BlockFooter, Button, List, ListInput, ListItem, Navbar, Page, Popup, f7 } from 'framework7-react';

import useFetch from '../../js/useFetch';
import SessionContext from '../../contexts/SessionContext';
import { useOnTrue } from '../../js/reactHooks';
import ErrorMessage from '../ErrorMessage';
import LogoutButton from '../LogoutButton';
import { Trans, useTranslation } from 'react-i18next';

function SetUserPasswordPopup() {
	const [password, setPassword] = useState('');
	const [password2, setPassword2] = useState('');
	const [opened, setOpened] = useState(false);
	const { connected, isAdmin, user, refresh } = useContext(SessionContext);
	const { t } = useTranslation('SetUserPasswordPopup');

	const [passwordSaveResp, internalSavePassword] = useFetch();

	const savePassword = useCallback(() => {
		if (password === password2) {
			internalSavePassword('/api/user/setPassword', { method: 'PUT', body: { password } });
		}
	}, [internalSavePassword, password, password2]);

	useOnTrue(connected && !isAdmin && user && !user.password, () => {
		if (f7.store.state.password) {
			setPassword(f7.store.state.password);
			f7.store.dispatch('unsetPassword');
		}
		setOpened(true);
	});

	useOnTrue(connected && !isAdmin && user && user.password, () => {
		setOpened(false);
		f7.store.dispatch('unsetPassword');
	});

	useOnTrue(!connected && opened, () => {
		setOpened(false);
		f7.store.dispatch('unsetPassword');
	});

	useOnTrue(passwordSaveResp.fulfilled, () => {
		f7.dialog.preloader(t('reloading'));
		refresh(() => {
			f7.dialog.close();
		});
	});

	return (
		<Popup closeOnEscape={false} closeByBackdropClick={false} opened={opened}>
			<Page>
				<Navbar title={t('navbar_title')} />
				{passwordSaveResp.rejected && (
					<Block>
						<ErrorMessage centered error={passwordSaveResp.error} />
					</Block>
				)}
				<Block strongIos outlineIos>
					<p>
						<Trans
							ns="SetUserPasswordPopup"
							i18nKey="connected_as"
							values={{ username: (connected && !isAdmin && user && user.username) || '' }}
						>
							{'Connected as '}
							<b>{connected && !isAdmin && user && user.username}</b>
						</Trans>
					</p>
					<p>{t('explanation_text')}</p>
					<List form strongIos dividersIos>
						<ListInput
							label={t('form.password.label')}
							disabled={passwordSaveResp.pending}
							type="password"
							placeholder={t('form.password.placeholder')}
							value={password}
							onChange={(e) => setPassword(e.target.value)}
						/>
						<ListInput
							label={t('form.password_confirm.label')}
							disabled={passwordSaveResp.pending}
							type="password"
							placeholder={t('form.password_confirm.placeholder')}
							value={password2}
							onChange={(e) => setPassword2(e.target.value)}
							errorMessageForce={password !== password2}
							errorMessage={t('form.passwords_are_different')}
						/>
						<ListItem>
							<Button fill disabled={passwordSaveResp.pending || password !== password2} onClick={savePassword}>
								{t('form.save_button')}
							</Button>
						</ListItem>
					</List>
				</Block>
				<BlockFooter>
					<List>
						<ListItem>{t('form.wrong_user_disconnect')}</ListItem>
						<ListItem>
							<LogoutButton />
						</ListItem>
					</List>
				</BlockFooter>
			</Page>
		</Popup>
	);
}

export default SetUserPasswordPopup;
