/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { useCallback, useMemo, useState } from 'react';
import { Link, List, ListButton, ListItem, NavRight, Navbar, Page, Popup } from 'framework7-react';
import PropTypes from 'prop-types';
import { useOnChange } from '../../js/reactHooks';
import { Trans, useTranslation } from 'react-i18next';

export function useResultsGroupGenerationPopupState() {
	const [opened, setOpened] = useState(false);

	const openPopup = useCallback(() => {
		setOpened(true);
	}, []);

	const closePopup = useCallback(() => {
		setOpened(false);
	}, []);

	const state = useMemo(
		() => ({
			opened,
			openPopup,
			closePopup,
		}),
		[closePopup, openPopup, opened]
	);

	return state;
}

function ResultsGroupsGenerationPopupButton(props) {
	const {
		fields,
		onSubmit,
		state: { closePopup, opened },
	} = props;
	const [formState, setFormState] = useState([]);
	const { t } = useTranslation('AdminResultsGroupsPage');

	useOnChange(fields, () => {
		setFormState(fields.map((field) => ({ ...field, checked: true })));
	});

	const generate = useCallback(() => {
		const outputFields = formState.filter(({ checked }) => checked).map(({ id }) => id);
		onSubmit(outputFields);
		closePopup();
	}, [closePopup, formState, onSubmit]);

	return (
		<>
			<Popup closeOnEscape={false} opened={opened} onPopupClosed={closePopup}>
				<Page>
					<Navbar title={t('generation_popup.popup_title')}>
						<NavRight>
							<Link onClick={closePopup}>{t('generation_popup.close_button')}</Link>
						</NavRight>
					</Navbar>
					<List>
						<ListItem>
							<Trans ns="AdminResultsGroupsPage" i18nKey="generation_popup.help_text">
								Sélectionnez les champs à utiliser pour la génération des nouveaux groupes.
								<br />
								Les groupes ayant les mêmes paramètres ne seront pas recréés.
							</Trans>
						</ListItem>
						{formState.map(({ id, name, checked }, arrayId) => (
							<ListItem
								key={`ResultsGroupsGenerationPopupButton-item-${id}`}
								checkbox
								checked={checked}
								title={name}
								onChange={(e) => {
									setFormState((oldFormState) => {
										const newFormState = [...oldFormState];
										newFormState[arrayId].checked = e.target.checked;
										return newFormState;
									});
								}}
							/>
						))}
						<ListButton onClick={generate}>{t('generation_popup.generate_button')}</ListButton>
					</List>
				</Page>
			</Popup>
		</>
	);
}

ResultsGroupsGenerationPopupButton.propTypes = {
	fields: PropTypes.arrayOf(
		PropTypes.shape({
			id: PropTypes.string,
			name: PropTypes.string,
		})
	).isRequired,
	onSubmit: PropTypes.func.isRequired,
	state: PropTypes.shape({
		opened: PropTypes.bool,
		setOpened: PropTypes.func,
		openPopup: PropTypes.func,
		closePopup: PropTypes.func,
	}).isRequired,
};

export default ResultsGroupsGenerationPopupButton;
