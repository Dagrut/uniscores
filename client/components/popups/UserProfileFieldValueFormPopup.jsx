/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import {
	Page,
	List,
	Button,
	Block,
	ListInput,
	Link,
	NavRight,
	Icon,
	NavTitle,
	NavLeft,
	Navbar,
	Popup,
	ListItem,
} from 'framework7-react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { strRemoveDiacritics } from '../../js/utils';

export function useUserProfileFieldValueFormPopupState() {
	const [opened, setOpened] = useState(false);
	const [initialValues, setInitialValues] = useState(null);
	const [fieldData, setFieldData] = useState(null);

	const open = useCallback((newValues, newFieldData) => {
		setOpened(true);
		setInitialValues(newValues || null);
		setFieldData(newFieldData);
	}, []);

	const close = useCallback(() => {
		setOpened(false);
	}, []);

	return {
		close,
		initialValues,
		fieldData,
		open,
		opened,
	};
}

function UserProfileFieldValueFormPopup(props) {
	const { state, onSubmit } = props;

	const { fieldData } = state;

	const [arrayId, setArrayId] = useState(-1);
	const [name, setName] = useState('');
	const [key, setKey] = useState('');

	const { t } = useTranslation(['UserProfileFieldValueFormPopup', 'common']);

	useEffect(() => {
		if (state.initialValues) {
			setArrayId(state.initialValues.arrayId);
			setName(state.initialValues.name);
			setKey(state.initialValues.key);
		} else {
			setArrayId(-1);
			setName('');
			setKey('');
		}
	}, [state.initialValues, state.opened]);

	const editingField = useMemo(() => state.initialValues, [state.initialValues]);

	const closePopup = useCallback(() => {
		state.close();
	}, [state]);

	const saveField = useCallback(() => {
		const autoKey = strRemoveDiacritics(name)
			.replace(/[^a-zA-Z0-9_-]/g, '-')
			.toLowerCase();
		const toSave = {
			arrayId,
			fieldData,
			name,
			key: key || autoKey,
		};
		onSubmit(toSave);
	}, [name, arrayId, fieldData, key, onSubmit]);

	const navTitle = useMemo(() => {
		if (fieldData) {
			if (editingField) {
				return t('navbar.title.edit_val');
			} else {
				return t('navbar.title.create_val');
			}
		} else {
			if (editingField) {
				return t('navbar.title.edit_key');
			} else {
				return t('navbar.title.create_key');
			}
		}
	}, [editingField, fieldData, t]);

	return (
		<Popup onPopupClosed={closePopup} opened={state.opened}>
			<Page>
				<Navbar>
					<NavLeft>
						<Button onClick={saveField}>
							{editingField ? t('navbar.save_button.edit') : t('navbar.save_button.create')}
						</Button>
					</NavLeft>
					<NavTitle>{navTitle}</NavTitle>
					<NavRight>
						<Link popupClose>
							<Icon f7="xmark" />
						</Link>
					</NavRight>
				</Navbar>

				<Block>
					<List
						form
						strongIos
						outlineIos
						dividersIos
						onSubmit={(e) => {
							e.preventDefault();
							saveField();
						}}
					>
						<ListInput
							label={t('form.name.label')}
							type="text"
							placeholder={t('form.name.placeholder')}
							value={name}
							onChange={(e) => setName(e.target.value)}
							validate
							required
						/>
						<ListInput
							label={t('form.key.label')}
							type="text"
							placeholder={t('form.key.placeholder')}
							value={key}
							onChange={(e) => setKey(e.target.value)}
							validate
						/>
						<ListItem style={{ display: 'none' }}>
							<Button type="submit">Hidden button, to allow onSubmit on enter keypress</Button>
						</ListItem>
					</List>
				</Block>
			</Page>
		</Popup>
	);
}

UserProfileFieldValueFormPopup.propTypes = {
	state: PropTypes.shape({
		close: PropTypes.func.isRequired,
		open: PropTypes.func.isRequired,
		opened: PropTypes.bool.isRequired,
		fieldData: PropTypes.shape({
			arrayId: PropTypes.number,
			name: PropTypes.string,
			key: PropTypes.string,
		}),
		initialValues: PropTypes.shape({
			arrayId: PropTypes.number,
			name: PropTypes.string,
			key: PropTypes.string,
		}),
	}).isRequired,
	onSubmit: PropTypes.func,
};

UserProfileFieldValueFormPopup.defaultProps = {
	onSubmit: () => {},
};

export default UserProfileFieldValueFormPopup;
