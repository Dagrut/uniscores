/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import {
	Block,
	Button,
	Icon,
	Link,
	List,
	ListInput,
	ListItem,
	NavLeft,
	NavRight,
	NavTitle,
	Navbar,
	Page,
	Popup,
} from 'framework7-react';
import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useMemo, useState } from 'react';

import './DisciplineFormPopup.less';
import { useTranslation } from 'react-i18next';

export function useDisciplineFormPopupState() {
	const [opened, setOpened] = useState(false);
	const [initialValues, setInitialValues] = useState(null);

	const open = useCallback((newValues) => {
		setOpened(true);
		setInitialValues(newValues || null);
	}, []);

	const close = useCallback(() => {
		setOpened(false);
	}, []);

	return {
		close,
		initialValues,
		open,
		opened,
	};
}

function DisciplineFormPopup(props) {
	const { state, onSubmit, onDelete } = props;

	const [name, setName] = useState('');
	const { t } = useTranslation('DisciplineFormPopup');

	useEffect(() => {
		if (state.initialValues) {
			setName(state.initialValues.name);
		} else {
			setName('');
		}
	}, [state.initialValues, state.opened]);

	const editingDiscipline = useMemo(() => state.initialValues, [state.initialValues]);

	const closePopup = useCallback(() => {
		state.close();
	}, [state]);

	const saveDiscipline = useCallback(() => {
		const toSave = {
			name,
		};
		if (editingDiscipline) {
			toSave.id = editingDiscipline.id;
		}
		onSubmit(toSave);
	}, [editingDiscipline, name, onSubmit]);

	const doDelete = useCallback(() => {
		if (!editingDiscipline) return;
		onDelete(editingDiscipline.id);
		state.close();
	}, [editingDiscipline, onDelete, state]);

	return (
		<Popup className="DisciplineFormPopup" onPopupClosed={closePopup} opened={state.opened}>
			<Page>
				<Navbar>
					<NavLeft>
						<Button onClick={saveDiscipline}>{editingDiscipline ? t('save_button.edit') : t('save_button.add')}</Button>
					</NavLeft>
					<NavTitle>
						{editingDiscipline ? t('popup_title.edit', { name: editingDiscipline.name }) : t('popup_title.add')}
					</NavTitle>
					<NavRight>
						<Link popupClose>
							<Icon f7="xmark" />
						</Link>
					</NavRight>
				</Navbar>

				<List
					form
					strongIos
					outlineIos
					dividersIos
					onSubmit={(e) => {
						e.preventDefault();
						saveDiscipline();
					}}
				>
					<ListInput
						label={t('form.name.label')}
						type="text"
						placeholder={t('form.name.placeholder')}
						value={name}
						onChange={(e) => setName(e.target.value)}
					/>
				</List>
				{editingDiscipline && (
					<Block>
						<Button onClick={doDelete}>{t('form.delete_button')}</Button>
					</Block>
				)}
				<ListItem style={{ display: 'none' }}>
					<Button type="submit">Hidden button, to allow onSubmit on enter keypress</Button>
				</ListItem>
			</Page>
		</Popup>
	);
}

DisciplineFormPopup.propTypes = {
	state: PropTypes.shape({
		close: PropTypes.func.isRequired,
		open: PropTypes.func.isRequired,
		opened: PropTypes.bool.isRequired,
		initialValues: PropTypes.shape({
			id: PropTypes.string,
			name: PropTypes.string,
			points: PropTypes.object,
		}),
	}).isRequired,
	onDelete: PropTypes.func,
	onSubmit: PropTypes.func,
};

DisciplineFormPopup.defaultProps = {
	onDelete: () => {},
	onSubmit: () => {},
};

export default DisciplineFormPopup;
