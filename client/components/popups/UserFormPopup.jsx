/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { useCallback, useContext, useEffect, useMemo, useState } from 'react';
import {
	Page,
	List,
	ListItem,
	Button,
	Block,
	ListInput,
	Radio,
	Preloader,
	Link,
	NavRight,
	Icon,
	NavTitle,
	NavLeft,
	Navbar,
	Popup,
} from 'framework7-react';
import PropTypes from 'prop-types';

import useFetch from '../../js/useFetch';
import ErrorMessage from '../ErrorMessage';

import { useOnTrue } from '../../js/reactHooks';
import UserProfileFieldsContext from '../../contexts/UserProfileFieldsContext';
import { useTranslation } from 'react-i18next';

export function useUserFormPopupState() {
	const [opened, setOpened] = useState(false);
	const [initialValues, setInitialValues] = useState(null);

	const open = useCallback((newValues) => {
		setOpened(true);
		setInitialValues(newValues || null);
	}, []);

	const close = useCallback(() => {
		setOpened(false);
	}, []);

	return {
		close,
		initialValues,
		open,
		opened,
	};
}

function UserFormPopup(props) {
	const { state, onSubmit, onPasswordReset } = props;

	const [firstname, setFirstname] = useState('');
	const [lastname, setLastname] = useState('');
	const [password, setPassword] = useState('');
	const [wasPasswordReset, setWasPasswordReset] = useState(false);
	const [extraFields, setExtraFields] = useState({});
	const { t } = useTranslation('UserFormPopup');

	const extraProfileFields = useContext(UserProfileFieldsContext);

	const [resetPasswordResp, internalResetPassword] = useFetch();

	useEffect(() => {
		if (state.initialValues) {
			setFirstname(state.initialValues.profile.firstname);
			setLastname(state.initialValues.profile.lastname);
			setExtraFields(
				extraProfileFields.fields.list.reduce((acc, field) => {
					acc[field.key] = state.initialValues.profile[field.key];
					return acc;
				}, {})
			);
		} else {
			setFirstname('');
			setLastname('');
			setExtraFields(
				extraProfileFields.fields.list.reduce((acc, field) => {
					acc[field.key] = field.values[0].key;
					return acc;
				}, {})
			);
		}
		setPassword('');
		setWasPasswordReset(false);
	}, [extraProfileFields.fields.list, state.initialValues, state.opened]);

	const editingUser = useMemo(() => state.initialValues, [state.initialValues]);

	const anyPending = useMemo(() => resetPasswordResp.pending, [resetPasswordResp.pending]);

	useOnTrue(resetPasswordResp.fulfilled || resetPasswordResp.rejected, () => {
		if (resetPasswordResp.fulfilled) {
			onPasswordReset(editingUser.id);
		}
		setWasPasswordReset(true);
	});

	const resetPassword = useCallback(() => {
		if (!editingUser) return;
		internalResetPassword(`/api/users/${editingUser.id}/resetPassword`, {
			method: 'PUT',
		});
	}, [editingUser, internalResetPassword]);

	const closePopup = useCallback(() => {
		state.close();
	}, [state]);

	const saveUser = useCallback(() => {
		const toSave = {
			password,
			profile: {
				...extraFields,
				firstname,
				lastname,
			},
		};
		if (editingUser) {
			toSave.id = editingUser.id;
		}
		onSubmit(toSave);
	}, [editingUser, extraFields, firstname, lastname, onSubmit, password]);

	return (
		<Popup onPopupClosed={closePopup} opened={state.opened}>
			<Page>
				<Navbar>
					<NavLeft>
						<Button onClick={saveUser}>
							{editingUser ? t('navbar.save_button.edit') : t('navbar.save_button.create')}
						</Button>
					</NavLeft>
					<NavTitle>
						{editingUser
							? t('navbar.title.edit', {
									firstname: editingUser.profile.firstname,
									lastname: editingUser.profile.lastname,
								})
							: t('navbar.title.create')}
					</NavTitle>
					<NavRight>
						<Link popupClose>
							<Icon f7="xmark" />
						</Link>
					</NavRight>
				</Navbar>

				<Block>
					<List
						form
						strongIos
						outlineIos
						dividersIos
						onSubmit={(e) => {
							e.preventDefault();
							saveUser();
						}}
					>
						<ListInput
							label={t('form.lastname.label')}
							disabled={anyPending}
							type="text"
							placeholder={t('form.lastname.placeholder')}
							value={lastname}
							onChange={(e) => setLastname(e.target.value)}
						/>
						<ListInput
							label={t('form.firstname.label')}
							disabled={anyPending}
							type="text"
							placeholder={t('form.firstname.placeholder')}
							value={firstname}
							onChange={(e) => setFirstname(e.target.value)}
						/>
						{extraProfileFields.fields.list.map((field) => (
							<ListInput
								key={`UserFormPopup-${field.key}-input`}
								label={field.name}
								disabled={anyPending}
								type="text"
								input={false}
							>
								<div slot="input" className="grid grid-gap grid-cols-1 small-grid-cols-2">
									{field.values.map((value) => (
										<label key={`UserFormPopup-${field.key}-radio-${value.key}`}>
											<span>{`${value.name} `}</span>
											<Radio
												name={field.key}
												disabled={anyPending}
												value={value.key}
												checked={extraFields[field.key] === value.key}
												onChange={(e) =>
													setExtraFields((oldExtraFields) => {
														const newExtraFields = { ...oldExtraFields };
														newExtraFields[field.key] = e.target.value;
														return newExtraFields;
													})
												}
											/>
										</label>
									))}
								</div>
							</ListInput>
						))}
						<ListItem>
							<Icon slot="media" f7="question_circle_fill" />
							{t('form.password.help_text')}
						</ListItem>
						<ListInput
							label={t('form.password.label')}
							disabled={anyPending}
							type="password"
							placeholder={t('form.password.placeholder')}
							value={password}
							onChange={(e) => setPassword(e.target.value)}
						/>
						<ListItem>
							{wasPasswordReset && resetPasswordResp.rejected && <ErrorMessage error={resetPasswordResp.error} />}
							{editingUser && !wasPasswordReset && editingUser.password === true && (
								<Button fill onClick={resetPassword} disabled={resetPassword.pending}>
									{t('form.password_erase.button')}
									{resetPassword.pending && <Preloader />}
								</Button>
							)}
							{wasPasswordReset && <span>{t('form.password_erase.success')}</span>}
						</ListItem>
						<ListItem style={{ display: 'none' }}>
							<Button type="submit">Hidden button, to allow onSubmit on enter keypress</Button>
						</ListItem>
					</List>
				</Block>
			</Page>
		</Popup>
	);
}

UserFormPopup.propTypes = {
	state: PropTypes.shape({
		close: PropTypes.func.isRequired,
		open: PropTypes.func.isRequired,
		opened: PropTypes.bool.isRequired,
		initialValues: PropTypes.shape({
			id: PropTypes.string,
			profile: PropTypes.shape({
				firstname: PropTypes.string,
				lastname: PropTypes.string,
			}),
		}),
	}).isRequired,
	onSubmit: PropTypes.func,
	onPasswordReset: PropTypes.func,
};

UserFormPopup.defaultProps = {
	onSubmit: () => {},
	onPasswordReset: () => {},
};

export default UserFormPopup;
