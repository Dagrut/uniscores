/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { useCallback, useMemo, useState } from 'react';
import {
	Icon,
	Link,
	ListButton,
	ListInput,
	ListItem,
	NavRight,
	NavTitle,
	Navbar,
	Popup,
	Preloader,
	f7,
	Block,
	List,
	Page,
} from 'framework7-react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

import useFetch from '../../js/useFetch';
import ErrorMessage from '../ErrorMessage';
import { useOnChange, useOnTrue } from '../../js/reactHooks';

export function useUserProfileFieldApplyPopupState() {
	const [opened, setOpened] = useState(false);
	const [fields, setFields] = useState([]);

	const open = useCallback((newFields) => {
		setOpened(true);
		setFields(newFields);
	}, []);

	const close = useCallback(() => {
		setOpened(false);
	}, []);

	return {
		close,
		fields,
		open,
		opened,
	};
}

function UserProfileFieldApplyPopup(props) {
	const { state } = props;

	const { fields } = state;

	const [currentField, setCurrentField] = useState(null);
	const [currentValue, setCurrentValue] = useState(null);

	const [applyFieldResponse, internalApplyField] = useFetch();

	const { t } = useTranslation(['UserProfileFieldApplyPopup']);

	const currentValues = useMemo(() => {
		const field = fields.find(({ key }) => key == currentField) || {};
		const values = field.values || [];
		return values;
	}, [currentField, fields]);

	useOnChange(fields && fields[0], () => {
		if (fields[0]) {
			const firstField = fields[0] || {};
			setCurrentField(typeof firstField.key === 'string' ? firstField.key : null);
		}
	});

	useOnChange(currentValues && currentValues[0], () => {
		if (currentValues[0]) {
			const firstVal = currentValues[0] || {};
			setCurrentValue(typeof firstVal.key === 'string' ? firstVal.key : null);
		}
	});

	const applyField = useCallback(
		(undefinedOnly, notIn = []) => {
			internalApplyField('/api/userProfileFields/applyField', {
				method: 'PUT',
				body: {
					field: currentField,
					value: currentValue,
					undefinedOnly,
					notIn,
				},
			});
		},
		[currentField, currentValue, internalApplyField]
	);

	const applyFieldAll = useCallback(() => {
		f7.dialog.confirm(t('apply_confirm_dialog'), () => {
			applyField(false, []);
		});
	}, [applyField, t]);

	const applyFieldInvalid = useCallback(() => {
		f7.dialog.confirm(t('apply_confirm_dialog'), () => {
			const goodValues = currentValues.map(({ key }) => key);
			applyField(false, goodValues);
		});
	}, [applyField, currentValues, t]);

	const applyFieldUndefined = useCallback(() => {
		f7.dialog.confirm(t('apply_confirm_dialog'), () => {
			applyField(true);
		});
	}, [applyField, t]);

	useOnTrue(applyFieldResponse.fulfilled, () => {
		f7.dialog.alert(t('toast_applied_field_text'), () => {
			window.location.reload();
		});
	});

	const closePopup = useCallback(() => {
		if (!applyFieldResponse.pending) {
			state.close();
		}
	}, [applyFieldResponse.pending, state]);

	return (
		<Popup onPopupClosed={closePopup} opened={state.opened}>
			<Page>
				<Navbar>
					<NavTitle>{t('navbar.title')}</NavTitle>
					<NavRight>
						<Link popupClose>
							<Icon f7="xmark" />
						</Link>
					</NavRight>
				</Navbar>

				<Block>
					<List strongIos outlineIos dividersIos>
						<ListInput
							label={t('form.field.label')}
							type="select"
							value={currentField || ''}
							onChange={(e) => setCurrentField(e.target.value)}
							validate
							required
						>
							{fields.map((field) => (
								<option key={`AppRightPanel-language-${field.key}`} value={field.key}>
									{field.name}
								</option>
							))}
						</ListInput>
						<ListInput
							label={t('form.value.label')}
							type="select"
							value={currentValue || ''}
							onChange={(e) => setCurrentValue(e.target.value)}
							validate
							required
						>
							{currentValues.map((value) => (
								<option key={`AppRightPanel-language-${value.key}`} value={value.key}>
									{value.name}
								</option>
							))}
						</ListInput>
						{applyFieldResponse.rejected && (
							<Block>
								<ErrorMessage error={applyFieldResponse.error} />
							</Block>
						)}
						{applyFieldResponse.pending ? (
							<Preloader />
						) : (
							[
								<ListItem key="UserProfileFieldApplyPopup-apply-undefined">
									{t('form.buttons.apply_field_undefined')}
								</ListItem>,
								<ListButton
									key="UserProfileFieldApplyPopup-apply-undefined-btn"
									color="green"
									onClick={applyFieldUndefined}
								>
									<Icon f7="floppy_disk" />
								</ListButton>,
								<ListItem key="UserProfileFieldApplyPopup-apply-invalid">
									{t('form.buttons.apply_field_invalid')}
								</ListItem>,
								<ListButton
									key="UserProfileFieldApplyPopup-apply-invalid-btn"
									color="orange"
									onClick={applyFieldInvalid}
								>
									<Icon f7="floppy_disk" />
								</ListButton>,
								<ListItem key="UserProfileFieldApplyPopup-apply-all">{t('form.buttons.apply_field_all')}</ListItem>,
								<ListButton key="UserProfileFieldApplyPopup-apply-all-btn" color="red" onClick={applyFieldAll}>
									<Icon f7="floppy_disk" />
								</ListButton>,
							]
						)}
					</List>
				</Block>
			</Page>
		</Popup>
	);
}

UserProfileFieldApplyPopup.propTypes = {
	state: PropTypes.shape({
		close: PropTypes.func.isRequired,
		open: PropTypes.func.isRequired,
		opened: PropTypes.bool.isRequired,
		fields: PropTypes.arrayOf(
			PropTypes.shape({
				name: PropTypes.string,
				key: PropTypes.string,
				values: PropTypes.arrayOf(
					PropTypes.shape({
						name: PropTypes.string,
						key: PropTypes.string,
					})
				),
			})
		),
	}).isRequired,
};

UserProfileFieldApplyPopup.defaultProps = {};

export default UserProfileFieldApplyPopup;
