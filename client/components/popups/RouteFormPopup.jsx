/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import {
	Button,
	Chip,
	Icon,
	Link,
	List,
	ListButton,
	ListInput,
	ListItem,
	NavLeft,
	NavRight,
	NavTitle,
	Navbar,
	Page,
	Popup,
} from 'framework7-react';
import React, { useCallback, useContext, useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';

import './RouteFormPopup.less';
import { useOnTrue } from '../../js/reactHooks';
import { useTranslation } from 'react-i18next';
import AppConfigContext from '../../contexts/AppConfigContext';

export function useRouteFormPopupState() {
	const [opened, setOpened] = useState(false);
	const [initialValues, setInitialValues] = useState(null);
	const [duplicating, setDuplicating] = useState(false);

	const open = useCallback((newValues) => {
		setOpened(true);
		setInitialValues(newValues || null);
		setDuplicating(false);
	}, []);

	const close = useCallback(() => {
		setOpened(false);
	}, []);

	return {
		close,
		duplicating,
		initialValues,
		open,
		opened,
		setDuplicating,
	};
}

function RouteFormPopup(props) {
	const { state, onSubmit, onDelete } = props;

	const { t } = useTranslation('RouteFormPopup');
	const { compMode } = useContext(AppConfigContext);

	const SCORING_METHODS = useMemo(
		() => [
			{
				id: 'absolute',
				name: t('form.scoring_method.absolute.name'),
				helpText: t('form.scoring_method.absolute.help'),
			},
			{
				id: 'mapped',
				name: t('form.scoring_method.mapped.name'),
				helpText: t('form.scoring_method.mapped.help'),
			},
			{
				id: 'shared',
				name: t('form.scoring_method.shared.name'),
				helpText: t('form.scoring_method.shared.help'),
			},
		],
		[t]
	);

	const SCORING_METHODS_MAP = useMemo(
		() =>
			SCORING_METHODS.reduce((acc, itm) => {
				acc[itm.id] = itm;
				return acc;
			}, {}),
		[SCORING_METHODS]
	);

	const INPUT_METHODS = useMemo(
		() => [
			{ id: 'number', name: t('form.input_method.number.name') },
			{ id: 'select', name: t('form.input_method.select.name') },
		],
		[t]
	);

	const [name, setName] = useState('');
	const [scoringMethod, setScoringMethod] = useState(SCORING_METHODS[0].id);
	const [inputMethod, setInputMethod] = useState(INPUT_METHODS[0].id);
	const [points, setPoints] = useState({});
	const [gradient, setGradient] = useState('0');
	const [max, setMax] = useState('0');
	const [min, setMin] = useState('0');
	const [reversed, setReversed] = useState(false);
	const [subFormFocused, setSubFormFocused] = useState(false);

	const [currentPointsName, setCurrentPointsName] = useState('');
	const [currentPointsCount, setCurrentPointsCount] = useState('');

	useEffect(() => {
		if (state.initialValues) {
			setName(state.initialValues.name);
			setInputMethod(state.initialValues.inputMethod);
			setScoringMethod(state.initialValues.scoringMethod);
			setPoints(state.initialValues.points);
			setGradient(state.initialValues.gradient);
			setMax(state.initialValues.max);
			setMin(state.initialValues.min);
			setReversed(state.initialValues.reversed);
		} else {
			setName('');
			setInputMethod(INPUT_METHODS[0].id);
			setScoringMethod(SCORING_METHODS[0].id);
			setPoints({});
			setGradient('0');
			setMax('0');
			setMin('0');
			setReversed(false);
		}
	}, [INPUT_METHODS, SCORING_METHODS, state.initialValues, state.opened]);

	const editingRoute = useMemo(() => state.initialValues, [state.initialValues]);

	const closePopup = useCallback(() => {
		state.close();
	}, [state]);

	const saveRoute = useCallback(() => {
		const toSave = {
			gradient: parseInt(gradient),
			max: parseFloat(max),
			min: parseFloat(min),
			name,
			points,
			inputMethod,
			scoringMethod,
			reversed,
		};
		if (toSave.gradient <= 1) {
			toSave.gradient = 0;
		}
		if (editingRoute && !state.duplicating) {
			toSave.id = editingRoute.id;
		}
		onSubmit(toSave);
	}, [
		editingRoute,
		gradient,
		inputMethod,
		scoringMethod,
		max,
		min,
		name,
		onSubmit,
		points,
		reversed,
		state.duplicating,
	]);

	const removePoints = useCallback((pointCount) => {
		setPoints((oldPoints) => {
			const newPoints = { ...oldPoints };
			delete newPoints[pointCount];
			return newPoints;
		});
	}, []);

	const addCurrentPoints = useCallback(() => {
		if (!currentPointsCount) return;
		if (currentPointsCount.match(/:/)) {
			let [min, max, step = '1'] = currentPointsCount.split(/:/g);
			min = parseFloat(min);
			max = parseFloat(max);
			step = parseFloat(step);

			if (Number.isNaN(min) || Number.isNaN(max) || Number.isNaN(step)) return;
			if (min > max || step < 0) return;

			setPoints((oldPoints) => {
				const newPoints = {
					...oldPoints,
				};

				for (let i = min; i <= max; i += step) {
					newPoints[i] = currentPointsName || i;
				}
				return newPoints;
			});
		} else {
			const countFlt = parseFloat(currentPointsCount, 10);
			if (Number.isNaN(countFlt)) return;

			setPoints((oldPoints) => ({
				...oldPoints,
				[countFlt]: currentPointsName || countFlt,
			}));
		}
		setCurrentPointsName('');
		setCurrentPointsCount('');
	}, [currentPointsCount, currentPointsName]);

	const doDelete = useCallback(() => {
		if (!editingRoute) return;
		onDelete(editingRoute.id);
		state.close();
	}, [editingRoute, onDelete, state]);

	useOnTrue(scoringMethod === 'mapped', () => {
		setInputMethod('number');
	});

	return (
		<Popup className="RouteFormPopup" onPopupClosed={closePopup} opened={state.opened}>
			<Page>
				<Navbar>
					<NavLeft>
						<Button onClick={saveRoute}>
							{editingRoute && !state.duplicating ? t('save_button.edit') : t('save_button.add')}
						</Button>
					</NavLeft>
					<NavTitle>
						{editingRoute && !state.duplicating
							? t('popup_title.edit', { name: editingRoute.name })
							: t('popup_title.add')}
					</NavTitle>
					<NavRight>
						<Link popupClose>
							<Icon f7="xmark" />
						</Link>
					</NavRight>
				</Navbar>

				<List
					form
					strongIos
					outlineIos
					dividersIos
					onSubmit={(e) => {
						e.preventDefault();
						if (subFormFocused) {
							addCurrentPoints();
						} else {
							saveRoute();
						}
					}}
				>
					<ListInput
						label={t('form.name.label')}
						type="text"
						placeholder={t('form.name.placeholder')}
						value={name}
						onChange={(e) => setName(e.target.value)}
					/>
					{compMode === 'boulder_1zone' && (
						<ListItem key="RouteFormPopup-scoring_method-help">
							<Icon slot="media" f7="question_circle_fill" color="red" />
							{t('boulder_mode_text')}
						</ListItem>
					)}
					<ListInput
						label={t('form.scoring_method.label')}
						type="select"
						value={scoringMethod}
						onChange={(e) => setScoringMethod(e.target.value)}
					>
						{SCORING_METHODS.map(({ id, name }) => (
							<option key={`RouteFormPopup-scoringMethod-${id}`} value={id}>
								{name}
							</option>
						))}
					</ListInput>
					<ListItem>
						<Icon slot="media" f7="question_circle_fill" />
						{SCORING_METHODS_MAP[scoringMethod].helpText}
					</ListItem>
					<ListInput
						label={t('form.input_method.label')}
						type="select"
						value={inputMethod}
						disabled={scoringMethod === 'mapped'}
						onChange={(e) => setInputMethod(e.target.value)}
					>
						{INPUT_METHODS.map(({ id, name }) => (
							<option key={`RouteFormPopup-inputMethod-${id}`} value={id}>
								{name}
							</option>
						))}
					</ListInput>
					{inputMethod === 'select' && [
						<ListItem key="RouteFormPopup-points-chips" className="points-subform-item">
							<span className="points-label">{'Points : '}</span>
							<span className="points-list">
								{Object.keys(points).map((pointCount) => (
									<Chip
										outline
										key={`RouteFormPopup-points-${pointCount}-${points[pointCount]}`}
										text={`${points[pointCount]} (${pointCount})`}
										deleteable
										onDelete={() => removePoints(pointCount)}
									/>
								))}
							</span>
						</ListItem>,
						<ListInput
							key="RouteFormPopup-points-count"
							className="points-subform-item"
							label={t('form.points_count.label')}
							info={t('form.points_count.info')}
							errorMessage={t('form.points_count.error')}
							type="text"
							placeholder={t('form.points_count.placeholder')}
							value={currentPointsCount}
							pattern="[+-]?([0-9]*\.)?[0-9]+(:[+-]?([0-9]*\.)?[0-9]+)?(:[+-]?([0-9]*\.)?[0-9]+)?"
							validateOnBlur={false}
							validate
							onChange={(e) => setCurrentPointsCount(e.target.value)}
							onFocus={() => setSubFormFocused(true)}
							onBlur={() => setSubFormFocused(false)}
						/>,
						<ListInput
							key="RouteFormPopup-points-name"
							className="points-subform-item"
							label={t('form.points_name.label')}
							type="text"
							placeholder={t('form.points_name.placeholder')}
							value={currentPointsName}
							onChange={(e) => setCurrentPointsName(e.target.value)}
							onFocus={() => setSubFormFocused(true)}
							onBlur={() => setSubFormFocused(false)}
						/>,
						<ListItem key="RouteFormPopup-points-add" className="points-subform-item">
							<Button slot="after" fill onClick={addCurrentPoints}>
								<Icon f7="plus_circle_fill" />
							</Button>
						</ListItem>,
					]}
					{scoringMethod === 'mapped' && [
						<ListInput
							key="RouteFormPopup-gradient"
							className="points-subform-item"
							label={t('form.gradient.label')}
							type="number"
							placeholder={t('form.gradient.placeholder')}
							value={gradient}
							onChange={(e) => setGradient(e.target.value)}
							onFocus={() => setSubFormFocused(true)}
							onBlur={() => setSubFormFocused(false)}
						/>,
						<ListItem key="RouteFormPopup-gradient-help">
							<Icon slot="media" f7="question_circle_fill" />
							{t('form.gradient.help_text')}
						</ListItem>,
						<ListInput
							key="RouteFormPopup-min"
							className="points-subform-item"
							label={t('form.points_min.label')}
							type="number"
							placeholder={t('form.points_min.placeholder')}
							value={min}
							onChange={(e) => setMin(e.target.value)}
							onFocus={() => setSubFormFocused(true)}
							onBlur={() => setSubFormFocused(false)}
						/>,
						<ListInput
							key="RouteFormPopup-max"
							className="points-subform-item"
							label={t('form.points_max.label')}
							type="number"
							placeholder={t('form.points_max.placeholder')}
							value={max}
							onChange={(e) => setMax(e.target.value)}
							onFocus={() => setSubFormFocused(true)}
							onBlur={() => setSubFormFocused(false)}
						/>,
						<ListItem
							key="RouteFormPopup-reversed"
							className="points-subform-item"
							checkbox
							checked={reversed}
							onChange={() => setReversed((oldReversed) => !oldReversed)}
							onFocus={() => setSubFormFocused(true)}
							onBlur={() => setSubFormFocused(false)}
						>
							{t('form.reversed.text')}
						</ListItem>,
					]}
					{editingRoute && !state.duplicating && (
						<ListButton onClick={() => state.setDuplicating(true)}>{t('form.buttons.duplicate_as_new')}</ListButton>
					)}
					{editingRoute && !state.duplicating && <ListButton onClick={doDelete}>{t('form.buttons.delete')}</ListButton>}
					<ListItem style={{ display: 'none' }}>
						<Button type="submit">Hidden button, to allow onSubmit on enter keypress</Button>
					</ListItem>
				</List>
			</Page>
		</Popup>
	);
}

RouteFormPopup.propTypes = {
	state: PropTypes.shape({
		close: PropTypes.func.isRequired,
		duplicating: PropTypes.bool.isRequired,
		open: PropTypes.func.isRequired,
		opened: PropTypes.bool.isRequired,
		setDuplicating: PropTypes.func.isRequired,
		initialValues: PropTypes.shape({
			gradient: PropTypes.number,
			id: PropTypes.string,
			inputMethod: PropTypes.string,
			max: PropTypes.number,
			min: PropTypes.number,
			name: PropTypes.string,
			points: PropTypes.object,
			reversed: PropTypes.bool,
			scoringMethod: PropTypes.string,
		}),
	}).isRequired,
	onDelete: PropTypes.func,
	onSubmit: PropTypes.func,
};

RouteFormPopup.defaultProps = {
	onDelete: () => {},
	onSubmit: () => {},
};

export default RouteFormPopup;
