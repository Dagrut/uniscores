import React from 'react';
import { parseBoulderPoints, reverseBoulderPoints } from '../js/boulderPoints';
import PropTypes from 'prop-types';
import { Trans } from 'react-i18next';

function FormattedPoints(props) {
	const { points, compMode, isTotal } = props;

	if (compMode === 'boulder_1zone') {
		const dec = isTotal ? reverseBoulderPoints(parseBoulderPoints(points)) : parseBoulderPoints(points);
		return (
			<div className="grid grid-gap grid-cols-2">
				<span>
					<Trans ns="FormattedPoints" i18nKey="boulder_labels.t" values={dec}>
						T: <b>{`${dec.t}`}</b>
					</Trans>
				</span>
				<span>
					<Trans ns="FormattedPoints" i18nKey="boulder_labels.z" values={dec}>
						Z: <b>{`${dec.z}`}</b>
					</Trans>
				</span>
				<span>
					<Trans ns="FormattedPoints" i18nKey="boulder_labels.ta" values={dec}>
						TA: <b>{`${dec.ta}`}</b>
					</Trans>
				</span>
				<span>
					<Trans ns="FormattedPoints" i18nKey="boulder_labels.za" values={dec}>
						ZA: <b>{`${dec.za}`}</b>
					</Trans>
				</span>
			</div>
		);
	}

	const fltPoints = typeof points === 'string' ? parseFloat(points) : points;

	return <b className="monospace-font">{Math.round(fltPoints * 100) / 100}</b>;
}

FormattedPoints.propTypes = {
	points: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
	compMode: PropTypes.oneOf(['point_based', 'time_based', 'rank_based', 'boulder_1zone']).isRequired,
	isTotal: PropTypes.bool,
};

FormattedPoints.defaultProps = {
	isTotal: false,
};

export default FormattedPoints;
