/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import PropTypes from 'prop-types';
import React from 'react';

function ComponentsStack(props) {
	const { children, components } = props;

	if (components.length === 0) {
		return children;
	}
	const FirstComponent = components[0];
	return (
		<FirstComponent>
			<ComponentsStack components={components.slice(1)}>{children}</ComponentsStack>
		</FirstComponent>
	);
}

ComponentsStack.propTypes = {
	children: PropTypes.node.isRequired,
	components: PropTypes.arrayOf(PropTypes.elementType).isRequired,
};

export default ComponentsStack;
