/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import { Block, BlockTitle, Button, Chip, Icon, List, ListItem, f7 } from 'framework7-react';
import React, { useCallback, useContext, useRef, useState } from 'react';
import PropTypes from 'prop-types';

import ErrorMessage from './ErrorMessage';
import RouteFormPopup, { useRouteFormPopupState } from './popups/RouteFormPopup';
import Tooltip from './Tooltip';
import useFetch from '../js/useFetch';
import { uniqId } from '../js/utils';
import { useErrorTranslator, useOnTrue } from '../js/reactHooks';

import './DisciplineRoutesList.less';
import { useTranslation } from 'react-i18next';
import AppConfigContext from '../contexts/AppConfigContext';

function DisciplineRoutesList(props) {
	const { disciplineId, routes: initialRoutes } = props;

	const [routes, setRoutes] = useState(initialRoutes);
	const pendingRouteOp = useRef([]);

	const [routeEditResponse, internalRouteEdit] = useFetch();

	const routeFormPopupState = useRouteFormPopupState();
	const { t } = useTranslation('DisciplineRoutesList');
	const { compMode } = useContext(AppConfigContext);
	const errorTs = useErrorTranslator();

	const editOrCreateRoute = useCallback(
		(route) => {
			internalRouteEdit(`/api/trials/${disciplineId}/${route.id}`, {
				method: 'PUT',
				body: route,
			});
		},
		[disciplineId, internalRouteEdit]
	);

	const deleteRoute = useCallback(
		(id) => {
			internalRouteEdit(`/api/trials/${disciplineId}/${id}`, {
				method: 'DELETE',
			});
		},
		[disciplineId, internalRouteEdit]
	);

	useOnTrue(routeEditResponse.rejected, () => {
		f7.dialog.alert(t('edit_error', { message: errorTs(routeEditResponse.error.message) }));
	});

	useOnTrue(routeEditResponse.fulfilled, () => {
		f7.toast
			.create({
				text: routeEditResponse.inputs.options.method === 'DELETE' ? t('toasts.deleted') : t('toasts.added'),
				position: 'top',
				horizontalPosition: 'right',
				closeTimeout: 2000,
			})
			.open();

		if (pendingRouteOp.current.length > 0) {
			const current = pendingRouteOp.current.shift();
			if (current.delete) {
				deleteRoute(current.id);
			} else {
				editOrCreateRoute(current.route);
			}
		}
	});

	const addRoute = useCallback(
		(route) => {
			if (!route.id) {
				route.id = uniqId();
			}
			setRoutes((oldRoutes) => {
				const newRoutes = { ...oldRoutes };
				newRoutes[route.id] = {
					...route,
					id: route.id,
				};

				return newRoutes;
			});

			if (routeEditResponse.pending) {
				pendingRouteOp.current.push({ editOrCreate: true, route });
			} else {
				editOrCreateRoute(route);
			}
		},
		[editOrCreateRoute, routeEditResponse.pending]
	);

	const delRoute = useCallback(
		(id) => {
			setRoutes((oldRoutes) => {
				const newRoutes = { ...oldRoutes };
				delete newRoutes[id];
				return newRoutes;
			});

			if (routeEditResponse.pending) {
				pendingRouteOp.current.push({ delete: true, id });
			} else {
				deleteRoute(id);
			}
		},
		[deleteRoute, routeEditResponse.pending]
	);

	return (
		<>
			<RouteFormPopup state={routeFormPopupState} onSubmit={addRoute} onDelete={delRoute} />

			<Block className="buttons-row">
				<Button fill onClick={() => routeFormPopupState.open()}>
					<Icon f7="text_badge_plus" />
					&nbsp;
					{t('add_button')}
				</Button>
			</Block>

			{routeEditResponse.rejected && <ErrorMessage centered error={routeEditResponse.error} />}

			<BlockTitle>{t('list_blocktitle')}</BlockTitle>
			<List strong inset dividers>
				{Object.values(routes).map((route) => (
					<ListItem
						key={`DisciplineRoutesList-${disciplineId}-route-${route.id}`}
						href="#"
						onClick={() => routeFormPopupState.open(route)}
					>
						<div slot="title">{route.name}</div>
						{compMode === 'boulder_1zone' && (
							<div slot="inner" className="DisciplineRoutesList-route-badges">
								<Tooltip component={Chip} tooltipText={t('boulder_mode_tooltip')} text="---" />
							</div>
						)}
						{compMode !== 'boulder_1zone' && [
							route.inputMethod === 'select' && (
								<div
									key={`DisciplineRoutesList-${disciplineId}-route-${route.id}-pointsChip`}
									slot="inner"
									className="DisciplineRoutesList-route-badges"
								>
									{Object.keys(route.points).map((pointCount) => (
										<Tooltip
											component={Chip}
											key={`DisciplineRoutesList-${disciplineId}-route-${route.id}-points-${pointCount}`}
											tooltipText={t('points_count_tooltip', { points: pointCount })}
											text={`${route.points[pointCount]}`}
										/>
									))}
								</div>
							),
							route.scoringMethod === 'mapped' && (
								<div
									key={`DisciplineRoutesList-${disciplineId}-route-${route.id}-infos`}
									slot="inner"
									className="DisciplineRoutesList-route-badges"
								>
									{!!route.gradient && (
										<Tooltip
											component={Chip}
											tooltipText={t('gradient_chip_tooltip', { gradient: route.gradient })}
											text={`${route.gradient}`}
											mediaBgColor="pink"
										>
											<Icon slot="media" f7="rectangle_split_3x1" />
										</Tooltip>
									)}
									<Tooltip
										component={Chip}
										tooltipText={
											route.reversed ? t('reversed_chip.tooltip.reversed') : t('reversed_chip.tooltip.normal')
										}
										text={route.reversed ? t('reversed_chip.text.reversed') : t('reversed_chip.text.normal')}
										mediaBgColor="yellow"
									>
										<Icon slot="media" f7="arrow_up_arrow_down" />
									</Tooltip>
									<Tooltip
										component={Chip}
										tooltipText={t('minmax_chip_tooltip', { min: route.min, max: route.max })}
										text={`[${route.min}, ${route.max}]`}
										mediaBgColor="blue"
									>
										<Icon slot="media" f7="minus_slash_plus" />
									</Tooltip>
								</div>
							),
						]}
					</ListItem>
				))}
			</List>
		</>
	);
}

DisciplineRoutesList.propTypes = {
	disciplineId: PropTypes.string.isRequired,
	routes: PropTypes.object.isRequired,
};

export default DisciplineRoutesList;
