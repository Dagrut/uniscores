/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import PropTypes from 'prop-types';
import React, { useRef, useState } from 'react';
import { useOnChange, useOnTrue } from '../js/reactHooks';
import { Block, BlockTitle, Button, f7 } from 'framework7-react';
import { useTranslation } from 'react-i18next';
import { uniqId } from '../js/utils';
import useFetch from '../js/useFetch';
import ErrorMessage from './ErrorMessage';

function UsersImporter(props) {
	const { list, onUserImport } = props;
	const [pending, setPending] = useState(null);
	const [errors, setErrors] = useState([]);
	const counts = useRef({
		total: 0,
	});
	const dialog = useRef(null);

	const { t } = useTranslation('AdminUsersPage');

	const [addUserResponse, internalAddUser] = useFetch();

	useOnChange(
		list,
		() => {
			if (list.length > 0) {
				setPending(list);
				counts.current.total = list.length;
				setErrors([]);
			}
		},
		false
	);

	useOnChange(
		pending,
		() => {
			if (!dialog.current) {
				dialog.current = f7.dialog.progress(t('import_dialog.title'), 0);
			}
			const progressId = 1 + counts.current.total - pending.length;
			dialog.current.setProgress((100 * progressId) / counts.current.total);
			dialog.current.setText(
				t('import_dialog.text', {
					current: progressId,
					total: counts.current.total,
				})
			);

			if (pending.length === 0) {
				dialog.current.close();
				dialog.current = null;

				if (errors.length > 0) {
					f7.dialog.alert(t('success.imported_errors', { count: counts.current.total, errors: errors.length }));
				} else {
					f7.dialog.alert(t('success.imported', { count: counts.current.total }));
				}
			} else {
				const nextUser = pending[0];

				internalAddUser('/api/users', {
					method: 'POST',
					body: {
						id: uniqId(),
						username: `${nextUser.firstname} ${nextUser.lastname}`,
						password: null,
						profile: nextUser,
					},
				});
			}
		},
		false
	);

	useOnTrue(addUserResponse.fulfilled, () => {
		onUserImport(addUserResponse.inputs.options.body);
		setPending((oldPending) => oldPending.slice(1));
	});

	useOnTrue(addUserResponse.rejected, () => {
		setErrors((oldErrors) => {
			const newErrors = [
				...oldErrors,
				{
					forUser: `${pending[0].firstname} ${pending[0].lastname}`,
					error: addUserResponse.error,
				},
			];
			return newErrors;
		});
		setPending((oldPending) => oldPending.slice(1));
	});

	if (errors.length > 0) {
		return (
			<>
				<BlockTitle>{t('titles.import_errors')}</BlockTitle>
				<Block>
					{errors.map(({ forUser, error }, arrayId) => (
						<div key={`UsersImporter-error-for-${forUser}-${arrayId}`}>
							{`${forUser} : `}
							<ErrorMessage error={error} />
						</div>
					))}
					<Button color="red" fill onClick={() => setErrors([])}>
						{t('import_errors_hide')}
					</Button>
				</Block>
			</>
		);
	}

	return false;
}

UsersImporter.propTypes = {
	list: PropTypes.arrayOf(
		PropTypes.shape({
			firstname: PropTypes.string,
			lastname: PropTypes.string,
		})
	).isRequired,
	onUserImport: PropTypes.func,
};

UsersImporter.defaultProps = {
	onUserImport: () => {},
};

export default UsersImporter;
