/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
const tsData = import.meta.glob('./locales/*/*.json', { eager: true });

const resources = Object.keys(tsData).reduce((acc, fileName) => {
	const matches = fileName.match(/^\.\/locales\/([^/]*)\/([^/]*)\.json$/);
	const [, lang, namespace] = matches;
	if (!acc[lang]) {
		acc[lang] = {};
	}
	acc[lang][namespace] = tsData[fileName].default;
	return acc;
}, {});

export const languages = Object.keys(resources);

export default {
	resources,
	fallbackLng: 'en',
	supportedLngs: languages,

	interpolation: {
		escapeValue: false, // react already safes from xss => https://www.i18next.com/translation-function/interpolation#unescape
	},
};
