/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { useCallback, useContext, useEffect, useState } from 'react';
import { Page, BlockTitle, f7, Button, Icon, Block, List, ListItem, Chip, Link } from 'framework7-react';

import CommonNavbar from '../components/CommonNavbar';
import ErrorMessage from '../components/ErrorMessage';
import UserProfileFieldFormPopup, {
	useUserProfileFieldFormPopupState,
} from '../components/popups/UserProfileFieldFormPopup';
import UserProfileFieldsContext from '../contexts/UserProfileFieldsContext';
import useFetch from '../js/useFetch';
import { useOnTrue, usePerms } from '../js/reactHooks';
import { useTranslation } from 'react-i18next';
import Tooltip from '../components/Tooltip';
import UserProfileFieldApplyPopup, {
	useUserProfileFieldApplyPopupState,
} from '../components/popups/UserProfileFieldApplyPopup';
import UserProfileFieldValueFormPopup, {
	useUserProfileFieldValueFormPopupState,
} from '../components/popups/UserProfileFieldValueFormPopup';

function AdminUsersProfileFieldsPage() {
	usePerms({ admin: true });

	const [fields, setFields] = useState([]);

	const [updatedFieldsResponse, internalUpdateFields] = useFetch();

	const extraProfileFields = useContext(UserProfileFieldsContext);
	const { t } = useTranslation('AdminUsersProfileFieldsPage');

	const saveProfileFields = useCallback(() => {
		internalUpdateFields('/api/userProfileFields', {
			method: 'POST',
			body: fields,
		});
	}, [fields, internalUpdateFields]);

	const userProfileFieldFormPopupState = useUserProfileFieldFormPopupState();
	const userProfileFieldValueFormPopupState = useUserProfileFieldValueFormPopupState();
	const userProfileFieldApplyPopupState = useUserProfileFieldApplyPopupState();

	useEffect(() => {
		setFields(extraProfileFields.fields.list);
	}, [extraProfileFields.fields.list]);

	useOnTrue(updatedFieldsResponse.fulfilled, () => {
		f7.toast
			.create({
				text: t('toast_updated_text'),
				position: 'top',
				horizontalPosition: 'right',
				closeTimeout: 2000,
			})
			.open();
		userProfileFieldFormPopupState.close();
		userProfileFieldValueFormPopupState.close();
		extraProfileFields.refresh();
	});

	const setField = useCallback(
		(field) => {
			const { arrayId, name, visible, key } = field;

			setFields((oldFields) => {
				const newFields = [...oldFields];

				if (arrayId >= 0) {
					newFields[arrayId] = {
						...newFields[arrayId],
						name,
						visible,
						key,
					};
				} else {
					newFields.push({
						name,
						visible,
						key,
						values: [],
					});
				}

				return newFields;
			});

			userProfileFieldFormPopupState.close();
		},
		[userProfileFieldFormPopupState]
	);

	const setFieldValue = useCallback(
		(field) => {
			const { arrayId, fieldData, name, key } = field;

			setFields((oldFields) => {
				const newFields = [...oldFields];

				if (fieldData && fieldData.arrayId >= 0) {
					if (arrayId >= 0) {
						newFields[fieldData.arrayId].values[arrayId] = {
							...newFields[fieldData.arrayId].values[arrayId],
							name,
							key,
						};
					} else {
						newFields[fieldData.arrayId].values.push({
							name,
							key,
						});
					}
				} else if (arrayId >= 0) {
					newFields[arrayId] = {
						...newFields[arrayId],
						name,
						key,
					};
				} else {
					newFields.push({
						name,
						key,
						values: [],
					});
				}

				return newFields;
			});

			userProfileFieldValueFormPopupState.close();
		},
		[userProfileFieldValueFormPopupState]
	);

	return (
		<Page>
			<CommonNavbar title={t('page_title')} />

			<UserProfileFieldFormPopup state={userProfileFieldFormPopupState} onSubmit={setField} />
			<UserProfileFieldValueFormPopup state={userProfileFieldValueFormPopupState} onSubmit={setFieldValue} />

			<UserProfileFieldApplyPopup state={userProfileFieldApplyPopupState} onSubmit={setField} />

			<BlockTitle>{t('fields_blocktitle')}</BlockTitle>
			<Block>{t('fields_help_text')}</Block>
			{updatedFieldsResponse.rejected && (
				<Block>
					<ErrorMessage error={updatedFieldsResponse.error} />
				</Block>
			)}
			<Block className="buttons-row">
				<Button fill onClick={() => userProfileFieldFormPopupState.open()}>
					<Icon f7="plus_app" />
					&nbsp;
					{t('form.buttons.add_field')}
				</Button>
				<Button fill onClick={() => userProfileFieldApplyPopupState.open(fields)}>
					<Icon f7="pencil_ellipsis_rectangle" />
					&nbsp;
					{t('form.buttons.apply_field')}
				</Button>
			</Block>

			<List inset outline dividers>
				{fields.map((field, fieldArrayId) => (
					<ListItem key={`AdminUsersProfileFieldsPage-field-${field.key}`}>
						<Tooltip
							slot="media"
							component={Button}
							color="red"
							small
							onClick={() =>
								setFields((oldFields) => {
									const newFields = [...oldFields];
									newFields.splice(fieldArrayId, 1);
									return newFields;
								})
							}
							tooltipText={t('form.buttons.delete_field')}
						>
							<Icon f7="trash_circle" />
						</Tooltip>
						<span>
							<Tooltip
								slot="media"
								component={Link}
								small
								onClick={() => {
									userProfileFieldFormPopupState.open({
										...field,
										arrayId: fieldArrayId,
									});
								}}
								tooltipText={t('form.buttons.edit_field')}
							>
								<Icon f7="pencil_circle" />
							</Tooltip>
							&nbsp;
							{`${field.name}`}
						</span>
						<span className="chips-list-container text-align-right">
							{field.values.map((value, valueArrayId) => (
								<div key={`AdminUsersProfileFieldsPage-field-${field.key}-value-${value.key}`}>
									<Chip
										outline
										deleteable
										onDelete={() =>
											setFields((oldFields) => {
												const newFields = [...oldFields];
												newFields[fieldArrayId].values = [...newFields[fieldArrayId].values];
												newFields[fieldArrayId].values.splice(valueArrayId, 1);
												return newFields;
											})
										}
									>
										<Tooltip
											slot="media"
											component={Link}
											small
											onClick={() => {
												userProfileFieldValueFormPopupState.open(
													{
														...value,
														arrayId: valueArrayId,
													},
													{ ...field, arrayId: fieldArrayId }
												);
											}}
											tooltipText={t('form.buttons.edit_value')}
										>
											<Icon f7="pencil_circle" />
										</Tooltip>
										&nbsp;
										{`${value.name}`}
									</Chip>
								</div>
							))}

							<Link
								fill
								onClick={() => userProfileFieldValueFormPopupState.open(null, { ...field, arrayId: fieldArrayId })}
							>
								<Icon f7="plus_app" />
							</Link>
						</span>
					</ListItem>
				))}
			</List>

			<Block className="buttons-row">
				<Button fill onClick={saveProfileFields}>
					<Icon f7="floppy_disk" />
					&nbsp;
					{t('form.buttons.save')}
				</Button>
			</Block>
		</Page>
	);
}

export default AdminUsersProfileFieldsPage;
