/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { useMemo } from 'react';
import { Page, Block, Link, f7 } from 'framework7-react';
import CommonNavbar from '../components/CommonNavbar';
import { Trans, useTranslation } from 'react-i18next';

function NotFoundPage() {
	const { t } = useTranslation('NotFoundPage');

	const pageContent = useMemo(() => {
		if (import.meta.env.CLI_UNISCORES_HOST_MODE === 'access-point') {
			const url = import.meta.env.CLI_UNISCORES_HOST_URL;

			const copyUrl = () => {
				if (navigator && navigator.clipboard && navigator.clipboard.writeText) {
					navigator.clipboard.writeText(url);
				} else {
					const dummy = document.createElement('input');

					document.body.appendChild(dummy);
					dummy.value = url;
					dummy.select();
					document.execCommand('copy');
					document.body.removeChild(dummy);
				}

				f7.toast
					.create({
						text: t('toast_copied_url'),
						position: 'center',
						horizontalPosition: 'center',
						closeTimeout: 2000,
					})
					.open();
			};

			return (
				<Trans t={t} ns="NotFoundPage" i18nKey="page_contents.hosted" values={{ url }}>
					<p>Vous êtes connecté au service de saisie des scores de la compétition!</p>
					<p>
						Pour accéder à la page principale, rendez-vous sur <Link onClick={copyUrl}>{url}</Link>.
					</p>
				</Trans>
			);
		} else {
			return (
				<Trans t={t} ns="NotFoundPage" i18nKey="page_contents.not_found">
					<p>Désolé, le contenu que vous cherchez n&apos;a pas été trouvé.</p>
					<p>
						Pour revenir à la page principale,
						<Link external href="/">
							cliquez ici
						</Link>
					</p>
				</Trans>
			);
		}
	}, [t]);

	const title = useMemo(() => {
		if (import.meta.env.CLI_UNISCORES_HOST_MODE === 'access-point') {
			return t('page_title.hosted');
		} else {
			return t('page_title.not_found');
		}
	}, [t]);

	return (
		<Page>
			<CommonNavbar backLink="/" title={title} />
			<Block strong inset>
				{pageContent}
			</Block>
		</Page>
	);
}

export default NotFoundPage;
