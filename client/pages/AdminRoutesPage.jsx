/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import { Block, BlockTitle, Button, Icon, Link, Page, Searchbar, f7 } from 'framework7-react';
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';

import CommonNavbar from '../components/CommonNavbar';
import ErrorMessage from '../components/ErrorMessage';
import useFetch from '../js/useFetch';
import { strRemoveDiacritics, uniqId } from '../js/utils';
import { useErrorTranslator, useOnTrue } from '../js/reactHooks';
import DisciplineFormPopup, { useDisciplineFormPopupState } from '../components/popups/DisciplineFormPopup';
import DisciplineRoutesList from '../components/DisciplineRoutesList';
import { useTranslation } from 'react-i18next';
import Tooltip from '../components/Tooltip';

function AdminRoutesPage() {
	const [filter, setFilter] = useState('');
	const [disciplines, setDisciplines] = useState({});
	const pendingDisciplineOp = useRef([]);
	const pendingDisciplinesOp = useRef([]);

	const [initialDisciplines] = useFetch('/api/trials');
	const [disciplineEditResponse, internalDisciplineEdit] = useFetch();

	const disciplineFormPopupState = useDisciplineFormPopupState();
	const { t } = useTranslation('AdminRoutesPage');
	const errorTs = useErrorTranslator();

	const editOrCreateDiscipline = useCallback(
		(discipline) => {
			internalDisciplineEdit(`/api/trials/${discipline.id}`, {
				method: 'PUT',
				body: discipline,
			});
		},
		[internalDisciplineEdit]
	);

	const deleteDiscipline = useCallback(
		(id) => {
			internalDisciplineEdit(`/api/trials/${id}`, {
				method: 'DELETE',
			});
		},
		[internalDisciplineEdit]
	);

	const onSearchChange = useCallback((e) => {
		setFilter(e.target.value);
	}, []);

	useOnTrue(disciplineEditResponse.rejected, () => {
		f7.dialog.alert(t('edit_error', { message: errorTs(disciplineEditResponse.error.message) }));
	});

	useOnTrue(disciplineEditResponse.fulfilled, () => {
		f7.toast
			.create({
				text:
					disciplineEditResponse.inputs.options.method === 'DELETE'
						? t('toasts.discipline_removed')
						: t('toasts.discipline_added'),
				position: 'top',
				horizontalPosition: 'right',
				closeTimeout: 2000,
			})
			.open();

		if (pendingDisciplinesOp.current.length > 0) {
			const current = pendingDisciplinesOp.current.shift();
			if (current.delete) {
				deleteDiscipline(current.id);
			} else {
				editOrCreateDiscipline(current.route);
			}
		}
	});

	useEffect(() => {
		if (initialDisciplines.fulfilled) {
			setDisciplines(initialDisciplines.json);
		}
	}, [initialDisciplines.fulfilled, initialDisciplines.json]);

	const addDiscipline = useCallback(
		(discipline) => {
			if (!discipline.id) {
				discipline.id = uniqId();
			}
			setDisciplines((oldDisciplines) => {
				const newDisciplines = { ...oldDisciplines };
				const oldDiscipline = oldDisciplines[discipline.id] || {};
				newDisciplines[discipline.id] = {
					routes: {},
					...oldDiscipline,
					...discipline,
				};

				return newDisciplines;
			});

			if (disciplineEditResponse.pending) {
				pendingDisciplineOp.current.push({ editOrCreate: true, discipline });
			} else {
				editOrCreateDiscipline(discipline);
			}
		},
		[editOrCreateDiscipline, disciplineEditResponse.pending]
	);

	const delDiscipline = useCallback(
		(id) => {
			setDisciplines((oldDisciplines) => {
				const newDisciplines = { ...oldDisciplines };
				delete newDisciplines[id];
				return newDisciplines;
			});

			if (disciplineEditResponse.pending) {
				pendingDisciplinesOp.current.push({ delete: true, id });
			} else {
				deleteDiscipline(id);
			}
		},
		[deleteDiscipline, disciplineEditResponse.pending]
	);

	const filteredDisciplinesArray = useMemo(() => {
		const cleanFilter = strRemoveDiacritics(filter).toLowerCase();

		const ret = Object.values(disciplines).filter((discipline) => {
			if (!filter) {
				return true;
			}

			const cleanedName = strRemoveDiacritics(discipline.name).toLowerCase();
			const allowed = cleanedName.indexOf(cleanFilter) >= 0;

			return allowed;
		});

		return ret;
	}, [disciplines, filter]);

	return (
		<Page>
			<CommonNavbar title={t('page_title')} />

			<DisciplineFormPopup state={disciplineFormPopupState} onSubmit={addDiscipline} onDelete={delDiscipline} />

			<Block className="buttons-row">
				<Button fill onClick={() => disciplineFormPopupState.open()}>
					<Icon f7="plus_circle" />
					&nbsp;
					{t('add_discipline_button')}
				</Button>
			</Block>

			{disciplineEditResponse.rejected && <ErrorMessage centered error={disciplineEditResponse.error} />}

			<BlockTitle>{t('filter_blocktitle')}</BlockTitle>
			<Block>
				<Searchbar
					disableButtonText={t('filter_searchbar.cancel')}
					placeholder={t('filter_searchbar.placeholder')}
					clearButton={true}
					onChange={onSearchChange}
					onSearchbarClear={() => setFilter('')}
					onSearchbarDisable={() => setFilter('')}
				/>
			</Block>

			{filteredDisciplinesArray.map((discipline) => (
				<div key={`AdminRoutesPage-discipline-${discipline.id}`}>
					<BlockTitle large>
						{t('discipline_blocktitle', { name: discipline.name })}
						&nbsp;
						<Link onClick={() => disciplineFormPopupState.open(discipline)}>
							<Tooltip component={Icon} f7="pencil_circle" tooltipText={t('discipline_icon_tooltip.edit')} />
						</Link>
					</BlockTitle>
					<DisciplineRoutesList disciplineId={discipline.id} routes={discipline.routes} />
				</div>
			))}
		</Page>
	);
}

export default AdminRoutesPage;
