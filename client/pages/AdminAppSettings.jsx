/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { useCallback, useContext, useEffect, useMemo, useRef, useState } from 'react';
import {
	Block,
	BlockTitle,
	Button,
	Checkbox,
	Icon,
	Input,
	List,
	ListInput,
	ListItem,
	Page,
	f7,
} from 'framework7-react';
import CommonNavbar from '../components/CommonNavbar';
import { useOnTrue, usePerms } from '../js/reactHooks';
import useFetch from '../js/useFetch';
import ErrorMessage from '../components/ErrorMessage';
import AppConfigContext from '../contexts/AppConfigContext';
import TooltipIcon from '../components/TooltipIcon';
import { useTranslation } from 'react-i18next';

function nowYMDHM(plusDays = 0) {
	const now = new Date();
	if (plusDays !== 0) {
		now.setDate(now.getDate() + plusDays);
	}

	const tz1 = new Date('2000-01-01T00:00:00.000');
	const tz2 = new Date('2000-01-01T00:00:00.000Z');
	const tzDiff = tz2.getTime() - tz1.getTime();
	now.setTime(now.getTime() + tzDiff);

	const iso = now.toISOString();
	const ymd = iso.replace(/:..\....Z$/, '');
	return ymd;
}

function dateToYMDHMOrNull(date) {
	if (date === null) return null;
	const iso = date.toISOString();
	const ymd = iso.replace(/:..\....Z$/, '');
	return ymd;
}

function dateYMDHMToIso(str) {
	const date = new Date(`${str}:00.000`);

	return date.toISOString();
}

function AdminAppSettings() {
	const [adminPassword, setAdminPassword] = useState('');
	const [adminPassword2, setAdminPassword2] = useState('');
	const [appThemeColor, setAppThemeColor] = useState('#1482f4');
	const [compIsOpen, setcompIsOpen] = useState(true);
	const [compMode, setCompMode] = useState('point_based');
	const [compName, setCompName] = useState('');
	const [displayResultsAfter, setDisplayResultsAfter] = useState(true);
	const [displayResultsDuring, setDisplayResultsDuring] = useState(true);
	const [displayStats, setDisplayStats] = useState(true);
	const [endDate, setEndDate] = useState(null);
	const [skipZeroPoints, setSkipZeroPoints] = useState(true);
	const [startDate, setStartDate] = useState(null);

	const themeColorChangeId = useRef(null);
	const confFileImport = useRef(null);

	const { t } = useTranslation('AdminAppSettings');
	const appSettingsCtx = useContext(AppConfigContext);

	const [confSaveResult, internalConfSave] = useFetch();
	const [adminPassSaveResult, internalPasswordSave] = useFetch();
	const [confResetResult, internalConfReset] = useFetch();
	const [confImportResult, internalConfImport] = useFetch();

	const compModes = useMemo(
		() => [
			{ key: 'point_based', label: t('form.comp_mode.point_based.label'), help: t('form.comp_mode.point_based.help') },
			{ key: 'time_based', label: t('form.comp_mode.time_based.label'), help: t('form.comp_mode.time_based.help') },
			{ key: 'rank_based', label: t('form.comp_mode.rank_based.label'), help: t('form.comp_mode.rank_based.help') },
			{
				key: 'boulder_1zone',
				label: t('form.comp_mode.boulder_1zone.label'),
				help: t('form.comp_mode.boulder_1zone.help'),
			},
		],
		[t]
	);

	const compModesHash = useMemo(
		() =>
			compModes.reduce((acc, item) => {
				acc[item.key] = item;
				return acc;
			}, {}),
		[compModes]
	);

	usePerms({ admin: true });

	useEffect(() => {
		if (themeColorChangeId.current) {
			clearTimeout(themeColorChangeId.current);
		}
		themeColorChangeId.current = setTimeout(() => {
			f7.setColorTheme(appThemeColor);
			themeColorChangeId.current = null;
		}, 200);
	}, [appThemeColor]);

	useOnTrue(!appSettingsCtx.loading, () => {
		setAppThemeColor(appSettingsCtx.appThemeColor);
		setCompMode(appSettingsCtx.compMode);
		setCompName(appSettingsCtx.compName);
		setDisplayResultsAfter(appSettingsCtx.displayResultsAfter);
		setDisplayResultsDuring(appSettingsCtx.displayResultsDuring);
		setDisplayStats(appSettingsCtx.displayStats);
		setEndDate(dateToYMDHMOrNull(appSettingsCtx.endDate));
		setStartDate(dateToYMDHMOrNull(appSettingsCtx.startDate));
		setcompIsOpen(appSettingsCtx.compIsOpen);
		setSkipZeroPoints(appSettingsCtx.skipZeroPoints);
	});

	const saveForm = useCallback(() => {
		internalConfSave('/api/app/config', {
			method: 'POST',
			body: {
				appThemeColor,
				compIsOpen,
				compMode,
				compName,
				displayResultsAfter,
				displayResultsDuring,
				displayStats,
				endDate: endDate ? dateYMDHMToIso(endDate) : null,
				skipZeroPoints,
				startDate: startDate ? dateYMDHMToIso(startDate) : null,
			},
		});
		appSettingsCtx.refresh();
	}, [
		appSettingsCtx,
		appThemeColor,
		compIsOpen,
		compMode,
		compName,
		displayResultsAfter,
		displayResultsDuring,
		displayStats,
		endDate,
		internalConfSave,
		skipZeroPoints,
		startDate,
	]);

	const passwordsAreEquals = useMemo(
		() => adminPassword.trim() === adminPassword2.trim(),
		[adminPassword, adminPassword2]
	);

	const saveAdminPass = useCallback(() => {
		if (passwordsAreEquals) {
			internalPasswordSave('/api/adminPassword', {
				method: 'POST',
				body: {
					password: adminPassword.trim(),
				},
			});
		}
	}, [adminPassword, internalPasswordSave, passwordsAreEquals]);

	const openConfImport = useCallback(() => {
		confFileImport.current.click();
	}, []);

	const importConf = useCallback(
		(e) => {
			internalConfImport('/api/app/import', {
				method: 'PUT',
				body: e.target.files[0],
			});
		},
		[internalConfImport]
	);

	const resetConf = useCallback(() => {
		f7.dialog.confirm(t('conf_reset_confirmation_dialog_text'), t('conf_reset_confirmation_dialog_title'), () => {
			internalConfReset('/api/app/reset', {
				method: 'PUT',
			});
		});
	}, [internalConfReset, t]);

	useOnTrue(confSaveResult.fulfilled, () => {
		f7.toast
			.create({
				text: t('conf_save_toast_text'),
				position: 'top',
				horizontalPosition: 'center',
				closeTimeout: 3000,
			})
			.open();
	});

	useOnTrue(adminPassSaveResult.fulfilled, () => {
		f7.toast
			.create({
				text: t('admin_pass_save_toast_text'),
				position: 'top',
				horizontalPosition: 'center',
				closeTimeout: 3000,
			})
			.open();
	});

	useOnTrue(confResetResult.fulfilled, () => {
		f7.dialog.alert(t('conf_reset_alert_text'), () => {
			window.location = '/';
		});
	});

	useOnTrue(confImportResult.fulfilled, () => {
		f7.dialog.alert(t('conf_import_alert_text'), () => {
			window.location = '/';
		});
	});

	const anyPending = useMemo(
		() =>
			appSettingsCtx.loading ||
			confSaveResult.pending ||
			adminPassSaveResult.pending ||
			confResetResult.pending ||
			confImportResult.pending,
		[
			appSettingsCtx.loading,
			confSaveResult.pending,
			adminPassSaveResult.pending,
			confResetResult.pending,
			confImportResult.pending,
		]
	);

	useOnTrue(appSettingsCtx.loading, () => {
		f7.preloader.show();
	});

	useOnTrue(!appSettingsCtx.loading, () => {
		f7.preloader.hide();
	});

	return (
		<Page>
			<CommonNavbar title={t('page_title')} />

			<BlockTitle>{t('titles.configuration')}</BlockTitle>
			{confSaveResult.rejected && <ErrorMessage centered error={confSaveResult.error} />}
			<List strong inset dividersIos>
				<ListInput
					label={t('form.competition_name.label')}
					type="text"
					placeholder={t('form.competition_name.placeholder')}
					disabled={anyPending}
					value={compName}
					onChange={(e) => setCompName(e.target.value)}
				/>
				<ListInput
					type="colorpicker"
					label={t('form.theme_color.label')}
					readonly
					value={{ hex: appThemeColor }}
					onColorPickerChange={(value) => setAppThemeColor(value.hex)}
					colorPickerParams={{
						targetEl: '.wheel-picker-target',
					}}
				>
					<Icon slot="media" f7="app_fill" color={appThemeColor} className="wheel-picker-target" />
				</ListInput>
				<ListInput
					label={t('form.comp_mode.label')}
					type="select"
					disabled={anyPending}
					value={compMode}
					onChange={(e) => setCompMode(e.target.value)}
				>
					{compModes.map(({ key, label }) => (
						<option key={`AppRightPanel-language-${key}`} value={key}>
							{label}
						</option>
					))}
				</ListInput>
				<ListItem>
					<Icon slot="media" f7="question_circle_fill" />
					{compModesHash[compMode] && compModesHash[compMode].help}
				</ListItem>
				<ListItem
					checkbox
					checked={skipZeroPoints}
					disabled={anyPending}
					onChange={() => setSkipZeroPoints((oldVal) => !oldVal)}
				>
					<span>
						<TooltipIcon text={t('form.skip_zero_points.tooltip')} />
						&nbsp;
						{t('form.skip_zero_points.label')}
					</span>
				</ListItem>
				<ListItem
					checkbox
					checked={compIsOpen}
					disabled={anyPending}
					onChange={() => setcompIsOpen((oldVal) => !oldVal)}
				>
					<span>
						<TooltipIcon text={t('form.comp_is_open.tooltip')} />
						&nbsp;
						{t('form.comp_is_open.label')}
					</span>
				</ListItem>
				<ListItem
					checkbox
					checked={displayResultsDuring && displayResultsAfter}
					disabled={anyPending}
					indeterminate={displayResultsDuring !== displayResultsAfter}
					onChange={() => {
						if (displayResultsDuring && displayResultsAfter) {
							setDisplayResultsDuring(false);
							setDisplayResultsAfter(false);
						} else {
							setDisplayResultsDuring(true);
							setDisplayResultsAfter(true);
						}
					}}
				>
					<span>
						<TooltipIcon text={t('form.display_results.tooltip')} />
						&nbsp;
						{t('form.display_results.label')}
					</span>
					<ul slot="root">
						<ListItem
							checkbox
							checked={displayResultsDuring}
							disabled={anyPending}
							onChange={() => setDisplayResultsDuring((oldVal) => !oldVal)}
						>
							{t('form.display_results_during.label')}
						</ListItem>
						<ListItem
							checkbox
							checked={displayResultsAfter}
							disabled={anyPending}
							onChange={() => setDisplayResultsAfter((oldVal) => !oldVal)}
						>
							{t('form.display_results_after.label')}
						</ListItem>
					</ul>
				</ListItem>
				<ListItem
					checkbox
					checked={displayStats}
					disabled={anyPending}
					onChange={() => setDisplayStats((oldVal) => !oldVal)}
				>
					<span>
						<TooltipIcon text={t('form.display_stats.tooltip')} />
						&nbsp;
						{t('form.display_stats.label')}
					</span>
				</ListItem>
				<ListItem>
					<span>
						<Checkbox
							disabled={anyPending}
							checked={startDate !== null}
							onChange={() => {
								setStartDate((oldVal) => (oldVal !== null ? null : nowYMDHM()));
							}}
						/>
						&nbsp;
						<TooltipIcon text={t('form.start_date.tooltip')} />
						&nbsp;
						{t('form.start_date.label')}
						{startDate !== null && (
							<Input
								type="datetime-local"
								disabled={anyPending}
								value={startDate || ''}
								onChange={(e) => setStartDate(e.target.value)}
							/>
						)}
					</span>
				</ListItem>
				<ListItem>
					<span>
						<Checkbox
							disabled={anyPending}
							checked={endDate !== null}
							onChange={() => {
								setEndDate((oldVal) => (oldVal !== null ? null : nowYMDHM(1)));
							}}
						/>
						&nbsp;
						<TooltipIcon text={t('form.end_date.tooltip')} />
						&nbsp;
						{t('form.end_date.label')}
						{endDate !== null && (
							<Input
								type="datetime-local"
								disabled={anyPending}
								value={endDate || ''}
								onChange={(e) => setEndDate(e.target.value)}
							/>
						)}
					</span>
				</ListItem>
				<ListItem>
					<Button fill disabled={anyPending} onClick={saveForm}>
						{t('form.save_button')}
					</Button>
				</ListItem>
			</List>

			<BlockTitle>{t('titles.admin_password')}</BlockTitle>
			{adminPassSaveResult.rejected && <ErrorMessage centered error={adminPassSaveResult.error} />}
			<List strong inset dividersIos>
				<ListInput
					label={t('form.admin_password.label')}
					type="password"
					placeholder={t('form.admin_password.placeholder')}
					disabled={anyPending}
					value={adminPassword}
					onChange={(e) => setAdminPassword(e.target.value)}
				/>
				<ListInput
					label={t('form.admin_password_confirm.label')}
					type="password"
					placeholder={t('form.admin_password_confirm.placeholder')}
					disabled={anyPending}
					value={adminPassword2}
					onChange={(e) => setAdminPassword2(e.target.value)}
					errorMessage={t('form.passwords_are_different')}
					errorMessageForce={!passwordsAreEquals}
				/>
				<ListItem>
					<Button fill disabled={anyPending} onClick={saveAdminPass}>
						{t('form.save_button')}
					</Button>
				</ListItem>
			</List>

			<BlockTitle>{t('titles.backup')}</BlockTitle>
			<Block>
				<Button fill color="green" href="/api/app/export" external target="_blank" disabled={anyPending}>
					{t('form.export_configuration.button')}
				</Button>
				<p>{t('form.export_configuration.explanation')}</p>
			</Block>

			<BlockTitle>{t('titles.restore')}</BlockTitle>
			{confImportResult.rejected && <ErrorMessage centered error={confImportResult.error} />}
			<Block>
				<Button fill color="orange" onClick={openConfImport} disabled={anyPending}>
					{t('form.import_configuration.button')}
				</Button>
				<input type="file" ref={confFileImport} style={{ display: 'none' }} onChange={importConf} accept=".gz" />
			</Block>

			<BlockTitle>{t('titles.reset')}</BlockTitle>
			{confResetResult.rejected && <ErrorMessage centered error={confResetResult.error} />}
			<Block>
				<Button fill color="red" onClick={resetConf} disabled={anyPending}>
					{t('form.reset_configuration.button')}
				</Button>
			</Block>
		</Page>
	);
}

export default AdminAppSettings;
