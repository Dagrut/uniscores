/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import { Button, Icon, Input } from 'framework7-react';
import React from 'react';
import PropTypes from 'prop-types';
import InputResultsGroupsKeysField from './InputResultsGroupsKeysField';
import InputResultsGroupsRoutesField from './InputResultsGroupsRoutesField';
import { useTranslation } from 'react-i18next';
import Tooltip from '../../components/Tooltip';

function ResultGroupItem(props) {
	const {
		anyPending,
		canMoveDown,
		canMoveUp,
		duplicate,
		editable,
		groupKeys,
		groupRoutes,
		id,
		moveItemDown,
		moveItemUp,
		name,
		removeGroupKey,
		setGroupKeys,
		setGroupName,
		setGroupRoutes,
		toggleEditable,
		toggleVisible,
		visible,
	} = props;

	const { t } = useTranslation('AdminResultsGroupsPage');

	return (
		<tr>
			<td>
				<div className="item-title item-label">{t('table_content.group_name')}</div>
				<Input type="text" value={name} disabled={anyPending} onChange={(e) => setGroupName(e.target.value)} />

				<div className="buttons-icons-row" style={{ marginTop: '10px' }}>
					<Tooltip
						tooltipText={t('table_content.buttons.duplicate')}
						component={Button}
						small
						disabled={anyPending}
						onClick={duplicate}
					>
						<Icon f7="square_on_square" />
					</Tooltip>
					<Tooltip
						tooltipText={t('table_content.buttons.visible')}
						component={Button}
						small
						disabled={anyPending}
						onClick={toggleVisible}
					>
						{visible ? <Icon f7="eye" /> : <Icon f7="eye_slash" />}
					</Tooltip>
					<Tooltip
						tooltipText={t('table_content.buttons.editable')}
						component={Button}
						small
						disabled={anyPending}
						onClick={toggleEditable}
					>
						{editable ? <Icon f7="pencil" /> : <Icon f7="pencil_slash" />}
					</Tooltip>
					<Tooltip
						tooltipText={t('table_content.buttons.move_down')}
						component={Button}
						style={{ visibility: canMoveDown ? 'visible' : 'hidden' }}
						small
						disabled={anyPending}
						onClick={moveItemDown}
					>
						<Icon f7="arrow_down" />
					</Tooltip>
					<Tooltip
						tooltipText={t('table_content.buttons.move_up')}
						component={Button}
						style={{ visibility: canMoveUp ? 'visible' : 'hidden' }}
						small
						disabled={anyPending}
						onClick={moveItemUp}
					>
						<Icon f7="arrow_up" />
					</Tooltip>
					<Tooltip
						tooltipText={t('table_content.buttons.delete')}
						component={Button}
						small
						color="red"
						disabled={anyPending}
						onClick={removeGroupKey}
					>
						<Icon f7="trash_circle" />
					</Tooltip>
				</div>
			</td>
			<td>
				<div className="item-title item-label">{t('table_content.group_keys_fields')}</div>
				<InputResultsGroupsKeysField
					groupId={id}
					value={groupKeys}
					disabled={anyPending}
					onChange={(newGroupKeys) => setGroupKeys(newGroupKeys)}
				/>
			</td>
			<td>
				<div className="item-title item-label">{t('table_content.group_routes_fields')}</div>
				<InputResultsGroupsRoutesField
					groupId={id}
					value={groupRoutes}
					disabled={anyPending}
					onChange={(newGroupRoutes) => setGroupRoutes(newGroupRoutes)}
				/>
			</td>
		</tr>
	);
}

ResultGroupItem.propTypes = {
	anyPending: PropTypes.bool.isRequired,
	canMoveDown: PropTypes.bool.isRequired,
	canMoveUp: PropTypes.bool.isRequired,
	duplicate: PropTypes.func.isRequired,
	editable: PropTypes.bool,
	id: PropTypes.string.isRequired,
	moveItemDown: PropTypes.func.isRequired,
	moveItemUp: PropTypes.func.isRequired,
	name: PropTypes.string.isRequired,
	removeGroupKey: PropTypes.func.isRequired,
	setGroupKeys: PropTypes.func.isRequired,
	setGroupName: PropTypes.func.isRequired,
	setGroupRoutes: PropTypes.func.isRequired,
	toggleEditable: PropTypes.func.isRequired,
	toggleVisible: PropTypes.func.isRequired,
	visible: PropTypes.bool,
	groupKeys: PropTypes.arrayOf(
		PropTypes.shape({
			field: PropTypes.string,
			value: PropTypes.string,
		})
	).isRequired,
	groupRoutes: PropTypes.arrayOf(
		PropTypes.shape({
			disciplineId: PropTypes.string,
			routeId: PropTypes.string,
		})
	),
};

ResultGroupItem.defaultProps = {
	groupRoutes: [],
	editable: false,
	visible: false,
};

export default ResultGroupItem;
