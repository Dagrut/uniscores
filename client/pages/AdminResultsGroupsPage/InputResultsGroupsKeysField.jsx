/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import { Chip, Input, f7 } from 'framework7-react';
import React, { useCallback, useContext, useEffect, useMemo, useRef } from 'react';
import PropTypes from 'prop-types';

import { useOnMount, useOnUnmount } from '../../js/reactHooks';

import UserProfileFieldsContext from '../../contexts/UserProfileFieldsContext';
import { useTranslation } from 'react-i18next';
import { objGet } from '../../js/utils';

function InputResultsGroupsKeysField(props) {
	const { groupId, onChange, value: groupKeys, disabled, ...extraProps } = props;

	const groupKeyAutocomplete = useRef(null);

	const userProfileFields = useContext(UserProfileFieldsContext);
	const { t } = useTranslation('AdminResultsGroupsPage');

	const userProfileFieldsSearchList = useMemo(() => {
		const assoc = userProfileFields.fields.assoc;

		const ret = Object.keys(assoc).reduce((acc, field) => {
			return acc.concat(
				Object.keys(assoc[field].values).map((value) => {
					const displayText = `${assoc[field].name}: ${assoc[field].values[value]}`;
					const valueObj = { field, value };
					return {
						displayText: `${assoc[field].name}: ${assoc[field].values[value]}`,
						lowerText: displayText.toLowerCase(),
						valueStr: JSON.stringify(valueObj),
						valueObj,
					};
				})
			);
		}, []);

		return ret;
	}, [userProfileFields.fields.assoc]);

	const internalSourceCb = useCallback(
		(query, render) => {
			if (query.length === 0) {
				render([]);
				return;
			}
			const lcQuery = query.toLowerCase();
			const results = userProfileFieldsSearchList
				.filter(({ lowerText }) => lowerText.indexOf(lcQuery) >= 0)
				.filter(
					({ valueObj }) =>
						groupKeys.findIndex(({ field, value }) => field == valueObj.field && value == valueObj.value) < 0
				);
			render(results);
		},
		[groupKeys, userProfileFieldsSearchList]
	);
	const onChangeInternalCb = useCallback(
		([value]) => {
			groupKeyAutocomplete.current.inputEl.value = '';
			onChange(groupKeys.concat(value.valueObj));
			setTimeout(() => groupKeyAutocomplete.current.inputEl.focus(), 150);
		},
		[groupKeys, onChange]
	);

	useOnMount(() => {
		if (!groupKeyAutocomplete.current) {
			groupKeyAutocomplete.current = f7.autocomplete.create({
				inputEl: `#inputResultsGroupsKeysField-${groupId}`,
				openIn: 'dropdown',
				valueProperty: 'valueStr',
				textProperty: 'displayText',
				highlightMatches: true,
				typeahead: true,
				source: internalSourceCb,
				on: {
					change: onChangeInternalCb,
				},
			});
		}
	});

	useEffect(() => {
		// To refresh the change callback when values are changing
		if (groupKeyAutocomplete.current) {
			groupKeyAutocomplete.current.off('change');
			groupKeyAutocomplete.current.on('change', onChangeInternalCb);
			groupKeyAutocomplete.current.params.source = internalSourceCb;
		}
	}, [groupKeys, internalSourceCb, onChange, onChangeInternalCb, userProfileFieldsSearchList]);

	useOnUnmount(() => {
		if (groupKeyAutocomplete.current) {
			groupKeyAutocomplete.current.destroy();
		}
	});

	const removeItem = useCallback(
		(id) => {
			if (!disabled) {
				const newGroupKeys = [...groupKeys];
				newGroupKeys.splice(id, 1);
				onChange(newGroupKeys);
			}
		},
		[disabled, groupKeys, onChange]
	);

	return (
		<div className="InputResultsGroupsKeysField">
			<span className="groups-list">
				{groupKeys.map(({ field, value }, arrayId) => (
					<React.Fragment key={`${groupId}-${field}-${value}`}>
						<Chip
							outline
							text={`${objGet(userProfileFields.fields.assoc, [field, 'name'], field)}: ${objGet(userProfileFields.fields.assoc, [field, 'values', value], value)}`}
							deleteable
							onDelete={() => removeItem(arrayId)}
						/>
						<br />
					</React.Fragment>
				))}
			</span>
			<Input
				type="text"
				name="filters"
				label={t('keys_field_input.label')}
				placeholder={t('keys_field_input.placeholder')}
				disabled={disabled}
				inputId={`inputResultsGroupsKeysField-${groupId}`}
				{...extraProps}
			/>
		</div>
	);
}

InputResultsGroupsKeysField.propTypes = {
	disabled: PropTypes.bool,
	groupId: PropTypes.string.isRequired,
	onChange: PropTypes.func,
	value: PropTypes.arrayOf(
		PropTypes.shape({
			field: PropTypes.string.isRequired,
			value: PropTypes.string.isRequired,
		})
	).isRequired,
};

InputResultsGroupsKeysField.defaultProps = {
	disabled: false,
	onChange: () => {},
};

export default InputResultsGroupsKeysField;
