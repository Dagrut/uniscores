/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import { Chip, Input, f7 } from 'framework7-react';
import React, { useCallback, useContext, useEffect, useMemo, useRef } from 'react';
import PropTypes from 'prop-types';

import { useOnMount, useOnUnmount } from '../../js/reactHooks';

import { useTranslation } from 'react-i18next';
import { objGet } from '../../js/utils';
import AdminResultsGroupsContext from './AdminResultsGroupsContext';

function InputResultsGroupsRoutesField(props) {
	const { groupId, onChange, value: groupRoutes, disabled, ...extraProps } = props;

	const groupRouteAutocomplete = useRef(null);

	const { disciplines } = useContext(AdminResultsGroupsContext);
	const { t } = useTranslation('AdminResultsGroupsPage');

	const getDisplayTextFor = useCallback(
		(disciplineId, routeId = null) => {
			if (routeId) {
				return `${objGet(disciplines, [disciplineId, 'name'], disciplineId)}: ${objGet(disciplines, [disciplineId, 'routes', routeId, 'name'], routeId)}`;
			}

			return `${objGet(disciplines, [disciplineId, 'name'], disciplineId)}: *`;
		},
		[disciplines]
	);

	const disciplinesSearchList = useMemo(() => {
		const ret = [];
		Object.keys(disciplines).forEach((disciplineId) => {
			const displayText = getDisplayTextFor(disciplineId);
			const valueObj = { disciplineId };
			ret.push({
				displayText,
				lowerText: displayText.toLowerCase(),
				valueStr: JSON.stringify(valueObj),
				valueObj,
			});

			Object.keys(disciplines[disciplineId].routes).forEach((routeId) => {
				const displayText = getDisplayTextFor(disciplineId, routeId);
				const valueObj = { disciplineId, routeId };
				ret.push({
					displayText,
					lowerText: displayText.toLowerCase(),
					valueStr: JSON.stringify(valueObj),
					valueObj,
				});
			});
		});

		return ret;
	}, [disciplines, getDisplayTextFor]);

	const internalSourceCb = useCallback(
		(query, render) => {
			if (query.length === 0) {
				render([]);
				return;
			}
			const lcQuery = query.toLowerCase();
			const results = disciplinesSearchList
				.filter(({ lowerText }) => lowerText.indexOf(lcQuery) >= 0)
				.filter(
					({ valueObj }) =>
						groupRoutes.findIndex(
							({ disciplineId, routeId }) => disciplineId == valueObj.disciplineId && routeId == valueObj.routeId
						) < 0
				);
			render(results);
		},
		[disciplinesSearchList, groupRoutes]
	);

	const onChangeInternalCb = useCallback(
		([value]) => {
			groupRouteAutocomplete.current.inputEl.value = '';
			onChange(groupRoutes.concat(value.valueObj));
			setTimeout(() => {
				groupRouteAutocomplete.current.inputEl.focus();
			}, 100);
		},
		[groupRoutes, onChange]
	);

	useOnMount(() => {
		if (!groupRouteAutocomplete.current) {
			groupRouteAutocomplete.current = f7.autocomplete.create({
				inputEl: `#inputResultsGroupsRoutesField-${groupId}`,
				openIn: 'dropdown',
				valueProperty: 'valueStr',
				textProperty: 'displayText',
				highlightMatches: true,
				typeahead: true,
				source: internalSourceCb,
				on: {
					change: onChangeInternalCb,
				},
			});
		}
	});

	useEffect(() => {
		// To refresh the change callback when values are changing
		if (groupRouteAutocomplete.current) {
			groupRouteAutocomplete.current.off('change');
			groupRouteAutocomplete.current.on('change', onChangeInternalCb);
			groupRouteAutocomplete.current.params.source = internalSourceCb;
		}
	}, [groupRoutes, onChange, disciplinesSearchList, onChangeInternalCb, internalSourceCb]);

	useOnUnmount(() => {
		if (groupRouteAutocomplete.current) {
			groupRouteAutocomplete.current.destroy();
		}
	});

	const removeItem = useCallback(
		(id) => {
			if (!disabled) {
				const newGroupRoutes = [...groupRoutes];
				newGroupRoutes.splice(id, 1);
				onChange(newGroupRoutes);
			}
		},
		[disabled, groupRoutes, onChange]
	);

	return (
		<div className="InputResultsGroupsRoutesField">
			<span className="groups-list">
				{groupRoutes.map(({ disciplineId, routeId = null }, arrayId) => {
					return (
						<React.Fragment key={`${groupId}-${disciplineId}-${routeId}`}>
							<Chip
								outline
								text={getDisplayTextFor(disciplineId, routeId)}
								deleteable
								onDelete={() => removeItem(arrayId)}
							/>
							<br />
						</React.Fragment>
					);
				})}
			</span>
			<Input
				type="text"
				name="filters"
				label={t('routes_field_input.label')}
				placeholder={t('routes_field_input.placeholder')}
				disabled={disabled}
				inputId={`inputResultsGroupsRoutesField-${groupId}`}
				{...extraProps}
			/>
		</div>
	);
}

InputResultsGroupsRoutesField.propTypes = {
	disabled: PropTypes.bool,
	groupId: PropTypes.string.isRequired,
	onChange: PropTypes.func,
	value: PropTypes.arrayOf(
		PropTypes.shape({
			disciplineId: PropTypes.string.isRequired,
			routeId: PropTypes.string,
		})
	).isRequired,
};

InputResultsGroupsRoutesField.defaultProps = {
	disabled: false,
	onChange: () => {},
};

export default InputResultsGroupsRoutesField;
