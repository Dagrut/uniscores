/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { useCallback, useContext, useMemo, useRef, useState } from 'react';
import { Block, BlockTitle, Button, Icon, Page, Preloader, Searchbar, f7 } from 'framework7-react';

import CommonNavbar from '../../components/CommonNavbar';
import ErrorMessage from '../../components/ErrorMessage';
import useFetch from '../../js/useFetch';
import { useOnTrue, usePerms } from '../../js/reactHooks';

import './AdminResultsGroupsPage.less';
import { clone, indexArrayBy, quickCipher, quickDecipher, strRemoveDiacritics, uniqId } from '../../js/utils';
import ResultsGroupsGenerationPopup, {
	useResultsGroupGenerationPopupState,
} from '../../components/popups/ResultsGroupsGenerationPopup';
import UserProfileFieldsContext from '../../contexts/UserProfileFieldsContext';
import { useTranslation } from 'react-i18next';
import AppConfigContext from '../../contexts/AppConfigContext';
import ResultGroupItem from './ResultGroupItem';
import { AdminResultsGroupsContextProvider } from './AdminResultsGroupsContext';

const RESULTS_GROUPS_EXPORT_KEY =
	'px[x]w`BjZ59xUxi29w0SylyoZ#cY?stXnVQ##V:z"n=bM/=oMpJx9lvesH=`j5fgF@Oilza$81~l#;L%{,YT>VJ]M*g7"';

const groupKeysAreEqual = (gk1, gk2) => {
	if (gk1.length !== gk2.length) {
		return false;
	}

	const idxGk2 = indexArrayBy('field', gk2);
	const equals = gk1.every(({ field, value }) => idxGk2[field] && idxGk2[field].value === value);

	return equals;
};

function AdminResultsGroupsPage() {
	usePerms({ admin: true });

	const [filter, setFilter] = useState('');
	const [groups, setGroups] = useState([]);
	const resultsGroupsImportAddInput = useRef(null);
	const resultsGroupsImportReplaceInput = useRef(null);

	const [resultsGroups] = useFetch('/api/resultsGroups');
	const [resultsGroupsUpdateResp, internalResultsGroupsUpdate] = useFetch();
	const [exportResult, internalExport] = useFetch();

	const userProfileFields = useContext(UserProfileFieldsContext);
	const { compName } = useContext(AppConfigContext);
	const { t } = useTranslation('AdminResultsGroupsPage');

	const generationPopupState = useResultsGroupGenerationPopupState();

	const resultsGroupsUpdate = useCallback(() => {
		internalResultsGroupsUpdate('/api/resultsGroups', {
			method: 'POST',
			body: groups,
		});
	}, [groups, internalResultsGroupsUpdate]);

	useOnTrue(resultsGroups.fulfilled, () => {
		setGroups(resultsGroups.json);
	});

	const onSearchChange = useCallback((e) => {
		setFilter(e.target.value);
	}, []);

	const generateDefaultResultsGroups = useCallback(
		(fields) => {
			const [currentField, ...otherFields] = fields;
			if (currentField === undefined)
				return [
					{
						id: uniqId(),
						name: '',
						groupKeys: [],
					},
				];

			const otherValues = generateDefaultResultsGroups(otherFields);

			const ret = Object.keys(userProfileFields.fields.assoc[currentField].values).reduce((acc, value) => {
				otherValues.forEach(({ name, groupKeys }) => {
					let newName = name ? `${name} - ` : '';
					newName += userProfileFields.fields.assoc[currentField].name;
					newName += ':';
					newName += userProfileFields.fields.assoc[currentField].values[value];
					acc.push({
						id: uniqId(),
						name: newName,
						groupKeys: [...groupKeys, { field: currentField, value }],
					});
				});

				return acc;
			}, []);

			return ret;
		},
		[userProfileFields.fields.assoc]
	);

	const moveItemUp = useCallback((arrayId) => {
		setGroups((oldGroups) => {
			const newGroupKeys = [...oldGroups];
			const [item] = newGroupKeys.splice(arrayId, 1);
			newGroupKeys.splice(arrayId - 1, 0, item);
			return newGroupKeys;
		});
	}, []);

	const moveItemDown = useCallback((arrayId) => {
		setGroups((oldGroups) => {
			const newGroupKeys = [...oldGroups];
			const [item] = newGroupKeys.splice(arrayId, 1);
			newGroupKeys.splice(arrayId + 1, 0, item);
			return newGroupKeys;
		});
	}, []);

	const removeGroupKey = useCallback((arrayId) => {
		setGroups((oldGroups) => {
			const newGroupKeys = [...oldGroups];
			newGroupKeys.splice(arrayId, 1);
			return newGroupKeys;
		});
	}, []);

	const addEmptyGroup = useCallback(() => {
		setGroups((oldGroups) => {
			const newGroup = {
				id: uniqId(),
				name: '',
				visible: true,
				editable: true,
				groupKeys: [],
				groupRoutes: [],
			};

			return oldGroups.concat(newGroup);
		});
	}, []);

	const anyPending = useMemo(() => resultsGroupsUpdateResp.pending, [resultsGroupsUpdateResp.pending]);

	const generationFieldsList = useMemo(
		() =>
			Object.keys(userProfileFields.fields.assoc).map((field) => ({
				id: field,
				name: userProfileFields.fields.assoc[field].name,
			})),
		[userProfileFields.fields.assoc]
	);

	const runGeneration = useCallback(
		(fieldsList) => {
			const allGroups = generateDefaultResultsGroups(fieldsList);

			const notCreatedGroups = allGroups.filter(
				({ groupKeys: gk1 }) => groups.findIndex(({ groupKeys: gk2 }) => groupKeysAreEqual(gk1, gk2)) < 0
			);

			setGroups((oldGroups) => [...oldGroups, ...notCreatedGroups]);
		},
		[generateDefaultResultsGroups, groups]
	);

	const exportCurrent = useCallback(() => {
		const uid = uniqId();
		const nowStr = new Date().toISOString().replace(/:/g, '-');

		const data = quickCipher(RESULTS_GROUPS_EXPORT_KEY, JSON.stringify(groups));

		const filteredCompName = strRemoveDiacritics(compName).replace(/[^a-zA-Z0-9_]+/g, '-');
		const suggestedName = `RG-${filteredCompName}-${nowStr}`;

		f7.dialog.prompt(
			t('export_dialog.text'),
			t('export_dialog.title'),
			(name) => {
				internalExport(`/api/export/${uid}`, {
					method: 'POST',
					body: {
						name: `${name}.resgrp`,
						mimeType: 'application/octet-stream',
						data,
					},
				});
			},
			() => {},
			suggestedName
		);
	}, [compName, groups, internalExport, t]);

	useOnTrue(exportResult.fulfilled, () => {
		const targetUrl = new URL(exportResult.inputs.url, window.location.href);
		window.location = targetUrl.toString();
	});

	const openImportAdd = useCallback(() => {
		resultsGroupsImportAddInput.current.click();
	}, []);

	const openImportReplace = useCallback(() => {
		resultsGroupsImportReplaceInput.current.click();
	}, []);

	const importAddResultsGroups = useCallback(
		(e) => {
			const file = e.target.files[0];
			const fr = new FileReader();
			fr.onload = () => {
				try {
					const deciphered = quickDecipher(RESULTS_GROUPS_EXPORT_KEY, fr.result);
					const newGroups = JSON.parse(deciphered);
					setGroups((oldGroups) => [...oldGroups, ...newGroups]);
					f7.dialog.alert(t('import_ok'));
				} catch (e) {
					f7.dialog.alert(t('import_invalid_file'));
				}
			};
			fr.readAsText(file);
		},
		[t]
	);

	const importReplaceResultsGroups = useCallback(
		(e) => {
			const file = e.target.files[0];
			const fr = new FileReader();
			fr.onload = () => {
				try {
					const deciphered = quickDecipher(RESULTS_GROUPS_EXPORT_KEY, fr.result);
					const newGroups = JSON.parse(deciphered);
					setGroups(newGroups);
					f7.dialog.alert(t('import_ok'));
				} catch (e) {
					f7.dialog.alert(t('import_invalid_file'));
				}
			};
			fr.readAsText(file);
		},
		[t]
	);

	const filteredGroups = useMemo(() => {
		const cleanFilter = strRemoveDiacritics(filter).toLowerCase();

		const ret = groups
			.map((group, arrayId) => ({ ...group, arrayId }))
			.filter((group) => {
				if (!filter) {
					return true;
				}

				const cleanedName = strRemoveDiacritics(group.name).toLowerCase();
				const allowed = cleanedName.indexOf(cleanFilter) >= 0;

				return allowed;
			});

		return ret;
	}, [filter, groups]);

	return (
		<Page className="AdminResultsGroupsPage">
			<CommonNavbar title={t('page_title')} />

			<BlockTitle>{t('resultgroups_blocktitle')}</BlockTitle>
			{resultsGroups.pending && <Preloader />}
			{resultsGroups.rejected && <ErrorMessage error={resultsGroups.error} centered />}
			{resultsGroups.fulfilled && (
				<>
					<Block className="buttons-row">
						<Button fill onClick={generationPopupState.openPopup}>
							<Icon f7="gear" />
							&nbsp;
							{t('open_generation_popup')}
						</Button>
						<Button fill onClick={exportCurrent}>
							<Icon f7="archivebox" />
							&nbsp;
							{t('export_button')}
						</Button>
						<Button fill onClick={openImportAdd}>
							<Icon f7="plus" />
							&nbsp;
							{t('import_add_button')}
						</Button>
						<Button fill onClick={openImportReplace}>
							<Icon f7="arrow_uturn_down_circle" />
							&nbsp;
							{t('import_replace_button')}
						</Button>
						<input
							type="file"
							ref={resultsGroupsImportAddInput}
							style={{ display: 'none' }}
							onChange={importAddResultsGroups}
							accept=".resgrp"
						/>
						<input
							type="file"
							ref={resultsGroupsImportReplaceInput}
							style={{ display: 'none' }}
							onChange={importReplaceResultsGroups}
							accept=".resgrp"
						/>
					</Block>
					{exportResult.rejected && (
						<Block>
							<ErrorMessage error={exportResult.error} />
						</Block>
					)}

					<BlockTitle>{t('filter_blocktitle')}</BlockTitle>
					<Block>
						<Searchbar
							disableButtonText={t('filter_searchbar.cancel')}
							placeholder={t('filter_searchbar.placeholder')}
							clearButton={true}
							onChange={onSearchChange}
							onSearchbarClear={() => setFilter('')}
							onSearchbarDisable={() => setFilter('')}
						/>
					</Block>

					<ResultsGroupsGenerationPopup
						fields={generationFieldsList}
						onSubmit={runGeneration}
						state={generationPopupState}
					/>

					<div className="data-table">
						<table>
							<thead>
								<tr>
									<th className="label-cell">{t('table_cols.group_name')}</th>
									<th className="label-cell">{t('table_cols.group_keys_fields')}</th>
									<th className="label-cell">{t('table_cols.group_routes_fields')}</th>
									{/* <th className="label-cell">{t('table_cols.group_manage')}</th> */}
								</tr>
							</thead>
							<tbody>
								<AdminResultsGroupsContextProvider>
									{filteredGroups.map(({ arrayId, ...group }) => (
										<ResultGroupItem
											key={group.id}
											{...group}
											anyPending={anyPending}
											canMoveDown={arrayId !== groups.length - 1}
											canMoveUp={arrayId !== 0}
											moveItemDown={() => moveItemDown(arrayId)}
											moveItemUp={() => moveItemUp(arrayId)}
											removeGroupKey={() => removeGroupKey(arrayId)}
											setGroupKeys={(newGroupKeys) =>
												setGroups((oldGroups) => {
													const newGroups = [...oldGroups];
													newGroups[arrayId].groupKeys = newGroupKeys;
													return newGroups;
												})
											}
											toggleVisible={() =>
												setGroups((oldGroups) => {
													const newGroups = [...oldGroups];
													newGroups[arrayId].visible = !newGroups[arrayId].visible;
													return newGroups;
												})
											}
											toggleEditable={() =>
												setGroups((oldGroups) => {
													const newGroups = [...oldGroups];
													newGroups[arrayId].editable = !newGroups[arrayId].editable;
													return newGroups;
												})
											}
											setGroupRoutes={(newGroupRoutes) =>
												setGroups((oldGroups) => {
													const newGroups = [...oldGroups];
													newGroups[arrayId].groupRoutes = newGroupRoutes;
													return newGroups;
												})
											}
											setGroupName={(newName) =>
												setGroups((oldGroups) => {
													const newGroups = [...oldGroups];
													newGroups[arrayId].name = newName;
													return newGroups;
												})
											}
											duplicate={() =>
												setGroups((oldGroups) => {
													const newGroupKeys = [...oldGroups];
													const newGroup = {
														id: uniqId(),
														name: `${group.name} (2)`,
														groupKeys: clone(group.groupKeys),
														groupRoutes: clone(group.groupRoutes),
													};

													newGroupKeys.splice(arrayId + 1, 0, newGroup);
													return newGroupKeys;
												})
											}
										/>
									))}
								</AdminResultsGroupsContextProvider>
							</tbody>
						</table>
					</div>
					<Block className="buttons-row">
						<Button fill disabled={anyPending} onClick={addEmptyGroup}>
							{t('buttons.add')}
						</Button>
						<Button fill disabled={anyPending} onClick={resultsGroupsUpdate}>
							{t('buttons.save')}
							{resultsGroupsUpdateResp.pending && (
								<>
									&nbsp;
									<Preloader />
								</>
							)}
						</Button>
					</Block>
				</>
			)}
		</Page>
	);
}

export default AdminResultsGroupsPage;
