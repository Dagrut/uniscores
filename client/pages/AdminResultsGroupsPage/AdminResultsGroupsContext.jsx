/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { createContext, useMemo } from 'react';
import PropTypes from 'prop-types';
import useFetch from '../../js/useFetch';

const defaultContext = {
	disciplines: {},
	loading: true,
};

const AdminResultsGroupsContext = createContext(defaultContext);

export function AdminResultsGroupsContextProvider(props) {
	const { children } = props;

	const [disciplinesResponse] = useFetch('/api/trials');

	const context = useMemo(() => {
		const ret = { ...defaultContext };

		if (disciplinesResponse.pending || disciplinesResponse.init) {
			ret.loading = true;
		} else if (disciplinesResponse.fulfilled) {
			ret.disciplines = disciplinesResponse.json;
		}

		return ret;
	}, [disciplinesResponse.fulfilled, disciplinesResponse.init, disciplinesResponse.json, disciplinesResponse.pending]);

	return <AdminResultsGroupsContext.Provider value={context}>{children}</AdminResultsGroupsContext.Provider>;
}

AdminResultsGroupsContextProvider.propTypes = {
	children: PropTypes.node.isRequired,
};

export default AdminResultsGroupsContext;
