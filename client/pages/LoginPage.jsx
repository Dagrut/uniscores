/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { useCallback, useContext, useMemo, useRef, useState } from 'react';
import { Button, List, ListInput, ListItem, LoginScreenTitle, Page, Preloader, f7 } from 'framework7-react';

import useFetch from '../js/useFetch';
import { useOnTrue, useOnUnmount } from '../js/reactHooks';
import SessionContext from '../contexts/SessionContext';
import ErrorMessage from '../components/ErrorMessage';
import { useTranslation } from 'react-i18next';
import PasswordInput from '../components/PasswordInput';

function LoginPage() {
	const usernameAutocomplete = useRef(null);
	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');
	const sessionContext = useContext(SessionContext);
	const { t } = useTranslation('LoginPage');

	const [users] = useFetch('/api/users');

	const [loginResponse, login] = useFetch();

	const connectUser = useCallback(() => {
		login('/api/auth/login', { method: 'POST', body: { username, password } });
	}, [login, password, username]);

	useOnTrue(loginResponse.fulfilled, () => {
		if (!loginResponse.json.password && password) {
			f7.store.dispatch('setPassword', password);
		}
		setUsername('');
		setPassword('');
		sessionContext.refresh(() => {
			f7.loginScreen.close();
		});
	});

	const allUsernames = useMemo(() => Object.values(users.json || {}).map((user) => user.username), [users.json]);

	const currentUserHavePassword = useMemo(() => {
		const currentUser = Object.values(users.json || {}).find((user) => user.username === username);

		if ((currentUser && currentUser.password) || username === 'admin') {
			return true;
		}

		return false;
	}, [username, users.json]);

	useOnTrue(users.fulfilled, () => {
		if (!usernameAutocomplete.current) {
			usernameAutocomplete.current = f7.autocomplete.create({
				inputEl: '#username-autocomplete',
				openIn: 'dropdown',
				source(query, render) {
					if (query.length === 0) {
						render([]);
						return;
					}
					const lcQuery = query.toLowerCase();
					const results = allUsernames.filter((username) => username.toLowerCase().indexOf(lcQuery) >= 0);
					render(results);
				},
			});
		}
	});

	useOnUnmount(() => {
		if (usernameAutocomplete.current) {
			usernameAutocomplete.current.destroy();
		}
	});

	return (
		<Page
			loginScreen
			onSubmit={(e) => {
				e.preventDefault();
				e.stopPropagation();
				connectUser();
			}}
		>
			<LoginScreenTitle>{t('page_title')}</LoginScreenTitle>
			<List
				form
				onSubmit={(e) => {
					e.preventDefault();
					e.stopPropagation();
					connectUser();
				}}
			>
				{users.pending && <Preloader />}
				<ListInput
					type="text"
					name="username"
					label={t('form.username.label')}
					placeholder={t('form.username.placeholder')}
					inputId="username-autocomplete"
					autocomplete="off"
					value={username}
					onInput={(e) => setUsername(e.target.value)}
				/>
				<PasswordInput
					name="password"
					label={t('form.password.label')}
					placeholder={t('form.password.placeholder')}
					value={password}
					onInput={(e) => setPassword(e.target.value)}
					style={{
						display: currentUserHavePassword ? 'block' : 'none',
					}}
				/>
				{loginResponse.rejected && (
					<ListItem>
						<ErrorMessage error={loginResponse.error} />
					</ListItem>
				)}
				<ListItem style={{ marginTop: '30px' }}>
					<Button type="submit" onClick={connectUser}>
						{t('form.login_button')}
					</Button>
				</ListItem>
				<ListItem>
					<Button type="button" loginScreenClose>
						{t('form.close_button')}
					</Button>
				</ListItem>
			</List>
		</Page>
	);
}

export default LoginPage;
