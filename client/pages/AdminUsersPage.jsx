/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { useCallback, useContext, useEffect, useMemo, useRef, useState } from 'react';
import { Page, BlockTitle, f7, Button, Icon, Block, Searchbar, Card, Preloader, Link } from 'framework7-react';
import { useErrorTranslator, useOnTrue, usePerms } from '../js/reactHooks';
import CommonNavbar from '../components/CommonNavbar';
import useFetch from '../js/useFetch';
import Tooltip from '../components/Tooltip';
import Pagination, { usePaginationState } from '../components/Pagination';
import UserFormPopup, { useUserFormPopupState } from '../components/popups/UserFormPopup';
import { strRemoveDiacritics, uniqId } from '../js/utils';

import UserProfileFieldsContext from '../contexts/UserProfileFieldsContext';
import { useTranslation } from 'react-i18next';
import { allUsersToCsv, parseCsvToArray, csvArrayToIndexedObjects } from '../js/csv';
import AppConfigContext from '../contexts/AppConfigContext';
import ErrorMessage from '../components/ErrorMessage';
import UsersImporter from '../components/UsersImporter';

function AdminUsersPage() {
	usePerms({ admin: true });

	const [toImport, setToImport] = useState([]);
	const [filter, setFilter] = useState('');
	const [users, setUsers] = useState({});
	const usersImportInput = useRef(null);

	const [initialUsers] = useFetch('/api/users');

	const [addUserResponse, internalAddUser] = useFetch();
	const [editUserResponse, internalEditUser] = useFetch();
	const [deleteUserResponse, internalDeleteUser] = useFetch();
	const [toExportUsers, internalFetchExportUsers] = useFetch();
	const [exportResult, internalExport] = useFetch();

	const extraProfileFields = useContext(UserProfileFieldsContext);
	const { compName } = useContext(AppConfigContext);
	const { t } = useTranslation('AdminUsersPage');
	const errorTs = useErrorTranslator();

	const fetchExportUsers = useCallback(() => {
		internalFetchExportUsers('/api/users');
	}, [internalFetchExportUsers]);

	const extraProfilesFieldsVassoc = useMemo(() => {
		const ret = [
			{ key: 'lastname', name: t('form.lastname.label') },
			{ key: 'firstname', name: t('form.firstname.label') },
			...extraProfileFields.fields.list.map(({ key, name, values }) => ({
				key,
				name,
				values: values.reduce((acc, value) => {
					acc[value.key] = value.name;
					return acc;
				}, {}),
			})),
		];
		return ret;
	}, [extraProfileFields.fields.list, t]);

	const userFormPopupState = useUserFormPopupState();

	useEffect(() => {
		if (initialUsers.fulfilled) {
			setUsers(initialUsers.json);
		}
	}, [initialUsers.fulfilled, initialUsers.json]);

	useOnTrue(addUserResponse.rejected, () => {
		f7.dialog.alert(t('errors.add', { message: errorTs(addUserResponse.error.message) }));
	});

	useOnTrue(editUserResponse.rejected, () => {
		f7.dialog.alert(t('errors.edit', { message: errorTs(editUserResponse.error.message) }));
	});

	useOnTrue(deleteUserResponse.rejected, () => {
		f7.dialog.alert(t('errors.delete', { message: errorTs(deleteUserResponse.error.message) }));
	});

	useOnTrue(addUserResponse.pending || editUserResponse.pending || deleteUserResponse.pending, () => {
		f7.preloader.show();
	});

	useOnTrue(!(addUserResponse.pending || editUserResponse.pending || deleteUserResponse.pending), () => {
		f7.preloader.hide();
	});

	useOnTrue(addUserResponse.fulfilled, () => {
		f7.toast
			.create({
				text: t('success.added'),
				position: 'top',
				horizontalPosition: 'right',
				closeTimeout: 2000,
			})
			.open();
		userFormPopupState.close();
	});

	useOnTrue(editUserResponse.fulfilled, () => {
		f7.toast
			.create({
				text: t('success.updated'),
				position: 'top',
				horizontalPosition: 'right',
				closeTimeout: 2000,
			})
			.open();
		userFormPopupState.close();
	});

	useOnTrue(deleteUserResponse.fulfilled, () => {
		f7.dialog.close();
		f7.toast
			.create({
				text: t('success.deleted'),
				position: 'top',
				horizontalPosition: 'right',
				closeTimeout: 2000,
			})
			.open();
	});

	const deleteUser = useCallback(
		(id) => {
			internalDeleteUser(`/api/users/${id}`, { method: 'DELETE' });
			setUsers((oldUsers) => {
				const newUsers = { ...oldUsers };
				delete newUsers[id];
				return newUsers;
			});
		},
		[internalDeleteUser]
	);

	const onSearchChange = useCallback((e) => {
		setFilter(e.target.value);
	}, []);

	const saveUser = useCallback(
		(user, newUser) => {
			const body = {
				id: user.id,
				username: `${user.profile.firstname} ${user.profile.lastname}`,
				password: user.password || null,
				profile: user.profile,
			};

			if (newUser) {
				internalAddUser('/api/users', {
					method: 'POST',
					body,
				});
			} else {
				internalEditUser(`/api/users/${user.id}`, {
					method: 'PATCH',
					body,
				});
			}
		},
		[internalAddUser, internalEditUser]
	);

	const editUserScores = useCallback((id) => {
		f7.views.main.router.navigate(`/scores/input/${id}`);
	}, []);

	const askDeleteUser = useCallback(
		(id) => {
			f7.dialog.confirm(t('delete_dialog.confirm'), function () {
				deleteUser(id);
				f7.dialog.preloader(t('delete_dialog.deleting'));
			});
		},
		[deleteUser, t]
	);

	const filteredUsersList = useMemo(() => {
		const arrayFilter = strRemoveDiacritics(filter).toLowerCase().split(/ +/g);

		const usersList = Object.values(users).filter((user) => {
			let allowed = false;

			if (!filter) {
				return true;
			}

			const searchTargets = extraProfilesFieldsVassoc.map(({ key, name, values }) => {
				let searchTarget = `${name}:${user.profile[key] || '_'}`;
				if (values) {
					searchTarget = `${name}:${values[user.profile[key]] || '_'}`;
				}
				return strRemoveDiacritics(searchTarget).replace(/ /g, '_').toLowerCase();
			});

			allowed ||= arrayFilter.every(
				(subFilter) => searchTargets.findIndex((searchTarget) => searchTarget.indexOf(subFilter) >= 0) >= 0
			);

			return allowed;
		});

		return usersList;
	}, [extraProfilesFieldsVassoc, filter, users]);

	const pagination = usePaginationState(filteredUsersList.length);

	const renderedUsers = useMemo(() => {
		const displayedUsersList = filteredUsersList.slice(pagination.offset, pagination.offset + pagination.perPage);

		return (
			<table>
				<thead>
					<tr>
						{extraProfilesFieldsVassoc.map(({ key, name }) => (
							<th key={`AdminUsersPage-users-thead-${key}`} className="label-cell">
								{name}
								&nbsp;
								<Link href="/admin/usersProfileFields">
									<Icon f7="pencil_circle" />
								</Link>
							</th>
						))}
						<th className="label-cell" style={{ width: '300px' }}>
							{t('form.modify_column')}
						</th>
					</tr>
				</thead>
				<tbody>
					{displayedUsersList.map((user) => {
						return (
							<tr key={`AdminUsersPage-users-tr-${user.id}`}>
								{extraProfilesFieldsVassoc.map(({ key, values }) => (
									<td key={`AdminUsersPage-users-td-${user.id}-${key}`} className="label-cell">
										{(values && values[user.profile[key]]) || user.profile[key]}
									</td>
								))}
								<td className="buttons-row" style={{ width: '300px' }}>
									<Tooltip
										component={Button}
										color="blue"
										small
										onClick={() => userFormPopupState.open(user)}
										tooltipText={t('form.modify_tooltips.edit_user')}
									>
										<Icon f7="pencil_circle" />
									</Tooltip>
									<Tooltip
										component={Button}
										color="green"
										small
										onClick={() => editUserScores(user.id)}
										tooltipText={t('form.modify_tooltips.edit_user_scores')}
									>
										<Icon f7="list_number" />
									</Tooltip>
									<Tooltip
										component={Button}
										color="red"
										small
										onClick={() => askDeleteUser(user.id)}
										tooltipText={t('form.modify_tooltips.delete_user')}
									>
										<Icon f7="trash_circle" />
									</Tooltip>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		);
	}, [
		filteredUsersList,
		pagination.offset,
		pagination.perPage,
		extraProfilesFieldsVassoc,
		t,
		userFormPopupState,
		editUserScores,
		askDeleteUser,
	]);

	const onPasswordReset = useCallback((userId) => {
		setUsers((oldUsers) => {
			const newUsers = { ...oldUsers };
			const user = oldUsers[userId];
			if (user) {
				user.password = false;
			}
			return newUsers;
		});
	}, []);

	const addUser = useCallback(
		(user) => {
			const newUser = !user.id;
			if (newUser) {
				user.id = uniqId();
			}
			setUsers((oldUsers) => {
				const newUsers = { ...oldUsers };
				const oldUser = oldUsers[user.id] || {};
				const newUser = {
					...oldUser,
					...user,
				};

				if (user.password) {
					newUser.password = true;
				} else if (oldUser.id) {
					newUser.password = oldUser.password;
				} else {
					newUser.password = false;
				}

				newUsers[user.id] = newUser;

				return newUsers;
			});

			saveUser(user, newUser);
		},
		[saveUser]
	);

	useOnTrue(toExportUsers.fulfilled, () => {
		const csv = allUsersToCsv(
			toExportUsers.json,
			extraProfilesFieldsVassoc.map(({ key }) => key)
		);

		const uid = uniqId();
		const nowStr = new Date().toISOString().replace(/:/g, '-');

		const filteredCompName = compName.replace(/[^a-zA-Z0-9_-]/g, '-');
		internalExport(`/api/export/${uid}`, {
			method: 'POST',
			body: {
				name: `Users-${filteredCompName}-${nowStr}.csv`,
				mimeType: 'text/csv',
				data: csv,
			},
		});
	});

	useOnTrue(exportResult.fulfilled, () => {
		const targetUrl = new URL(exportResult.inputs.url, window.location.href);
		window.location = targetUrl.toString();
	});

	const openUserImport = useCallback(() => {
		usersImportInput.current.click();
	}, []);

	const importUsers = useCallback(
		(e) => {
			const file = e.target.files[0];
			const fr = new FileReader();
			fr.onload = () => {
				const csvObj = csvArrayToIndexedObjects(parseCsvToArray(fr.result));
				if (csvObj.length > 0) {
					const keys = Object.keys(csvObj[0]);
					const requiredKeys = extraProfilesFieldsVassoc.map(({ key }) => key);
					if (keys.length < requiredKeys.length) {
						f7.dialog.alert(t('errors.import_length', { keys: requiredKeys.join(', ') }));
						return;
					}
					const badKey = requiredKeys.findIndex((key) => keys.indexOf(key) < 0);
					if (badKey >= 0) {
						f7.dialog.alert(t('errors.import_key', { key: requiredKeys[badKey], keys: requiredKeys.join(', ') }));
						return;
					}

					setToImport(csvObj);
				} else {
					f7.dialog.alert(t('errors.import_empty'));
				}
			};
			fr.readAsText(file);
		},
		[extraProfilesFieldsVassoc, t]
	);

	const onUserImport = useCallback((user) => {
		setUsers((oldUsers) => {
			const newUsers = { ...oldUsers };
			newUsers[user.id] = {
				...user,
				password: false,
			};
			return newUsers;
		});
	}, []);

	return (
		<Page>
			<CommonNavbar title={t('page_title')} />

			<UserFormPopup state={userFormPopupState} onSubmit={addUser} onPasswordReset={onPasswordReset} />

			<UsersImporter list={toImport} onUserImport={onUserImport} />

			<BlockTitle>{t('titles.manage_user')}</BlockTitle>
			{exportResult.rejected && (
				<Block>
					<ErrorMessage error={exportResult.error} />
				</Block>
			)}
			{toExportUsers.rejected && (
				<Block>
					<ErrorMessage error={toExportUsers.error} />
				</Block>
			)}
			<Block className="buttons-row">
				<Button fill onClick={() => userFormPopupState.open()}>
					<Icon f7="person_badge_plus" />
					&nbsp;
					{t('form.buttons.add_user')}
				</Button>
				<Button fill onClick={fetchExportUsers}>
					<Icon f7="table" />
					&nbsp;
					{t('form.buttons.export_users')}
				</Button>
				<Button fill onClick={openUserImport}>
					<Icon f7="arrow_uturn_down_circle" />
					&nbsp;
					{t('form.buttons.import_users')}
				</Button>
				<input type="file" ref={usersImportInput} style={{ display: 'none' }} onChange={importUsers} accept=".csv" />
			</Block>

			<BlockTitle>{t('titles.users_list')}</BlockTitle>
			<Block>
				<Searchbar
					disableButtonText={t('form.searchbar.cancel')}
					placeholder={t('form.searchbar.placeholder')}
					clearButton={true}
					onChange={onSearchChange}
					onSearchbarClear={() => setFilter('')}
					onSearchbarDisable={() => setFilter('')}
				/>
			</Block>

			<Card className="data-table data-table-init">
				{initialUsers.pending && <Preloader />}
				{initialUsers.fulfilled && renderedUsers}
				<Pagination state={pagination} />
			</Card>
		</Page>
	);
}

export default AdminUsersPage;
