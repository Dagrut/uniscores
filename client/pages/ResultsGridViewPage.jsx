/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { useCallback, useContext, useRef, useState } from 'react';
import { Page, BlockTitle, Block, Preloader, Toggle, Button, Icon, Link, f7 } from 'framework7-react';
import { useTranslation } from 'react-i18next';

import CommonNavbar from '../components/CommonNavbar';
import ErrorMessage from '../components/ErrorMessage';
import { useOnTrue, usePerms } from '../js/reactHooks';
import AppConfigContext from '../contexts/AppConfigContext';
import useResultsView from '../js/useResultsView';
import FormattedPoints from '../components/FormattedPoints';

import './ResultsGridViewPage.less';
import Tooltip from '../components/Tooltip';
import SessionContext from '../contexts/SessionContext';
import useFetch from '../js/useFetch';
import { allScoresToCsv } from '../js/csv';
import { strRemoveDiacritics, uniqId } from '../js/utils';
import UserProfileFieldsContext from '../contexts/UserProfileFieldsContext';
import html2canvas from '@html2canvas/html2canvas';

const ResultsGridViewPage = () => {
	const [showRoutes, setShowRoutes] = useState(true);
	const [showAllResultGroups, setShowAllResultGroups] = useState(false);
	const pngDownloadLinkRef = useRef(null);
	const [exportResult, internalExport] = useFetch();

	const extraProfileFields = useContext(UserProfileFieldsContext);
	const { isAdmin = false } = useContext(SessionContext);
	const { compName, compMode } = useContext(AppConfigContext);
	const { t } = useTranslation('ResultsGridViewPage');

	usePerms({ canDisplayScores: true });

	const { allErrors, allTrials, anyIsError, anyIsLoading, results } = useResultsView(showAllResultGroups);

	const exportTheseResults = useCallback(
		(sortedResults, trials, extra) => {
			const csv = allScoresToCsv(trials, sortedResults, extraProfileFields, {
				compMode,
				compName,
				t,
			});

			const uid = uniqId();
			const nowStr = new Date().toISOString().replace(/:/g, '-');

			const filteredCompName = compName.replace(/[^a-zA-Z0-9_]+/g, '-');
			internalExport(`/api/export/${uid}`, {
				method: 'POST',
				body: {
					name: `Results-${filteredCompName}-${extra}-${nowStr}.csv`,
					mimeType: 'text/csv',
					data: csv,
				},
			});
		},
		[compMode, compName, extraProfileFields, internalExport, t]
	);

	const exportResults = useCallback(() => {
		exportTheseResults(results, allTrials.json, 'visible');
	}, [allTrials.json, exportTheseResults, results]);

	const saveThisRG = useCallback(
		(resultGroup) => {
			exportTheseResults(
				[resultGroup],
				allTrials.json,
				strRemoveDiacritics(resultGroup.name).replace(/[^a-z0-9_]+/gi, '_')
			);
		},
		[allTrials.json, exportTheseResults]
	);

	const saveThisRGAsPng = useCallback(
		(rgId, rgName) => {
			const targetDiv = document.getElementById(rgId);
			if (targetDiv) {
				const filteredCompName = strRemoveDiacritics(compName).replace(/[^a-zA-Z0-9_]+/g, '-');
				const extra = strRemoveDiacritics(rgName).replace(/[^a-zA-Z0-9_]+/g, '-');
				const pngFileName = `Results-${filteredCompName}-${extra}.png`;
				targetDiv.classList.add('capturing-png');

				html2canvas(targetDiv)
					.then((canvas) => {
						targetDiv.classList.remove('capturing-png');

						canvas.toBlob((blob) => {
							if (!blob) {
								f7.dialog.alert('Cannot render canvas to blob!');
								return;
							}

							const downloadUrl = URL.createObjectURL(blob);
							pngDownloadLinkRef.current.download = pngFileName;
							pngDownloadLinkRef.current.href = downloadUrl;
							pngDownloadLinkRef.current.click();
						}, 'image/png');
					})
					.catch((e) => {
						targetDiv.classList.remove('capturing-png');
						f7.dialog.alert(`${e}`);
					});
			}
		},
		[compName]
	);

	useOnTrue(exportResult.fulfilled, () => {
		const targetUrl = new URL(exportResult.inputs.url, window.location.href);
		window.location = targetUrl.toString();
	});

	return (
		<Page className="ResultsGridViewPage">
			<CommonNavbar title={t('page_title')} />

			<a ref={pngDownloadLinkRef} style={{ display: 'none' }} target="_blank" className="external" />

			{anyIsLoading && (
				<Block>
					<Preloader />
				</Block>
			)}
			{anyIsError &&
				allErrors.map((error) => (
					<Block key={error.message}>
						<ErrorMessage error={error} centered />
					</Block>
				))}

			{!anyIsLoading && !anyIsError && isAdmin && (
				<>
					<Block className="buttons-row">
						<Button fill disabled={exportResult.pending} onClick={exportResults}>
							<Icon f7="table" />
							&nbsp;
							{t('export_button')}
						</Button>
					</Block>
					{exportResult.rejected && (
						<Block>
							<ErrorMessage error={exportResult.error} />
						</Block>
					)}
				</>
			)}

			<Block className="switches-list">
				<div>
					<Toggle defaultChecked onToggleChange={() => setShowRoutes((x) => !x)} /> {t('show_routes_label')}
				</div>
				{isAdmin && (
					<div>
						<Toggle onToggleChange={() => setShowAllResultGroups((x) => !x)} /> {t('show_all_result_groups_label')}
					</div>
				)}
			</Block>

			<div className={`results-groups comp-mode-${compMode}`}>
				{!anyIsLoading &&
					!anyIsError &&
					results.map((resultGroup) => {
						const tableLines = [];

						const thead = [
							<th className="wide-text" key="RG-name">
								{t('col.climber')}
							</th>,
						];

						extraProfileFields.fields.list.forEach(({ key, name, visible }) => {
							if (visible) {
								thead.push(
									<th key={`RG-user-profile-${key}`} className="profile-field">
										{name}
									</th>
								);
							}
						});

						thead.push(
							<th className="wide-text" key="RG-rank">
								{t('col.ranking')}
							</th>,
							<th className="wide-text" key="RG-total">
								{t('col.total')}
							</th>
						);

						Object.values(allTrials.json).forEach((discipline) => {
							if (!resultGroup.enabledDisciplines[discipline.id]) return;

							thead.push(
								<th key={discipline.id}>
									<div>
										<span>
											<Tooltip tooltipText={discipline.name}>{`${discipline.name}`}</Tooltip>
										</span>
									</div>
								</th>
							);

							if (showRoutes) {
								Object.values(discipline.routes).forEach((route) => {
									if (!resultGroup.enabledRoutes[route.id]) return;
									thead.push(
										<th key={route.id} className="route-text">
											<div>
												<span>
													<Tooltip tooltipText={route.name}>{route.name}</Tooltip>
												</span>
											</div>
										</th>
									);
								});
							}
						});

						resultGroup.rankings.forEach(({ user, total, rank, perDiscipline, perRoute }) => {
							const currentLine = [
								<td key="RG-user" className="username">{`${user.profile.firstname} ${user.profile.lastname}`}</td>,
							];

							extraProfileFields.fields.list.forEach(({ key, visible }) => {
								if (visible) {
									const value = extraProfileFields.fields.assoc[key].values[user.profile[key]] || user.profile[key];
									currentLine.push(
										<td key={`RG-user-profile-${key}`} className="profile-field">
											{value}
										</td>
									);
								}
							});

							currentLine.push(
								<td key="RG-rank" className="rank">
									{rank}
								</td>,
								<td key="RG-points" className="total">
									<FormattedPoints points={total} compMode={compMode} isTotal />
								</td>
							);

							Object.values(allTrials.json).forEach((discipline) => {
								if (!resultGroup.enabledDisciplines[discipline.id]) return;
								currentLine.push(
									<td key={discipline.id} className="discipline">
										<FormattedPoints points={perDiscipline[discipline.id]} compMode={compMode} />
									</td>
								);

								if (showRoutes) {
									Object.values(discipline.routes).forEach((route) => {
										if (!resultGroup.enabledRoutes[route.id]) return;
										let points;
										points = perRoute[route.id] || '0';
										currentLine.push(
											<td key={route.id}>
												<FormattedPoints points={points} compMode={compMode} />
											</td>
										);
									});
								}
							});

							tableLines.push(<tr key={user.id}>{currentLine}</tr>);
						});

						const rgId = `result-group-table-${resultGroup.id}`;
						return (
							<div key={resultGroup.id}>
								<div id={rgId} style={{ display: 'inline-block' }}>
									<BlockTitle large>
										{resultGroup.name}{' '}
										<Tooltip
											tooltipText={t('save_result_group_csv_tooltip')}
											component={Link}
											small
											onClick={() => saveThisRG(resultGroup)}
											className="rg-download-icon-button"
										>
											<Icon f7="floppy_disk" />
										</Tooltip>{' '}
										<Tooltip
											tooltipText={t('save_result_group_png_tooltip')}
											component={Link}
											small
											onClick={() => saveThisRGAsPng(rgId, resultGroup.name)}
											className="rg-download-icon-button"
										>
											<Icon f7="camera" />
										</Tooltip>
									</BlockTitle>
									<Block>
										<table key={resultGroup.id}>
											<thead>
												<tr>{thead}</tr>
											</thead>
											<tbody>{tableLines}</tbody>
										</table>
									</Block>
								</div>
							</div>
						);
					})}
			</div>
		</Page>
	);
};

export default ResultsGridViewPage;
