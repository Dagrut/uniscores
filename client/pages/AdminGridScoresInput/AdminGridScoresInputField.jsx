import PropTypes from 'prop-types';
import React from 'react';
import { encodeBoulderPoints, parseBoulderPoints } from '../../js/boulderPoints';
import { Checkbox, Input, Stepper } from 'framework7-react';

function AdminGridScoresInputField(props) {
	const {
		compMode,
		currentScore,
		disabled,
		discipineId,
		inputMethod,
		name,
		onScoreUpdate,
		points,
		routeId,
		t,
		userId,
	} = props;

	if (compMode === 'boulder_1zone') {
		const currentPoints = parseBoulderPoints(currentScore || 0);
		const onChangeBoulder = (key, newValue) => {
			const encodedPoints = encodeBoulderPoints({
				...currentPoints,
				[key]: newValue,
			});
			onScoreUpdate(userId, discipineId, routeId, encodedPoints);
		};

		return (
			<div className="AdminGridScoresInputField-boulder_1zone-container">
				<div>
					<span className="item-title item-label">{t('boulder_inputs.top')}</span>
					<br />
					<Checkbox checked={!!currentPoints.t} onChange={() => onChangeBoulder('t', currentPoints.t ? 0 : 1)} />
				</div>
				<div>
					<span className="item-title item-label">{t('boulder_inputs.zone')}</span>
					<br />
					<Checkbox checked={!!currentPoints.z} onChange={() => onChangeBoulder('z', currentPoints.z ? 0 : 1)} />
				</div>
				<div>
					<span className="item-title item-label">{t('boulder_inputs.top_attempts')}</span>
					<br />
					<Stepper
						fill
						small
						wraps
						value={currentPoints.ta}
						min={0}
						max={1000}
						step={1}
						readonly={disabled}
						onStepperChange={(newVal) => onChangeBoulder('ta', newVal)}
					/>
				</div>
				<div>
					<span className="item-title item-label">{t('boulder_inputs.zone_attempts')}</span>
					<br />
					<Stepper
						fill
						small
						wraps
						value={currentPoints.za}
						min={0}
						max={1000}
						step={1}
						readonly={disabled}
						onStepperChange={(newVal) => onChangeBoulder('za', newVal)}
					/>
				</div>
			</div>
		);
	}

	if (inputMethod === 'select') {
		return (
			<Input
				label={name}
				type="select"
				readonly={disabled}
				value={currentScore || Object.keys(points)[0]}
				onChange={(e) => onScoreUpdate(userId, discipineId, routeId, e.target.value)}
			>
				{Object.keys(points).map((pointCount) => (
					<option
						key={`ScoresInputPage-discipline-${discipineId}-route-${routeId}-grade-${pointCount}`}
						value={pointCount}
					>
						{points[pointCount]}
					</option>
				))}
			</Input>
		);
	}

	return (
		<Input
			label={name}
			type="text"
			readonly={disabled}
			value={currentScore || 0}
			onChange={(e) => onScoreUpdate(userId, discipineId, routeId, e.target.value)}
			validate
			pattern="[0-9]+(\.[0-9]+)?"
			errorMessage={t('input_not_a_number_error')}
		/>
	);
}

AdminGridScoresInputField.propTypes = {
	compMode: PropTypes.string.isRequired,
	currentScore: PropTypes.string,
	disabled: PropTypes.bool.isRequired,
	discipineId: PropTypes.string.isRequired,
	inputMethod: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	onScoreUpdate: PropTypes.func.isRequired,
	points: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number])).isRequired,
	routeId: PropTypes.string.isRequired,
	t: PropTypes.func.isRequired,
	userId: PropTypes.string.isRequired,
};

AdminGridScoresInputField.defaultProps = {
	currentScore: undefined,
};

export default AdminGridScoresInputField;
