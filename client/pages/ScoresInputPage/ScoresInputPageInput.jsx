import PropTypes from 'prop-types';
import React from 'react';
import { encodeBoulderPoints, parseBoulderPoints } from '../../js/boulderPoints';
import { Block, Checkbox, ListInput, ListItem, Stepper } from 'framework7-react';

function ScoresInputPageInput(props) {
	const { name, discipineId, routeId, currentScore, inputMethod, points, disabled, t, compMode, onScoreUpdate } = props;

	if (compMode === 'boulder_1zone') {
		const currentPoints = parseBoulderPoints(currentScore || 0);
		const onChangeBoulder = (key, newValue) => {
			const encodedPoints = encodeBoulderPoints({
				...currentPoints,
				[key]: newValue,
			});
			onScoreUpdate(discipineId, routeId, encodedPoints);
		};

		return (
			<ListItem>
				<span slot="root-start">{name}</span>
				<Block style={{ width: '100%' }} strong className="grid grid-gap grid-cols-2">
					<div>
						<span className="item-title item-label">{t('boulder_inputs.top')}</span>
						<br />
						<Checkbox checked={!!currentPoints.t} onChange={() => onChangeBoulder('t', currentPoints.t ? 0 : 1)} />
					</div>
					<div>
						<span className="item-title item-label">{t('boulder_inputs.zone')}</span>
						<br />
						<Checkbox checked={!!currentPoints.z} onChange={() => onChangeBoulder('z', currentPoints.z ? 0 : 1)} />
					</div>
					<div>
						<span className="item-title item-label">{t('boulder_inputs.top_attempts')}</span>
						<br />
						<Stepper
							fill
							small
							wraps
							value={currentPoints.ta}
							min={0}
							max={1000}
							step={1}
							onStepperChange={(newVal) => onChangeBoulder('ta', newVal)}
						/>
					</div>
					<div>
						<span className="item-title item-label">{t('boulder_inputs.zone_attempts')}</span>
						<br />
						<Stepper
							fill
							small
							wraps
							value={currentPoints.za}
							min={0}
							max={1000}
							step={1}
							onStepperChange={(newVal) => onChangeBoulder('za', newVal)}
						/>
					</div>
				</Block>
			</ListItem>
		);
	}

	if (inputMethod === 'select') {
		return (
			<ListInput
				label={name}
				type="select"
				disabled={disabled}
				value={currentScore || Object.keys(points)[0]}
				onChange={(e) => onScoreUpdate(discipineId, routeId, e.target.value)}
			>
				{Object.keys(points).map((pointCount) => (
					<option
						key={`ScoresInputPage-discipline-${discipineId}-route-${routeId}-grade-${pointCount}`}
						value={pointCount}
					>
						{points[pointCount]}
					</option>
				))}
			</ListInput>
		);
	}

	return (
		<ListInput
			label={name}
			type="text"
			disabled={disabled}
			value={currentScore || 0}
			onChange={(e) => onScoreUpdate(discipineId, routeId, e.target.value)}
			validate
			pattern="[0-9]+(\.[0-9]+)?"
			errorMessage={t('input_not_a_number_error')}
		/>
	);
}

ScoresInputPageInput.propTypes = {
	compMode: PropTypes.string.isRequired,
	currentScore: PropTypes.string,
	disabled: PropTypes.bool.isRequired,
	discipineId: PropTypes.string.isRequired,
	inputMethod: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	onScoreUpdate: PropTypes.func.isRequired,
	points: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number])).isRequired,
	routeId: PropTypes.string.isRequired,
	t: PropTypes.func.isRequired,
};

ScoresInputPageInput.defaultProps = {
	currentScore: undefined,
};

ScoresInputPageInput.displayName = 'f7-list-input';

export default ScoresInputPageInput;
