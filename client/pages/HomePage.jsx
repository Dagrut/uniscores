/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { useCallback, useContext, useMemo } from 'react';
import { BlockTitle, List, ListItem, Page, Preloader } from 'framework7-react';
import SessionContext from '../contexts/SessionContext';
import CommonNavbar from '../components/CommonNavbar';
import { objGet } from '../js/utils';
import AppConfigContext from '../contexts/AppConfigContext';
import { useTranslation } from 'react-i18next';

function HomePage() {
	const { connected, isAdmin, loading: sessionLoading } = useContext(SessionContext);
	const {
		compIsOpen,
		compIsStarted,
		compName,
		displayStats,
		displayResultsAfter,
		displayResultsDuring,
		loading: confLoading,
	} = useContext(AppConfigContext);
	const { t } = useTranslation('HomePage');

	const menusList = useMemo(
		() => [
			{
				filterKeys: ['sessionStatus.isAdmin'],
				link: '/admin/settings/app',
				title: t('general_parameters'),
			},
			{
				filterKeys: ['sessionStatus.isAdmin'],
				link: '/admin/users',
				title: t('participants'),
			},
			{
				filterKeys: ['sessionStatus.isAdmin'],
				link: '/admin/routes',
				title: t('boulders_and_routes'),
			},
			{
				filterKeys: ['sessionStatus.isAdmin'],
				link: '/admin/resultsGroups',
				title: t('result_groups'),
			},
			{
				filterKeys: ['sessionStatus.isAdmin'],
				link: '/admin/scores/input/grid',
				title: t('scores_input_grid'),
			},
			{
				filterKeys: ['sessionStatus.isUser', 'appConf.compIsOpen', 'appConf.compIsStarted'],
				link: '/scores/input',
				title: t('write_my_points'),
			},
			{
				filterKeys: ['appConf.displayStats'],
				link: '/comp/progress',
				title: t('see_the_comp_stats'),
			},
			{
				filterKeys: ['appConf.displayResults'],
				link: '/results/view/grid',
				title: t('see_the_results_tables'),
			},
		],
		[t]
	);

	const filterParams = useMemo(
		() => ({
			sessionStatus: {
				isAdmin: connected && isAdmin,
				isUser: connected && !isAdmin,
			},
			appConf: {
				compIsOpen,
				compIsStarted,
				compName,
				displayStats,
				displayResults:
					(connected && isAdmin) || (compIsStarted && displayResultsDuring) || (!compIsStarted && displayResultsAfter),
			},
		}),
		[compIsOpen, compIsStarted, compName, connected, displayStats, displayResultsAfter, displayResultsDuring, isAdmin]
	);

	const checkFilter = useCallback(
		({ filterKeys }) => {
			const ok = filterKeys.every((filterKey) => objGet(filterParams, filterKey, false));
			return ok && !confLoading;
		},
		[confLoading, filterParams]
	);

	return (
		<Page name="home">
			<CommonNavbar backLink={false} title={compName} />

			{(sessionLoading || confLoading) && <Preloader />}
			<BlockTitle>{t('main_menu')}</BlockTitle>
			<List strong inset dividersIos>
				{menusList.filter(checkFilter).map(({ link, title }) => (
					<ListItem key={`HomePage-menu-${link.replace('/', '-')}`} link={link} title={title} />
				))}
			</List>
		</Page>
	);
}

export default HomePage;
