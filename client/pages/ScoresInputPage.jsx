/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { useRef, useCallback, useState, useMemo, useContext } from 'react';
import PropTypes from 'prop-types';
import { Page, List, BlockTitle, Block, Preloader, f7, Fab } from 'framework7-react';

import useFetch from '../js/useFetch';
import { useErrorTranslator, useOnTrue, usePerms } from '../js/reactHooks';
import SessionContext from '../contexts/SessionContext';
import CommonNavbar from '../components/CommonNavbar';
import { objGet } from '../js/utils';
import { useTranslation } from 'react-i18next';
import AppConfigContext from '../contexts/AppConfigContext';
import ScoresInputPageInput from './ScoresInputPage/ScoresInputPageInput';
import { addEnabledTrialsToSet } from '../js/resultsComputers';

function ScoresInputPage(props) {
	const { userId } = props;
	const [scores, setScores] = useState({});
	const pendingAPIScores = useRef(null);
	const lastSentScores = useRef([]);

	const { user: sessionUser = { profile: {} }, isAdmin, loading: sessionLoading } = useContext(SessionContext);
	const { compMode } = useContext(AppConfigContext);
	const { t } = useTranslation(['ScoresInputPage']);
	const errorTs = useErrorTranslator();

	const [initialScores] = useFetch(`/api/scores/${userId}`);
	const [initialTrials] = useFetch('/api/trials');
	const [initialResultGroups] = useFetch('/api/resultsGroups');
	const [apiUser, fetchApiUser] = useFetch();
	const [scoreUpdate, updateAPIScores] = useFetch();

	useOnTrue(!sessionLoading, () => {
		if (isAdmin) {
			fetchApiUser(`/api/users/${userId}`);
		}
	});

	const user = useMemo(() => {
		if (isAdmin) {
			return apiUser.json || { profile: {} };
		}
		return sessionUser;
	}, [apiUser.json, isAdmin, sessionUser]);

	usePerms({ canInputScores: true });

	const visibleAndSortedTrials = useMemo(() => {
		const allEnabled = {
			enabledDisciplines: {},
			enabledRoutes: {},
		};

		(initialResultGroups.json || [])
			.filter(({ editable }) => editable || isAdmin)
			.forEach(({ groupKeys, groupRoutes }) => {
				const indexedGroupKeys = groupKeys.reduce((acc, { field, value }) => {
					if (!acc[field]) acc[field] = [];
					acc[field].push(value);
					return acc;
				}, {});

				// intermediate hash of all users for this group
				const isInGroup = Object.keys(indexedGroupKeys).every(
					(key) => indexedGroupKeys[key].indexOf(user.profile[key]) >= 0
				);

				if (isInGroup) {
					addEnabledTrialsToSet(initialTrials.json, groupRoutes, allEnabled);
				}
			});

		const ret = Object.values(initialTrials.json || {})
			.filter(({ id }) => allEnabled.enabledDisciplines[id])
			.sort(({ name }, { name: name2 }) => {
				if (name > name2) return 1;
				if (name < name2) return -1;
				return 0;
			})
			.map((trial) => {
				const routes = Object.values(trial.routes)
					.filter(({ id }) => allEnabled.enabledRoutes[id])
					.sort(({ name }, { name: name2 }) => {
						if (name > name2) return 1;
						if (name < name2) return -1;
						return 0;
					});
				return {
					...trial,
					routes,
				};
			});

		return ret;
	}, [initialResultGroups.json, initialTrials.json, isAdmin, user.profile]);

	useOnTrue(initialScores.fulfilled, () => {
		if (initialScores.fulfilled) {
			setScores(initialScores.json);
		}
	});

	useOnTrue(
		scoreUpdate.fulfilled,
		() => {
			if (pendingAPIScores.current) {
				lastSentScores.current = Object.values(pendingAPIScores.current);
				updateAPIScores(`/api/scores/${userId}/`, { method: 'POST', body: pendingAPIScores.current });
				pendingAPIScores.current = null;
			}
		},
		false
	);

	useOnTrue(initialScores.rejected, () => {
		const currentError = errorTs(initialScores.error.message);
		f7.dialog.alert(t('errors.scores_load', { error: currentError }));
	});

	useOnTrue(initialTrials.rejected, () => {
		const currentError = errorTs(initialTrials.error.message);
		f7.dialog.alert(t('errors.route_load', { error: currentError }));
	});

	useOnTrue(scoreUpdate.rejected, () => {
		const currentError = errorTs(scoreUpdate.error.message);
		const names = lastSentScores.current.map(
			({ disciplineId, routeId }) => initialTrials.json[disciplineId].routes[routeId].name
		);

		f7.dialog.alert(t('errors.points_save', { routes: names.join(', '), error: currentError }));
	});

	const onScoreUpdate = useCallback(
		(disciplineId, routeId, value) => {
			setScores((prevScore) => {
				const newScore = { ...prevScore };
				if (!newScore[disciplineId]) {
					newScore[disciplineId] = {};
				}
				newScore[disciplineId][routeId] = value;
				if (!scoreUpdate.pending) {
					const body = [{ disciplineId, routeId, score: value }];
					lastSentScores.current = [{ disciplineId, routeId }];
					updateAPIScores(`/api/scores/${userId}/`, { method: 'POST', body });
				} else {
					if (!pendingAPIScores.current) {
						pendingAPIScores.current = [];
					}
					pendingAPIScores.current.push({ disciplineId, routeId, score: value });
				}
				return newScore;
			});
		},
		[scoreUpdate.pending, updateAPIScores, userId]
	);

	return (
		<Page>
			<CommonNavbar title={t('page_title')} />

			<BlockTitle large>
				{t('main_blocktitle', { firstname: user.profile.firstname, lastname: user.profile.lastname })}
			</BlockTitle>
			{(initialTrials.pending || initialResultGroups.pending || initialScores.pending) && (
				<Block>
					<Preloader />
				</Block>
			)}

			{visibleAndSortedTrials.map(({ id: discipineId, name: disciplineName, routes }) => (
				<div key={`ScoresInputPage-discipline-${discipineId}`}>
					<BlockTitle>{disciplineName}</BlockTitle>
					<Block>
						<List strongIos outlineIos dividersIos>
							{routes.map(({ id: routeId, inputMethod, name, points }) => {
								const key = `ScoresInputPage-discipline-${discipineId}-route-${routeId}`;

								return (
									<ScoresInputPageInput
										key={key}
										name={name}
										compMode={compMode}
										currentScore={objGet(scores, [discipineId, routeId])}
										disabled={
											!initialScores.fulfilled || (scoreUpdate.pending && lastSentScores.current.indexOf(routeId) >= 0)
										}
										discipineId={discipineId}
										inputMethod={inputMethod}
										onScoreUpdate={onScoreUpdate}
										points={points}
										routeId={routeId}
										t={t}
									/>
								);
							})}
						</List>
					</Block>
				</div>
			))}

			{scoreUpdate.pending && (
				<Fab position="right-bottom" slot="fixed" style={{ opacity: '0.75' }}>
					<Preloader />
				</Fab>
			)}
		</Page>
	);
}

ScoresInputPage.propTypes = {
	userId: PropTypes.string,
};

ScoresInputPage.defaultProps = {
	userId: 'self',
};

export default ScoresInputPage;
