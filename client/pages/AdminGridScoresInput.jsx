/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import React, { useCallback, useContext, useMemo, useRef, useState } from 'react';
import { Page, BlockTitle, Block, Preloader, Toggle, f7 } from 'framework7-react';
import { useTranslation } from 'react-i18next';

import CommonNavbar from '../components/CommonNavbar';
import ErrorMessage from '../components/ErrorMessage';
import { useErrorTranslator, useOnTrue, usePerms } from '../js/reactHooks';
import AppConfigContext from '../contexts/AppConfigContext';

import './AdminGridScoresInput.less';
import useFetch from '../js/useFetch';
import { addEnabledTrialsToSet } from '../js/resultsComputers';
import { objGet } from '../js/utils';
import AdminGridScoresInputField from './AdminGridScoresInput/AdminGridScoresInputField';

const AdminGridScoresInput = () => {
	usePerms({ admin: true });

	const [scores, setScores] = useState({});
	const [showAllResultGroups, setShowAllResultGroups] = useState(false);
	const { compMode } = useContext(AppConfigContext);
	const pendingAPIScores = useRef([]);
	const lastSentScore = useRef([]);
	const { t } = useTranslation('AdminGridScoresInput');
	const errorTs = useErrorTranslator();

	const [initialScores] = useFetch('/api/scores');
	const [allUsers] = useFetch('/api/users');
	const [allTrials] = useFetch('/api/trials');
	const [resultsGroups] = useFetch('/api/resultsGroups');
	const [scoreUpdate, updateAPIScores] = useFetch();

	useOnTrue(initialScores.fulfilled, () => {
		if (initialScores.fulfilled) {
			setScores(initialScores.json);
		}
	});

	useOnTrue(
		scoreUpdate.fulfilled,
		() => {
			if (pendingAPIScores.current.length > 0) {
				const nextItem = pendingAPIScores.current.shift();
				lastSentScore.current = nextItem;
				const { userId, ...data } = nextItem;
				updateAPIScores(`/api/scores/${userId}/`, { method: 'POST', body: [data] });
			} else {
				lastSentScore.current = null;
			}
		},
		false
	);

	useOnTrue(scoreUpdate.rejected, () => {
		const currentError = errorTs(scoreUpdate.error.message);
		pendingAPIScores.current.uhshift();
		const { disciplineId, routeId } = lastSentScore.current;
		const name = allTrials.json[disciplineId].routes[routeId].name;

		f7.dialog.alert(t('error_points_save', { routeName: name, error: currentError }));
	});

	const onScoreUpdate = useCallback(
		(userId, disciplineId, routeId, score) => {
			setScores((prevScore) => {
				const newScore = { ...prevScore };
				if (!newScore[userId]) {
					newScore[userId] = {};
				}
				if (!newScore[userId][disciplineId]) {
					newScore[userId][disciplineId] = {};
				}
				newScore[userId][disciplineId][routeId] = score;
				if (!scoreUpdate.pending) {
					const body = [{ disciplineId, routeId, score }];
					lastSentScore.current = { userId, disciplineId, routeId, score };
					updateAPIScores(`/api/scores/${userId}/`, { method: 'POST', body });
				} else {
					pendingAPIScores.current.push({ userId, disciplineId, routeId, score });
				}
				return newScore;
			});
		},
		[scoreUpdate.pending, updateAPIScores]
	);

	const anyIsLoading = useMemo(
		() =>
			initialScores.pending ||
			allTrials.pending ||
			allUsers.pending ||
			resultsGroups.pending ||
			initialScores.init ||
			allTrials.init ||
			allUsers.init ||
			resultsGroups.init,
		[
			initialScores.init,
			initialScores.pending,
			allTrials.init,
			allTrials.pending,
			allUsers.init,
			allUsers.pending,
			resultsGroups.init,
			resultsGroups.pending,
		]
	);

	const anyIsError = useMemo(
		() => initialScores.rejected || allUsers.rejected || resultsGroups.rejected || allTrials.rejected,
		[allTrials.rejected, initialScores.rejected, allUsers.rejected, resultsGroups.rejected]
	);

	const allErrors = useMemo(
		() =>
			[
				initialScores.rejected && initialScores.error,
				allUsers.rejected && allUsers.error,
				resultsGroups.rejected && resultsGroups.error,
				allTrials.rejected && allTrials.error,
			].filter((x) => x),
		[
			initialScores.error,
			initialScores.rejected,
			allTrials.error,
			allTrials.rejected,
			allUsers.error,
			allUsers.rejected,
			resultsGroups.error,
			resultsGroups.rejected,
		]
	);

	return (
		<Page className="AdminGridScoresInput">
			<CommonNavbar title={t('page_title')} />

			{anyIsLoading && (
				<Block>
					<Preloader />
				</Block>
			)}
			{anyIsError &&
				allErrors.map((error) => (
					<Block key={error.message}>
						<ErrorMessage error={error} centered />
					</Block>
				))}

			<Block className="switches-list">
				<div>
					<Toggle onToggleChange={() => setShowAllResultGroups((x) => !x)} /> {t('show_all_result_groups_label')}
				</div>
			</Block>

			<div className={`results-groups comp-mode-${compMode}`}>
				{!anyIsLoading &&
					!anyIsError &&
					resultsGroups.json
						.filter(({ visible }) => visible || showAllResultGroups)
						.map((resultGroup) => {
							const tableLines = [];

							const filterSet = addEnabledTrialsToSet(allTrials.json, resultGroup.groupRoutes);

							const thead = [<th key="RG-name">{t('column_climber')}</th>];
							Object.values(allTrials.json).forEach((discipline) => {
								if (!filterSet.enabledDisciplines[discipline.id]) return;

								Object.values(discipline.routes).forEach((route) => {
									if (!filterSet.enabledRoutes[route.id]) return;
									thead.push(
										<th key={route.id}>
											<span className="discipline-text">{discipline.name}</span>
											<br />
											<span className="route-text">{route.name}</span>
										</th>
									);
								});
							});

							const indexedGroupKeys = resultGroup.groupKeys.reduce((acc, { field, value }) => {
								if (!acc[field]) acc[field] = [];
								acc[field].push(value);
								return acc;
							}, {});

							Object.values(allUsers.json)
								.filter(({ profile }) => {
									const isInGroup = Object.keys(indexedGroupKeys).every(
										(key) => indexedGroupKeys[key].indexOf(profile[key]) >= 0
									);
									return isInGroup;
								})
								.forEach((user) => {
									const currentLine = [
										<td key="RG-user" className="username">{`${user.profile.firstname} ${user.profile.lastname}`}</td>,
									];

									Object.values(allTrials.json).forEach((discipline) => {
										if (!filterSet.enabledDisciplines[discipline.id]) return;

										Object.values(discipline.routes).forEach((route) => {
											if (!filterSet.enabledRoutes[route.id]) return;
											currentLine.push(
												<td key={route.id}>
													<AdminGridScoresInputField
														compMode={compMode}
														currentScore={objGet(scores, [user.id, discipline.id, route.id])}
														disabled={scoreUpdate.pending}
														discipineId={discipline.id}
														inputMethod={route.inputMethod}
														name={route.name}
														onScoreUpdate={onScoreUpdate}
														points={route.points}
														userId={user.id}
														routeId={route.id}
														t={t}
													/>
												</td>
											);
										});
									});

									tableLines.push(<tr key={user.id}>{currentLine}</tr>);
								});

							return (
								<React.Fragment key={resultGroup.id}>
									<BlockTitle large>{resultGroup.name}</BlockTitle>
									<Block>
										<table key={resultGroup.id}>
											<thead>
												<tr>{thead}</tr>
											</thead>
											<tbody>{tableLines}</tbody>
										</table>
									</Block>
								</React.Fragment>
							);
						})}
			</div>
		</Page>
	);
};

export default AdminGridScoresInput;
