/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import { Badge, Block, BlockTitle, List, ListInput, ListItem, Page, Preloader } from 'framework7-react';
import React, { useContext, useMemo, useState } from 'react';
import { Trans, useTranslation } from 'react-i18next';

import AppConfigContext from '../contexts/AppConfigContext';
import CommonNavbar from '../components/CommonNavbar';
import ErrorMessage from '../components/ErrorMessage';
import useFetch from '../js/useFetch';
import { computeProgress } from '../js/resultsComputers';
import { usePerms } from '../js/reactHooks';

function CompStatsPage() {
	const [nameFilter, setNameFilter] = useState('');
	const [displayedResultGroupId, setDisplayedResultGroupId] = useState('');
	const [sortDescending, setSortDescending] = useState(true);
	const [allScores] = useFetch('/api/scores');
	const [allUsers] = useFetch('/api/users');
	const [allTrials] = useFetch('/api/trials');
	const [resultsGroups] = useFetch('/api/resultsGroups');

	const { compMode } = useContext(AppConfigContext);
	const { t } = useTranslation('CompStatsPage');

	usePerms({ canDisplayStats: true });

	const anyIsLoading = useMemo(
		() =>
			allScores.pending ||
			allTrials.pending ||
			allUsers.pending ||
			resultsGroups.pending ||
			allScores.init ||
			allTrials.init ||
			allUsers.init ||
			resultsGroups.init,
		[
			allScores.init,
			allScores.pending,
			allTrials.init,
			allTrials.pending,
			allUsers.init,
			allUsers.pending,
			resultsGroups.init,
			resultsGroups.pending,
		]
	);

	const anyIsError = useMemo(
		() => allScores.rejected || allUsers.rejected || resultsGroups.rejected || allTrials.rejected,
		[allTrials.rejected, allScores.rejected, allUsers.rejected, resultsGroups.rejected]
	);

	const allErrors = useMemo(
		() =>
			[
				allScores.rejected && allScores.error,
				allUsers.rejected && allUsers.error,
				resultsGroups.rejected && resultsGroups.error,
				allTrials.rejected && allTrials.error,
			].filter((x) => x),
		[
			allScores.error,
			allScores.rejected,
			allTrials.error,
			allTrials.rejected,
			allUsers.error,
			allUsers.rejected,
			resultsGroups.error,
			resultsGroups.rejected,
		]
	);

	const allProgress = useMemo(() => {
		if (anyIsLoading || anyIsError) {
			return null;
		}

		const ret = computeProgress(allScores.json, allUsers.json, allTrials.json, resultsGroups.json, { compMode });

		return ret;
	}, [anyIsLoading, anyIsError, allScores.json, allUsers.json, allTrials.json, resultsGroups.json, compMode]);

	const routesDisciplinesHash = useMemo(() => {
		const disciplines = allTrials.json || {};

		const hash = Object.values(disciplines).reduce((acc, discipline) => {
			const { routes } = disciplines[discipline.id];

			Object.values(routes).forEach((route) => {
				acc[route.id] = {
					routeName: route.name,
					disciplineName: discipline.name,
				};
			});

			return acc;
		}, {});

		return hash;
	}, [allTrials.json]);

	const [currentProgress, totalClimbsCount] = useMemo(() => {
		if (!allProgress) return [];

		const indexToSort = !displayedResultGroupId ? allProgress.general : allProgress.groups[displayedResultGroupId];

		let total = 0;
		const list = Object.keys(indexToSort).map((routeId) => {
			total += indexToSort[routeId];
			return {
				routeId,
				climbed: indexToSort[routeId],
			};
		});

		list.sort((a, b) => {
			if (sortDescending) {
				return b.climbed - a.climbed;
			}

			return a.climbed - b.climbed;
		});

		return [list, total];
	}, [allProgress, displayedResultGroupId, sortDescending]);

	const filteredCurrentProgress = useMemo(
		() =>
			!nameFilter
				? currentProgress
				: currentProgress.filter(({ routeId }) => {
						const { disciplineName, routeName } = routesDisciplinesHash[routeId];

						const lowerFilter = nameFilter.toLowerCase();
						if (disciplineName.toLowerCase().indexOf(lowerFilter) >= 0) {
							return true;
						} else if (routeName.toLowerCase().indexOf(lowerFilter) >= 0) {
							return true;
						}

						return false;
					}),
		[currentProgress, nameFilter, routesDisciplinesHash]
	);

	return (
		<Page>
			<CommonNavbar title={t('page_title')} />

			<BlockTitle>{t('stats_filters_blocktitle')}</BlockTitle>
			{anyIsLoading && <Preloader />}
			{anyIsError &&
				allErrors.map((error) => (
					<Block key={error.message}>
						<ErrorMessage error={error} centered />
					</Block>
				))}
			{!anyIsLoading && !anyIsError && (
				<>
					<List outline inset>
						<ListInput
							label={t('form.search.label')}
							type="text"
							placeholder={t('form.search.placeholder')}
							value={nameFilter}
							onChange={(e) => setNameFilter(e.target.value)}
						/>
						<ListInput
							label={t('form.result_group.label')}
							type="select"
							value={displayedResultGroupId}
							onChange={(e) => setDisplayedResultGroupId(e.target.value)}
						>
							<option value={''}>{t('form.result_group.general')}</option>
							{(resultsGroups.json || []).map(({ id, name }) => (
								<option key={`CompStatsPage-resultGroup-${id}`} value={id}>
									{name}
								</option>
							))}
						</ListInput>
						<ListItem
							className="points-subform-item"
							checkbox
							checked={sortDescending}
							onChange={() => setSortDescending((oldReversed) => !oldReversed)}
						>
							{t('form.sort_descending.text')}
						</ListItem>
					</List>

					<BlockTitle>{t('rankings_blocktitle')}</BlockTitle>
					<Block>
						<h3>
							<Trans ns="CompStatsPage" i18nKey="total_climbs" values={{ count: totalClimbsCount }}>
								{"Nombre total d'ascensions ou tentatives : "}
								<Badge color="red">{{ totalClimbsCount }}</Badge>
							</Trans>
						</h3>
					</Block>
					<List outline strong dividers>
						{filteredCurrentProgress.map(({ routeId, climbed }) => {
							const { disciplineName, routeName } = routesDisciplinesHash[routeId];
							return (
								<ListItem key={`CompStatsPage-progress-${routeId}`}>
									<span>
										<Trans
											ns="CompStatsPage"
											i18nKey="progress_item"
											values={{ count: climbed, disciplineName, routeName }}
										>
											{`[${disciplineName}] `}
											<b>{routeName}</b>
											{` grimpée `}
											<b>{climbed}</b>
											{' fois'}
										</Trans>
									</span>
								</ListItem>
							);
						})}
					</List>
				</>
			)}
		</Page>
	);
}

export default CompStatsPage;
