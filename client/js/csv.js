/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import { parseBoulderPoints } from './boulderPoints';

function escapeCsvEntry(rawValue) {
	const value = `${rawValue}`;
	if (value.match(/[,"\n\r]/)) {
		return `"${value.replace(/"/g, '""')}"`;
	}

	return value;
}

function encodeCsvLine(line) {
	return line.map(escapeCsvEntry).join(',');
}

export function allScoresToCsv(trials, results, extraProfileFields, { compName, compMode, t }) {
	const formatPoints = (points) => {
		if (compMode === 'boulder_1zone') {
			const parsed = parseBoulderPoints(points);
			const formatted = t('export_formats.boulder_1zone', parsed);
			return formatted;
		}

		return points;
	};

	const groupsCsvs = results.map((resultGroupData) => {
		const csvLines = [];

		const groupFirstLine = [`[${resultGroupData.name}]`];
		extraProfileFields.fields.list.forEach(({ name, visible }) => {
			if (visible) {
				groupFirstLine.push(name);
			}
		});
		groupFirstLine.push('Rank', 'Total');

		Object.values(trials).forEach((discipline) => {
			if (!resultGroupData.enabledDisciplines[discipline.id]) return;

			groupFirstLine.push(`[${discipline.name}]`);
			Object.values(discipline.routes).forEach((route) => {
				if (!resultGroupData.enabledRoutes[route.id]) return;

				groupFirstLine.push(route.name);
			});
		});

		csvLines.push(encodeCsvLine(groupFirstLine));

		resultGroupData.rankings.forEach(({ user, total, rank, perDiscipline, perRoute }) => {
			const currentLine = [`${user.profile.firstname} ${user.profile.lastname}`];

			extraProfileFields.fields.list.forEach(({ key, visible }) => {
				if (visible) {
					const value = extraProfileFields.fields.assoc[key].values[user.profile[key]] || user.profile[key];
					currentLine.push(value);
				}
			});

			currentLine.push(rank, formatPoints(total));

			Object.values(trials).forEach((discipline) => {
				if (!resultGroupData.enabledDisciplines[discipline.id]) return;

				currentLine.push(formatPoints(perDiscipline[discipline.id]));

				Object.values(discipline.routes).forEach((route) => {
					if (!resultGroupData.enabledRoutes[route.id]) return;

					let points = perRoute[route.id] || '0';
					points = formatPoints(points);
					currentLine.push(points);
				});
			});

			csvLines.push(encodeCsvLine(currentLine));
		});

		return csvLines.join('\n');
	});

	const wholeCsv = `${compName}\n\n${groupsCsvs.join('\n\n')}`;

	return wholeCsv;
}

export function allUsersToCsv(users, fields) {
	const headerLine = encodeCsvLine(fields);

	const usersCsvArray = Object.values(users).map(({ profile }) => {
		const userLine = fields.map((key) => profile[key]);
		return encodeCsvLine(userLine);
	});

	const wholeCsv = `${headerLine}\n${usersCsvArray.join('\n')}`;

	return wholeCsv;
}

export function parseCsvToArray(csvContent) {
	const QUOTE = '"';
	const COMMA = ',';

	const csvArray = [];
	let currentLine = [];
	let currentCell = '';
	let quotes = 0;
	let lastChar = null;

	for (let i = 0; i < csvContent.length; i++) {
		const c = csvContent[i];

		if (!quotes) {
			if (c === QUOTE) {
				quotes = 1;
			} else if (c === COMMA) {
				currentLine.push(currentCell);
				currentCell = '';
			} else if (c === '\r') {
				/* Skip */
			} else if (c === '\n') {
				currentLine.push(currentCell);
				currentCell = '';
				csvArray.push(currentLine);
				currentLine = [];
			} else {
				currentCell += c;
			}
		} else {
			if (c === QUOTE) {
				quotes++;
				if (quotes === 3) {
					currentCell += QUOTE;
					quotes = 1;
				}
			} else if (lastChar === QUOTE && quotes === 2) {
				quotes = 0;
				i--;
			} else {
				currentCell += c;
			}
		}

		lastChar = c;
	}

	if (lastChar !== '\n') {
		currentLine.push(currentCell);
		currentCell = '';
		csvArray.push(currentLine);
		currentLine = [];
	}

	return csvArray;
}

export function csvArrayToIndexedObjects(csvArray) {
	const csvKeys = csvArray.shift();
	const csvObjArray = csvArray.map((row) => {
		const ret = csvKeys.reduce((acc, key, id) => {
			acc[key] = row[id];
			return acc;
		}, {});

		return ret;
	});

	return csvObjArray;
}
