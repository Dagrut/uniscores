/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */

import { reverseEncodedBoulderPoints } from './boulderPoints';
import { objGet } from './utils';

const COMP_MODES = {
	point_based: { reversed: false },
	time_based: { reversed: true },
	rank_based: { reversed: true },
	boulder_1zone: { reversed: false },
};

function computeScoreForRoute(rawScore, route, { finishes = 1, min = 1, max = 1 } = {}) {
	if (route.scoringMethod === 'absolute') {
		return rawScore;
	}

	if (route.scoringMethod === 'mapped') {
		let score;
		let scoreMappedIn0To1;

		if (max !== min) {
			scoreMappedIn0To1 = (rawScore - min) / (max - min);
		} else {
			scoreMappedIn0To1 = route.reversed ? 0 : 1;
		}

		if (route.reversed) {
			scoreMappedIn0To1 = 1 - scoreMappedIn0To1;
		}

		let gradientScore = scoreMappedIn0To1;
		if (route.gradient) {
			gradientScore = Math.round(scoreMappedIn0To1 * route.gradient) / route.gradient;
		}

		score = gradientScore * (route.max - route.min) + route.min;

		return score;
	}

	if (route.scoringMethod === 'shared') {
		return rawScore / finishes;
	}

	return rawScore;
}

export function addEnabledTrialsToSet(trials, groupRoutes, set = {}) {
	if (!set.enabledRoutes) set.enabledRoutes = {};
	if (!set.enabledDisciplines) set.enabledDisciplines = {};

	groupRoutes.forEach(({ disciplineId, routeId }) => {
		if (routeId) {
			set.enabledRoutes[routeId] = true;
			set.enabledDisciplines[disciplineId] = true;
		} else {
			set.enabledDisciplines[disciplineId] = true;
			const routesIds = Object.keys(objGet(trials, [disciplineId, 'routes'], {}));
			routesIds.forEach((routeId2) => {
				set.enabledRoutes[routeId2] = true;
			});
		}
	});

	return set;
}

export function computeResults(scores, users, trials, resultsGroups, { compMode, all = false, skipZeroPoints }) {
	const finalResults = resultsGroups
		.filter(({ visible }) => all || visible)
		.map(({ id, name, groupKeys, groupRoutes }) => {
			// Returned value for each group
			const groupResults = {
				id,
				name,
				enabledRoutes: {},
				enabledDisciplines: {},
			};

			addEnabledTrialsToSet(trials, groupRoutes, groupResults);

			const isRouteEnabledForThisRG = (routeId) => {
				return groupResults.enabledRoutes[routeId];
			};

			const indexedGroupKeys = groupKeys.reduce((acc, { field, value }) => {
				if (!acc[field]) acc[field] = [];
				acc[field].push(value);
				return acc;
			}, {});

			// intermediate hash of all users for this group
			const usersInGroup = Object.values(users)
				.filter(({ profile }) => {
					const isInGroup = Object.keys(indexedGroupKeys).every(
						(key) => indexedGroupKeys[key].indexOf(profile[key]) >= 0
					);
					return isInGroup;
				})
				.reduce((acc, user) => {
					acc[user.id] = user;
					return acc;
				}, {});

			// Count of all finishes per route, indexed by route ID
			const statsPerRoute = Object.keys(scores)
				.filter((userId) => usersInGroup[userId])
				.reduce((acc, userId) => {
					const allUserScores = scores[userId];
					Object.keys(allUserScores).forEach((disciplineId) => {
						if (!trials[disciplineId]) return;
						const { routes } = trials[disciplineId];
						const userDisciplineScores = allUserScores[disciplineId];
						Object.keys(userDisciplineScores).forEach((routeId) => {
							if (!routes[routeId]) return;
							if (!isRouteEnabledForThisRG(routeId)) return;
							const scoreForRoute =
								typeof userDisciplineScores[routeId] === 'string'
									? parseFloat(userDisciplineScores[routeId], 10)
									: userDisciplineScores[routeId];
							if (!scoreForRoute) return;
							if (
								compMode !== 'boulder_1zone' &&
								routes[routeId].inputMethod === 'select' &&
								!routes[routeId].points[scoreForRoute]
							)
								return;
							if (!acc[routeId]) {
								acc[routeId] = {
									finishes: 1,
									min: scoreForRoute,
									max: scoreForRoute,
								};
							} else {
								acc[routeId].finishes += 1;
								if (acc[routeId].min > scoreForRoute) {
									acc[routeId].min = scoreForRoute;
								}
								if (acc[routeId].max < scoreForRoute) {
									acc[routeId].max = scoreForRoute;
								}
							}
						});
					});
					return acc;
				}, {});

			// List of all route IDs
			const routesIdsList = Object.values(trials).reduce((acc, discipline) => {
				const { routes } = discipline;

				return acc.concat(Object.keys(routes));
			}, []);

			// Final rankings, array of users and their scores
			groupResults.rankings = Object.values(usersInGroup).map((user) => {
				const userRanking = {
					user,
					total: 0,
					perDiscipline: Object.keys(trials).reduce((acc, disciplineId) => {
						acc[disciplineId] = 0;
						return acc;
					}, {}),
					perRoute: routesIdsList.reduce((acc, routeId) => {
						acc[routeId] = 0;
						return acc;
					}, {}),
				};

				const allUserScores = scores[user.id];
				if (allUserScores) {
					Object.keys(allUserScores).forEach((disciplineId) => {
						let disciplineScores = 0;
						const userDisciplineScores = allUserScores[disciplineId];
						const discipline = trials[disciplineId];
						if (userDisciplineScores && discipline) {
							Object.keys(userDisciplineScores).forEach((routeId) => {
								const rawScore =
									typeof userDisciplineScores[routeId] === 'string'
										? parseFloat(userDisciplineScores[routeId], 10)
										: userDisciplineScores[routeId];
								const route = discipline.routes[routeId];
								if (!rawScore || !route) return;
								if (!isRouteEnabledForThisRG(routeId)) return;
								if (compMode !== 'boulder_1zone' && route.inputMethod === 'select' && !route.points[rawScore]) return;
								const finalScore =
									compMode === 'boulder_1zone'
										? rawScore
										: computeScoreForRoute(rawScore, route, statsPerRoute[routeId]);
								userRanking.perRoute[routeId] = finalScore;
								userRanking.total += finalScore;
								disciplineScores += finalScore;
							});
						}
						userRanking.perDiscipline[disciplineId] = disciplineScores;
					});
				}

				return userRanking;
			}, []);

			if (compMode === 'rank_based') {
				routesIdsList.forEach((routeId) => {
					if (!isRouteEnabledForThisRG(routeId)) return;

					groupResults.rankings = groupResults.rankings
						.sort((a, b) => b.perRoute[routeId] - a.perRoute[routeId])
						.map((ranking) => ({
							ranking,
							rank: 1,
						}))
						.map((data, arrayId, array) => {
							if (arrayId === 0) {
								data.rank = 1;
							} else {
								const prev = array[arrayId - 1];
								if (prev.ranking.perRoute[routeId] > data.ranking.perRoute[routeId]) {
									data.rank = arrayId + 1;
								} else {
									data.rank = prev.rank;
								}
							}
							return data;
						})
						.map(({ ranking, rank }) => {
							ranking.perRoute[routeId] = rank;
							return ranking;
						});
				});

				Object.values(trials).forEach((discipline) => {
					const routeIds = Object.keys(discipline.routes);
					const anyRouteEnabled = routeIds.findIndex((routeId) => isRouteEnabledForThisRG(routeId)) >= 0;
					if (!anyRouteEnabled) return;

					groupResults.rankings.forEach((ranking) => {
						const disciplinePoints = routeIds.reduce((acc, routeId) => {
							if (!isRouteEnabledForThisRG(routeId)) return acc;

							return acc + ranking.perRoute[routeId];
						}, 0);

						ranking.perDiscipline[discipline.id] = disciplinePoints;
					});

					groupResults.rankings
						.sort((a, b) => a.perDiscipline[discipline.id] - b.perDiscipline[discipline.id])
						.map((ranking) => ({
							ranking,
							rank: 1,
						}))
						.map((data, arrayId, array) => {
							if (arrayId === 0) {
								data.rank = 1;
							} else {
								const prev = array[arrayId - 1];
								if (prev.ranking.perDiscipline[discipline.id] < data.ranking.perDiscipline[discipline.id]) {
									data.rank = arrayId + 1;
								} else {
									data.rank = prev.rank;
								}
							}
							return data;
						})
						.map(({ ranking, rank }) => {
							ranking.perDiscipline[discipline.id] = rank;
							return ranking;
						});
				});

				groupResults.rankings.forEach((ranking) => {
					const rankingPoints = Object.values(ranking.perDiscipline).reduce(
						(acc, disciplinePoints) => acc + disciplinePoints,
						0
					);

					ranking.total = rankingPoints;
				});
			}

			return groupResults;
		});

	const rankedAndSortedResults = finalResults.map((resultGroup) => {
		const rankings = resultGroup.rankings
			.map((data) => {
				if (compMode === 'boulder_1zone') {
					return {
						...data,
						total: reverseEncodedBoulderPoints(data.total),
					};
				}

				return data;
			})
			.filter((a) => (skipZeroPoints ? !!a.total : true))
			.sort((a, b) => (COMP_MODES[compMode].reversed ? a.total - b.total : b.total - a.total))
			.map((data, arrayId, array) => {
				if (arrayId === 0) {
					data.rank = 1;
				} else {
					const prev = array[arrayId - 1];
					if (COMP_MODES[compMode].reversed) {
						if (prev.total < data.total) {
							data.rank = arrayId + 1;
						} else {
							data.rank = prev.rank;
						}
					} else {
						if (prev.total > data.total) {
							data.rank = arrayId + 1;
						} else {
							data.rank = prev.rank;
						}
					}
				}
				return data;
			});

		return {
			...resultGroup,
			rankings,
		};
	});

	return rankedAndSortedResults;
}

export function computeProgress(scores, users, trials, resultsGroups, { compMode }) {
	// Produces an object like { userId: [resultGroupId, resultGroupId, ...] }
	const usersGroupsHash = resultsGroups.reduce((acc, { id, groupKeys }) => {
		const indexedGroupKeys = groupKeys.reduce((acc2, { field, value }) => {
			if (!acc2[field]) acc2[field] = [];
			acc2[field].push(value);
			return acc2;
		}, {});

		const usersInGroup = Object.values(users).filter(({ profile }) => {
			const isInGroup = Object.keys(indexedGroupKeys).every((key) => indexedGroupKeys[key].indexOf(profile[key]) >= 0);
			return isInGroup;
		});

		usersInGroup.forEach((user) => {
			if (!acc[user.id]) {
				acc[user.id] = [];
			}
			acc[user.id].push(id);
		}, []);

		return acc;
	}, {});

	// Produces an object where all routeIds are zero, like { routeId: 0 }
	const allRoutesToZero = Object.values(trials).reduce((acc, discipline) => {
		const { routes } = discipline;

		Object.values(routes).forEach((route) => {
			acc[route.id] = 0;
		});

		return acc;
	}, {});

	// Same but for each resultGroupId as index
	const groupsToZero = resultsGroups.reduce((acc, { id }) => {
		acc[id] = { ...allRoutesToZero };
		return acc;
	}, {});

	// Final counters, see initial acc object for the shape
	const counters = Object.keys(scores).reduce(
		(acc, userId) => {
			const allUserScores = scores[userId];
			Object.keys(allUserScores).forEach((disciplineId) => {
				if (!trials[disciplineId]) return;
				const { routes } = trials[disciplineId];

				const routesPoints = allUserScores[disciplineId];
				Object.keys(routesPoints).forEach((routeId) => {
					if (!routes[routeId]) return;
					const scoreForRoute =
						typeof routesPoints[routeId] === 'string' ? parseFloat(routesPoints[routeId], 10) : routesPoints[routeId];
					if (!scoreForRoute) return;
					if (
						compMode !== 'boulder_1zone' &&
						routes[routeId].inputMethod === 'select' &&
						!routes[routeId].points[scoreForRoute]
					)
						return;

					if (!acc.general[routeId]) {
						acc.general[routeId] = 1;
					} else {
						acc.general[routeId]++;
					}

					if (usersGroupsHash[userId]) {
						usersGroupsHash[userId].forEach((resultGroupId) => {
							if (!acc.groups[resultGroupId][routeId]) {
								acc.groups[resultGroupId][routeId] = 1;
							} else {
								acc.groups[resultGroupId][routeId]++;
							}
						});
					}
				});
			});
			return acc;
		},
		{
			groups: groupsToZero,
			general: { ...allRoutesToZero },
		}
	);

	return counters;
}
