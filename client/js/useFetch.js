/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import { useCallback, useEffect, useState } from 'react';

const STATUSES = {
	init: 'init',
	pending: 'pending',
	fulfilled: 'fulfilled',
	rejected: 'rejected',
};

function useFetch(initialUrl, initialOptions = {}) {
	const [url, setUrl] = useState(initialUrl);
	const [options, setOptions] = useState(initialOptions);
	const [json, setJson] = useState(null);
	const [status, setStatus] = useState(STATUSES.init);
	const [error, setError] = useState('');
	const [fetchIndex, setFetchIndex] = useState(0);

	useEffect(() => {
		const fetchData = async () => {
			if (!url) return;
			setStatus(STATUSES.pending);
			try {
				const optionsCopy = { ...options };
				if (optionsCopy.body && !(optionsCopy.body instanceof Blob) && !(optionsCopy.body instanceof File)) {
					optionsCopy.body = new Blob([JSON.stringify(optionsCopy.body)], {
						type: 'application/json',
					});
				}
				const response = await fetch(url, optionsCopy);
				const result = await response.json();

				if (response.ok) {
					setJson(result);
					setStatus(STATUSES.fulfilled);
				} else {
					setError(result);
					setStatus(STATUSES.rejected);
				}
			} catch (err) {
				setError(err);
				setStatus(STATUSES.rejected);
			}
		};

		fetchData();
	}, [url, fetchIndex, options]);

	const reload = useCallback((newUrl, newOptions) => {
		if (typeof newUrl === 'object') {
			setOptions(newUrl);
		} else if (typeof newUrl === 'string') {
			setUrl(newUrl);
			if (typeof newOptions === 'object') {
				setOptions(newOptions);
			}
		}

		setFetchIndex((oldIndex) => oldIndex + 1);
	}, []);

	return [
		{
			inputs: {
				options,
				fetchIndex,
				url,
			},
			error,
			fulfilled: status === STATUSES.fulfilled,
			init: status === STATUSES.init,
			json,
			pending: status === STATUSES.pending,
			rejected: status === STATUSES.rejected,
		},
		reload,
	];
}

export default useFetch;
