/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
export function randomChars(count, pool = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') {
	let ret = '';

	for (let i = 0; i < count; i++) {
		const id = (Math.random() * pool.length) | 0;
		ret += pool[id];
	}

	return ret;
}

const uniqToken = randomChars(10);
let uniqIncrement = 0;

export function uniqId() {
	const p1 = Date.now();
	const p2 = randomChars(10);
	const p3 = uniqIncrement;
	uniqIncrement++;

	return `${p1}-${uniqToken}-${p3}-${p2}`;
}

export function objGet(obj, keys, dft) {
	const keysArray = keys instanceof Array ? keys : `${keys}`.split('.');

	try {
		let ret = obj;
		for (let i = 0; i < keysArray.length; i++) {
			ret = ret[keysArray[i]];
		}
		if (ret === undefined) {
			return dft;
		}
		return ret;
	} catch (e) {
		return dft;
	}
}

export function clone(any) {
	return JSON.parse(JSON.stringify(any));
}

export function indexArrayBy(key, arr) {
	return arr.reduce((acc, itm) => {
		acc[objGet(itm, key, 'undefined')] = itm;
		return acc;
	}, {});
}

export function strPadLeft(str, n, char) {
	let ret = `${str}`;

	while (ret.length < n) {
		ret = `${char}${ret}`;
	}

	return ret;
}

// The only point of these fonctions is to prevent users from updating data in some exported files. They don't have to be cryptographically secure.
export function quickCipher(key, message) {
	const alphabet = '!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~';
	let idxKey = [];
	let ret = '';

	for (let i = 0; i < key.length; i++) {
		const c = key[i];
		const idx = alphabet.indexOf(c);
		if (idx >= 0) {
			idxKey.push(idx);
		}
	}

	for (let i = 0; i < message.length; i++) {
		const c = message[i];
		const idx = alphabet.indexOf(c);
		if (idx >= 0) {
			const shift = idxKey[i % idxKey.length];
			const newC = alphabet[(idx + shift) % alphabet.length];
			ret += newC;
		} else {
			ret += c;
		}
	}

	return ret;
}

export function quickDecipher(key, message) {
	const alphabet = '!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~';
	let idxKey = [];
	let ret = '';

	for (let i = 0; i < key.length; i++) {
		const c = key[i];
		const idx = alphabet.indexOf(c);
		if (idx >= 0) {
			idxKey.push(idx);
		}
	}

	for (let i = 0; i < message.length; i++) {
		const c = message[i];
		const idx = alphabet.indexOf(c);
		if (idx >= 0) {
			const shift = idxKey[i % idxKey.length];
			const newC = alphabet[(idx - shift + alphabet.length) % alphabet.length];
			ret += newC;
		} else {
			ret += c;
		}
	}

	return ret;
}

export function strRemoveDiacritics(str) {
	return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
}
