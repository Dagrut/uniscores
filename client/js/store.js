/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import { createStore } from 'framework7/lite';

const store = createStore({
	state: {
		password: null,
	},
	getters: {
		password({ state }) {
			return state.password;
		},
	},
	actions: {
		setPassword({ state }, password) {
			state.password = password;
		},
		unsetPassword({ state }) {
			state.password = null;
		},
	},
});

export default store;
