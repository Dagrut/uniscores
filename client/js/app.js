/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
// Import React and ReactDOM
import React from 'react';
import { createRoot } from 'react-dom/client';

// Import i18next
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';

// Import Framework7
import Framework7 from 'framework7/lite-bundle';

// Import Framework7-React Plugin
import Framework7React from 'framework7-react';

// Import Framework7 Styles
import 'framework7/css/bundle';

// Import i18next config and translations
import i18nextConfig from '../i18n/config.js';

// Import Icons and App Custom Styles
import '../css/icons.css';
import '../css/app.less';

// Import App Component
import App from '../App.jsx';

i18n.use(initReactI18next).use(LanguageDetector).init(i18nextConfig);

// Init F7 React Plugin
Framework7.use(Framework7React);

// Mount React App
const root = createRoot(document.getElementById('app'));
root.render(React.createElement(App));
