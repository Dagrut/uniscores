/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
const MAX = {
	t: 256,
	z: 256,
	ta: 2048,
	za: 2048,
};
const SHIFTS = {
	t: MAX.za * MAX.ta * MAX.z,
	z: MAX.za * MAX.ta,
	ta: MAX.za,
	za: 1,
};

export function parseBoulderPoints(points) {
	if (typeof points === 'string') {
		points = parseFloat(points);
	}

	return {
		t: Math.floor(points / SHIFTS.t) % MAX.t,
		z: Math.floor(points / SHIFTS.z) % MAX.z,
		ta: Math.floor(points / SHIFTS.ta) % MAX.ta,
		za: Math.floor(points / SHIFTS.za) % MAX.za,
	};
}

export function encodeBoulderPoints(points) {
	const { t, z, ta, za } = points;

	let ret = 0;

	ret += (t % MAX.t) * SHIFTS.t;
	ret += (z % MAX.z) * SHIFTS.z;
	ret += (ta % MAX.ta) * SHIFTS.ta;
	ret += (za % MAX.za) * SHIFTS.za;

	return ret;
}

export function reverseBoulderPoints(points) {
	const { t, z, ta, za } = points;

	return {
		t,
		z,
		ta: MAX.ta - 1 - ta,
		za: MAX.za - 1 - za,
	};
}

export function reverseEncodedBoulderPoints(points) {
	return encodeBoulderPoints(reverseBoulderPoints(parseBoulderPoints(points)));
}
