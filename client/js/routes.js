/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import AdminAppSettings from '../pages/AdminAppSettings.jsx';
import AdminGridScoresInput from '../pages/AdminGridScoresInput.jsx';
import AdminResultsGroupsPage from '../pages/AdminResultsGroupsPage/AdminResultsGroupsPage.jsx';
import AdminRoutesPage from '../pages/AdminRoutesPage.jsx';
import AdminUsersPage from '../pages/AdminUsersPage.jsx';
import AdminUsersProfileFieldsPage from '../pages/AdminUsersProfileFieldsPage.jsx';
import CompStatsPage from '../pages/CompStatsPage.jsx';
import HomePage from '../pages/HomePage.jsx';
import NotFoundPage from '../pages/NotFoundPage.jsx';
import ResultsGridViewPage from '../pages/ResultsGridViewPage.jsx';
import ScoresInputPage from '../pages/ScoresInputPage.jsx';

var routes = [
	{
		path: '/',
		component: HomePage,
	},
	{
		path: '/admin/settings/app',
		component: AdminAppSettings,
	},
	{
		path: '/admin/users',
		component: AdminUsersPage,
	},
	{
		path: '/admin/usersProfileFields',
		component: AdminUsersProfileFieldsPage,
	},
	{
		path: '/admin/resultsGroups',
		component: AdminResultsGroupsPage,
	},
	{
		path: '/admin/routes',
		component: AdminRoutesPage,
	},
	{
		path: '/scores/input',
		component: ScoresInputPage,
	},
	{
		path: '/admin/scores/input/grid',
		component: AdminGridScoresInput,
	},
	{
		path: '/scores/input/:userId',
		component: ScoresInputPage,
	},
	{
		path: '/results/view/grid',
		component: ResultsGridViewPage,
	},
	{
		path: '/comp/progress',
		component: CompStatsPage,
	},

	{
		path: '(.*)',
		component: NotFoundPage,
	},
];

export default routes;
