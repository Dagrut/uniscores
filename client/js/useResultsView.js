import { useContext, useMemo } from 'react';
import useFetch from './useFetch';
import AppConfigContext from '../contexts/AppConfigContext';
import { computeResults } from './resultsComputers';

function useResultsView(all) {
	const [allScores] = useFetch('/api/scores');
	const [allUsers] = useFetch('/api/users');
	const [allTrials] = useFetch('/api/trials');
	const [resultsGroups] = useFetch('/api/resultsGroups');

	const { compMode, skipZeroPoints } = useContext(AppConfigContext);

	const anyIsLoading = useMemo(
		() =>
			allScores.pending ||
			allTrials.pending ||
			allUsers.pending ||
			resultsGroups.pending ||
			allScores.init ||
			allTrials.init ||
			allUsers.init ||
			resultsGroups.init,
		[
			allScores.init,
			allScores.pending,
			allTrials.init,
			allTrials.pending,
			allUsers.init,
			allUsers.pending,
			resultsGroups.init,
			resultsGroups.pending,
		]
	);

	const anyIsError = useMemo(
		() => allScores.rejected || allUsers.rejected || resultsGroups.rejected || allTrials.rejected,
		[allTrials.rejected, allScores.rejected, allUsers.rejected, resultsGroups.rejected]
	);

	const allErrors = useMemo(
		() =>
			[
				allScores.rejected && allScores.error,
				allUsers.rejected && allUsers.error,
				resultsGroups.rejected && resultsGroups.error,
				allTrials.rejected && allTrials.error,
			].filter((x) => x),
		[
			allScores.error,
			allScores.rejected,
			allTrials.error,
			allTrials.rejected,
			allUsers.error,
			allUsers.rejected,
			resultsGroups.error,
			resultsGroups.rejected,
		]
	);

	const results = useMemo(() => {
		if (anyIsLoading || anyIsError) {
			return [];
		}

		const ret = computeResults(allScores.json, allUsers.json, allTrials.json, resultsGroups.json, {
			compMode,
			skipZeroPoints,
			all,
		});

		return ret;
	}, [
		anyIsLoading,
		anyIsError,
		allScores.json,
		allUsers.json,
		allTrials.json,
		resultsGroups.json,
		compMode,
		skipZeroPoints,
		all,
	]);

	return { allErrors, allTrials, anyIsError, anyIsLoading, results };
}

export default useResultsView;
