/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */
import { useCallback, useContext, useEffect, useMemo, useRef } from 'react';
import SessionContext from '../contexts/SessionContext';
import { f7 } from 'framework7-react';
import AppConfigContext from '../contexts/AppConfigContext';
import { useTranslation } from 'react-i18next';

export function useOnTrue(condition, callback) {
	const called = useRef(false);

	useEffect(() => {
		if (condition && !called.current) {
			called.current = true;
			callback();
		} else if (!condition) {
			called.current = false;
		}
	}, [callback, condition]);
}

export function useOnChange(value, callback, firstCall = true) {
	const called = useRef(firstCall ? new Date() : value);

	useEffect(() => {
		if (value !== called.current) {
			called.current = value;
			callback();
		}
	}, [callback, value]);
}

export function useOnMount(callback) {
	const called = useRef(false);

	useEffect(() => {
		if (!called.current) {
			called.current = true;
			callback();
		}
	}, [callback]);
}

export function useOnUnmount(callback) {
	useEffect(() => {
		return callback;
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);
}

export function usePerms({
	admin: checkIsAdmin = false,
	canInputScores = false,
	canDisplayScores = false,
	canDisplayStats = false,
}) {
	const { connected, loading: sessionLoading, user = false, isAdmin = false } = useContext(SessionContext);
	const {
		compIsOpen,
		compIsStarted,
		displayStats,
		displayResultsAfter,
		displayResultsDuring,
		loading: confLoading,
	} = useContext(AppConfigContext);

	useEffect(() => {
		const redirect = () => {
			setTimeout(() => {
				f7.views.main.router.navigate('/', {
					reloadAll: true,
					clearPreviousHistory: true,
					history: false,
					ignoreCache: true,
					animate: false,
				});
			}, 500);
		};

		if (sessionLoading || confLoading) return;

		if (checkIsAdmin && !isAdmin) {
			redirect();
		}
		if (canInputScores && !isAdmin) {
			if (!connected || !compIsOpen || !compIsStarted) {
				redirect();
			}
		}
		if (canDisplayScores && !isAdmin) {
			if (compIsStarted && !displayResultsDuring) {
				redirect();
			} else if (!compIsStarted && !displayResultsAfter) {
				redirect();
			}
		}
		if (canDisplayStats && !isAdmin) {
			if (!displayStats) {
				redirect();
			}
		}
	}, [
		canDisplayScores,
		canDisplayStats,
		canInputScores,
		checkIsAdmin,
		compIsOpen,
		compIsStarted,
		confLoading,
		connected,
		displayResultsAfter,
		displayResultsDuring,
		displayStats,
		isAdmin,
		sessionLoading,
		user,
	]);
}

export function useErrorTranslator() {
	const { t } = useTranslation('ErrorMessage');

	const errorTranslations = useMemo(
		() => ({
			apiNotFound: t('apiNotFound'),
			badCredentials: t('badCredentials'),
			forbidden: t('forbidden'),
			invalidDataType: t('invalidDataType'),
			noPasswordSet: t('noPasswordSet'),
			resultGroupNotFound: t('resultGroupNotFound'),
			routeNotFound: t('routeNotFound'),
			trialsDisciplineNotFound: t('trialsDisciplineNotFound'),
			userNotFound: t('userNotFound'),
			usernameExists: t('usernameExists'),
			badConfExportFormat: t('badConfExportFormat'),
		}),
		[t]
	);

	return useCallback(
		(text) => {
			if (errorTranslations[text]) {
				return errorTranslations[text];
			}
			return text;
		},
		[errorTranslations]
	);
}
