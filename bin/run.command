#!/bin/bash
# Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file ::

cd $(dirname "$0")/..

if test '!' -x ./nodejs/npm; then
	echo NodeJS not found, downloading...
	wget -O nodejs.tar.gz 'https://nodejs.org/dist/v20.9.0/node-v20.9.0-darwin-x64.tar.gz'
	echo Extracting...
	tar -xvzf nodejs.tar.gz
	mv node-v20.9.0-darwin-x64 nodejs
	rm nodejs.tar.gz
fi

export PATH="$PWD/nodejs/bin:$PATH"

if test -d ./node_modules; then
	echo Modules OK
else
	echo Modules not found, downloading...
	npm install
fi

if test -d ./www; then
	echo Client OK
else
	echo Client not found, building...
	npm run build
fi

echo Starting server...
npm run start
