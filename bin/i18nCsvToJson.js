/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */

const fs = require('fs');
const path = require('path');

function parseCsvToArray(csvContent) {
	const QUOTE = '"';
	const COMMA = ',';

	const csvArray = [];
	let currentLine = [];
	let currentCell = '';
	let quotes = 0;
	let lastChar = null;

	for (let i = 0; i < csvContent.length; i++) {
		const c = csvContent[i];

		if (!quotes) {
			if (c === QUOTE) {
				quotes = 1;
			} else if (c === COMMA) {
				currentLine.push(currentCell);
				currentCell = '';
			} else if (c === '\r') {
				/* Skip */
			} else if (c === '\n') {
				currentLine.push(currentCell);
				currentCell = '';
				csvArray.push(currentLine);
				currentLine = [];
			} else {
				currentCell += c;
			}
		} else {
			if (c === QUOTE) {
				quotes++;
				if (quotes === 3) {
					currentCell += QUOTE;
					quotes = 1;
				}
			} else if (lastChar === QUOTE && quotes === 2) {
				quotes = 0;
				i--;
			} else {
				currentCell += c;
			}
		}

		lastChar = c;
	}

	if (lastChar !== '\n') {
		currentLine.push(currentCell);
		currentCell = '';
		csvArray.push(currentLine);
		currentLine = [];
	}

	return csvArray;
}

function csvArrayToI18nKeysMap(csvArray) {
	const csvKeys = csvArray.shift();
	csvKeys.shift(); // Remove the translation keys header line

	csvArray.sort((a, b) => {
		if (a[0] > b[0]) return 1;
		if (a[0] < b[0]) return -1;
		return 0;
	});

	const csvHash = csvArray.reduce((acc, row) => {
		const langs = csvKeys.reduce((acc2, key, id) => {
			acc2[key] = row[id + 1];
			return acc2;
		}, {});

		acc[row[0]] = langs;

		return acc;
	}, {});

	return csvHash;
}

function objSet(obj, keys, data) {
	const keysArray = keys instanceof Array ? keys : `${keys}`.split('.');

	let ret = obj;
	const lastKey = keysArray.pop();

	for (let i = 0; i < keysArray.length; i++) {
		const key = keysArray[i];
		if (typeof ret[key] !== 'object') {
			ret[key] = {};
		}
		ret = ret[key];
	}

	ret[lastKey] = data;

	return ret;
}

function decodeTranslations(hash, langs) {
	const ret = langs.reduce((acc, lang) => {
		acc[lang] = {};
		return acc;
	}, {});

	Object.keys(hash).forEach((key) => {
		const allTs = hash[key];

		langs.forEach((lang) => {
			objSet(ret[lang], key, allTs[lang] || '');
		});
	});

	return ret;
}

async function writeTranslationFiles(hash, langs, i18nDir) {
	for (let i = 0; i < langs.length; i++) {
		const lang = langs[i];
		const files = Object.keys(hash[lang]);

		for (let j = 0; j < files.length; j++) {
			const file = files[j];
			const fileName = `${file}.json`;
			const json = JSON.stringify(hash[lang][file], null, '\t');

			await fs.promises.writeFile(path.join(i18nDir, lang, fileName), json + '\n', { encoding: 'utf8' });
		}
	}
}

async function main() {
	const relativeI18nDir = path.join('client', 'i18n', 'locales');
	const relativeCsvFile = path.join('client', 'i18n', 'locales.csv');
	const i18nDir = path.join(__dirname, '..', relativeI18nDir);
	const csvFile = path.join(__dirname, '..', relativeCsvFile);

	const csvData = await fs.promises.readFile(csvFile, { encoding: 'utf8' });

	const csvArrayData = parseCsvToArray(csvData);
	const langs = csvArrayData[0].slice(1);
	const translationsHash = csvArrayToI18nKeysMap(csvArrayData);

	const decodedTranslationsHash = decodeTranslations(translationsHash, langs);

	await writeTranslationFiles(decodedTranslationsHash, langs, i18nDir);

	console.log(`CSV file "${relativeCsvFile}" exported to JSON files in "${relativeI18nDir}/".`);
}

main();
