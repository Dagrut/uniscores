/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */

const fs = require('fs');
const path = require('path');

async function readAllData(i18nDir) {
	const langs = await fs.promises.readdir(i18nDir);

	const allI18nData = {};

	for (let i = 0; i < langs.length; i++) {
		const lang = langs[i];
		allI18nData[lang] = {};

		const allFiles = await fs.promises.readdir(path.join(i18nDir, lang));
		const jsonFiles = allFiles.filter((fname) => fname.match(/\.json$/));

		for (let j = 0; j < jsonFiles.length; j++) {
			const jsonFile = jsonFiles[j];
			const prefix = jsonFile.replace(/\.json$/, '');

			const json = await fs.promises.readFile(path.join(i18nDir, lang, jsonFile), { encoding: 'utf8' });
			allI18nData[lang][prefix] = JSON.parse(json);
		}
	}

	return allI18nData;
}

function escapeCsvEntry(rawValue) {
	const value = `${rawValue}`;
	if (value.match(/[,"\n\r]/)) {
		return `"${value.replace(/"/g, '""')}"`;
	}

	return value;
}

function encodeCsvLine(line) {
	return line.map(escapeCsvEntry).join(',');
}

function flattenI18nData(allI18nData) {
	const langs = Object.keys(allI18nData);
	const emptyLangsObj = langs.reduce((acc, lang) => {
		acc[lang] = '';
		return acc;
	}, {});

	const flattenedKvs = {};
	function flatten(lang, obj, currentKey = []) {
		if (typeof obj === 'object') {
			Object.keys(obj).forEach((key) => {
				flatten(lang, obj[key], [...currentKey, key]);
			});
		} else {
			const finalKey = currentKey.join('.');
			if (!flattenedKvs[finalKey]) {
				flattenedKvs[finalKey] = { ...emptyLangsObj };
			}
			flattenedKvs[finalKey][lang] = obj;
		}
	}

	langs.forEach((lang) => {
		flatten(lang, allI18nData[lang]);
	});

	return flattenedKvs;
}

async function main() {
	const relativeI18nDir = path.join('client', 'i18n', 'locales');
	const relativeCsvFile = path.join('client', 'i18n', 'locales.csv');
	const i18nDir = path.join(__dirname, '..', relativeI18nDir);
	const csvFile = path.join(__dirname, '..', relativeCsvFile);

	const allI18nData = await readAllData(i18nDir);
	const langs = Object.keys(allI18nData);

	const flattenedI18nData = flattenI18nData(allI18nData);

	const csvData = [encodeCsvLine(['Key', ...langs])];
	Object.keys(flattenedI18nData)
		.sort()
		.forEach((key) => {
			const line = [key, ...langs.map((lang) => flattenedI18nData[key][lang] || '')];
			csvData.push(encodeCsvLine(line));
		});

	const csvStr = csvData.join('\n');

	await fs.promises.writeFile(csvFile, csvStr, { encoding: 'utf8' });

	console.log(`JSON files in "${relativeI18nDir}/" exported to CSV file "${relativeCsvFile}".`);
}

main();
