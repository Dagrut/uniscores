:: Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file ::

@ECHO off
SETLOCAL

cd "%~dp0\.."

IF NOT EXIST .\nodejs\npm.cmd (
  ECHO NodeJS not found, downloading...
	CALL powershell -Command "(New-Object Net.WebClient).DownloadFile('https://nodejs.org/download/release/v16.20.2/node-v16.20.2-win-x64.zip', 'nodejs.zip')"
  ECHO Extracting...
	CALL :UnZipFile "%CD%" "%CD%\nodejs.zip"
	RENAME node-v16.20.2-win-x64 nodejs
	DEL nodejs.zip
)

SET "PATH=%CD%\nodejs;%PATH%"

IF EXIST .\node_modules\ (
  ECHO Modules OK
) ELSE (
  ECHO Modules not found, downloading...
	CALL .\nodejs\npm.cmd install
)

IF EXIST .\www\ (
  ECHO Client OK
) ELSE (
  ECHO Client not found, building...
	CALL .\nodejs\npm.cmd run build
)

ECHO Starting server...
CALL .\nodejs\npm.cmd run start

:UnZipFile <ExtractTo> <newzipfile>
SET vbs="%temp%\_.vbs"
IF EXIST %vbs% DEL /f /q %vbs%
>%vbs% ECHO Set fso = CreateObject("Scripting.FileSystemObject")
>>%vbs% ECHO If NOT fso.FolderExists(%1) Then
>>%vbs% ECHO fso.CreateFolder(%1)
>>%vbs% ECHO End If
>>%vbs% ECHO set objShell = CreateObject("Shell.Application")
>>%vbs% ECHO set FilesInZip=objShell.NameSpace(%2).items
>>%vbs% ECHO objShell.NameSpace(%1).CopyHere(FilesInZip)
>>%vbs% ECHO Set fso = Nothing
>>%vbs% ECHO Set objShell = Nothing
cscript //nologo %vbs%
IF EXIST %vbs% DEL /f /q %vbs%
