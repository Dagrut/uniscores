'use strict';
/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */

module.exports = {
	HTTP_PORT: process.env.SRV_UNISCORES_SERVER_PORT,
	DATA_DIR: process.env.SRV_UNISCORES_DATA_DIR,
	UNRELIABLE_RTC: process.env.SRV_UNISCORES_UNRELIABLE_RTC,
};
