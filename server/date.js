'use strict';
/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */

/* File mostly inspired from https://github.com/will123195/set-now */

const DateDefault = Date;

let delta = 0;

function getDelta() {
	return delta;
}

function setNow(...args) {
	if (args.length === 0) {
		delta = 0;
		return;
	}
	delta = new DateDefault(...args).getTime() - DateDefault.now();
}

function DateModified(...args) {
	if (args.length === 0) {
		return new DateDefault(DateDefault.now() + delta);
	}
	return new DateDefault(...args);
}

DateModified.prototype = DateDefault.prototype;
DateModified.now = () => DateDefault.now() + delta;
DateModified.UTC = DateDefault.UTC;
DateModified.parse = DateDefault.parse;

global.Date = DateModified;
module.exports.DateModified = DateModified;
module.exports.DateDefault = DateDefault;
module.exports.setNow = setNow;
module.exports.getDelta = getDelta;
