'use strict';
/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */

const path = require('path');
const fs = require('fs');

const { DATA_DIR } = require('./env.js');

function FileDatabase(path, formatter) {
	this.path = path;
	this.data = null;
	this.formatter = formatter;
}

FileDatabase.prototype.read = async function () {
	try {
		const newData = await fs.promises.readFile(this.path, { encoding: 'utf8' });

		this.data = this.formatter.parse(newData);
	} catch (e) {
		this.error = e;
		throw e;
	}
};

FileDatabase.prototype.write = async function () {
	try {
		await fs.promises.writeFile(this.path, this.formatter.stringify(this.data, null, 2), { encoding: 'utf8' });
	} catch (e) {
		this.error = e;
		throw e;
	}
};

async function loadDatabase(name, defaultValue, { formatter = JSON, extension = 'json' } = {}) {
	const db = new FileDatabase(
		path.join(DATA_DIR || path.join(__dirname, '..', 'data'), `db.${name}.${extension}`),
		formatter
	);

	try {
		await db.read();
	} catch (e) {
		if (e.code !== 'ENOENT') {
			throw e;
		}
	}

	db.reset = async () => {
		db.data = formatter.parse(formatter.stringify(defaultValue));
		await db.write();
	};

	if (db.data === null) {
		await db.reset();
	}

	return db;
}

module.exports.loadDatabase = loadDatabase;
