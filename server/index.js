'use strict';
/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */

// Load date first to override any use of Date before loading anything
require('./date.js');

const path = require('path');
const express = require('express');
const cookieParser = require('cookie-parser');

const { HTTP_PORT } = require('./env.js');
const apiLoader = require('./api.js');
const init = require('./init.js');
const { Logger } = require('./logs.js');

const app = express();
const logger = new Logger('queries');

(async () => {
	const srvConf = await init();
	const api = apiLoader(srvConf);

	app.use(cookieParser());

	app.use((req, _res, next) => {
		const ip = req.socket.remoteAddress;
		logger.log(`[${ip}] "${req.method} ${req.url}"`);
		next();
	});

	app.use(express.static(path.join(__dirname, '../www')));
	app.use(express.json());

	app.use('/api', api);

	app.use((_req, res) => {
		res.status(200).sendFile(path.join(__dirname, '../www/index.html'));
	});

	const port = HTTP_PORT || 8080;
	app.listen(port, () => {
		const srvLogger = new Logger('server');
		srvLogger.log(`App listening on port ${port}`);
		srvLogger.log('Press Ctrl+C to quit.');
	});
})();
