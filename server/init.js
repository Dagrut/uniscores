'use strict';
/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */

const path = require('path');
const fs = require('fs');
const crypto = require('crypto');
const YAML = require('yaml');

const { loadDatabase } = require('./fileDatabase.js');
const { Logger } = require('./logs.js');
const { randomHexStr } = require('./utils.js');
const { storageRegister } = require('./storage.js');
const { DATA_DIR } = require('./env.js');

const logger = new Logger('init');

const userProfileFieldsDefault = [
	{
		key: 'gender',
		name: 'Genre',
		ts: {
			fr: 'Genre',
			en: 'Gender',
		},
		values: [
			{ key: 'M', name: 'Homme', ts: { fr: 'Homme', en: 'Man' } },
			{ key: 'W', name: 'Femme', ts: { fr: 'Femme', en: 'Woman' } },
		],
	},
	{
		key: 'category',
		name: 'Catégorie',
		ts: {
			fr: 'Catégorie',
			en: 'Category',
		},
		values: [
			{ key: 'U8', name: 'U8 (Koala)', ts: { fr: 'U8 (Koala)', en: 'U8' } },
			{ key: 'U10', name: 'U10 (Microbe)', ts: { fr: 'U10 (Microbe)', en: 'U10' } },
			{ key: 'U12', name: 'U12 (Poussin)', ts: { fr: 'U12 (Poussin)', en: 'U12' } },
			{ key: 'U14', name: 'U14 (Benjamin)', ts: { fr: 'U14 (Benjamin)', en: 'U14' } },
			{ key: 'U16', name: 'U16 (Minime)', ts: { fr: 'U16 (Minime)', en: 'U16' } },
			{ key: 'U18', name: 'U18 (Cadet)', ts: { fr: 'U18 (Cadet)', en: 'U18' } },
			{ key: 'U20', name: 'U20 (Junior)', ts: { fr: 'U20 (Junior)', en: 'U20' } },
			{ key: 'U40', name: 'U40 (Sénior)', ts: { fr: 'U40 (Sénior)', en: 'U40' } },
			{ key: 'U60', name: 'U60 (Vétéran)', ts: { fr: 'U60 (Vétéran)', en: 'U60' } },
		],
	},
];

async function init() {
	await fs.promises.mkdir(DATA_DIR || path.join(__dirname, '..', 'data'), { recursive: true });

	const srvConf = {
		db: {},
		fn: {},
	};

	const databases = {
		config: {
			params: [{}],
		},
		resultsGroups: {
			params: [[]],
		},
		scores: {
			params: [{}],
		},
		trials: {
			params: [{}],
		},
		users: {
			params: [{ ids: {}, usernames: {} }],
		},
		userProfileFields: {
			params: [userProfileFieldsDefault, { formatter: YAML, extension: 'yaml' }],
		},
	};
	const promises = Object.keys(databases).map(async (name) => {
		logger.info(`Loading ${name} database`);
		srvConf.db[name] = await loadDatabase(name, ...databases[name].params);
	});

	await Promise.all(promises);

	const passwordsSaltStorage = storageRegister('secret', 'passwordsSalt', 'data', () => crypto.randomBytes(30));

	srvConf.fn.hashPassword = async function hashPassword(password) {
		const passwordsSalt = await passwordsSaltStorage.read();
		const hashed = crypto.createHmac('sha256', passwordsSalt).update(password).digest('hex');

		return hashed;
	};

	logger.info('Loading secret.jwtSecret.txt file');
	await storageRegister('secret', 'jwtSecret', 'txt', () => randomHexStr(32)).read();

	logger.info('Loading secret.adminPassword.txt file');
	await storageRegister(
		'secret',
		'adminPassword',
		'txt',
		async () => await srvConf.fn.hashPassword('uniscores')
	).read();

	return srvConf;
}

module.exports = init;
