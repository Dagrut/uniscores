'use strict';
/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */

const express = require('express');
const zlib = require('zlib');
const jwt = require('jsonwebtoken');

const { Logger } = require('./logs.js');
const { storageGet } = require('./storage.js');
const { quickSrvCipher, quickSrvDecipher } = require('./utils.js');
const { UNRELIABLE_RTC } = require('./env.js');
const date = require('./date.js');

const logger = new Logger('api');

const SESSION_COOKIE_NAME = 'userSession';
const SESSION_DURATION_SEC = 7 * 24 * 60 * 60;
const SESSION_DURATION_MSEC = SESSION_DURATION_SEC * 1000;
const SESSION_REFRESH_SEC = SESSION_DURATION_SEC - 1 * 24 * 60 * 60;
const UNISCORES_EXPORT_PASSWORD =
	'"mFVK|KZH&`+|[#8?8B/i[=?w5J:&D+y#&!)B*QM`x]7V}J#&k4jI>eB2NI7G>AX3{6\'Jrp#nhUOzbBaYXf<@R+Ace<j~&';

const adminPasswordStorage = storageGet('secret', 'adminPassword', 'txt');
const jwtSessionsSecretStorage = storageGet('secret', 'jwtSecret', 'txt');
const passwordsSaltStorage = storageGet('secret', 'passwordsSalt', 'data');

const exportsMemDB = {};
let exportsChecksTimeoutId = null;
let pendingExportsChecks = false;

function scheduleExportsCleanup() {
	if (exportsChecksTimeoutId !== null) {
		pendingExportsChecks = true;
		return;
	}

	function check() {
		const now = Date.now();
		Object.keys(exportsMemDB).forEach((key) => {
			if (exportsMemDB[key.expires] >= now) {
				delete exportsMemDB[key.expires];
			}
		});

		if (pendingExportsChecks) {
			exportsChecksTimeoutId = setTimeout(check, 60 * 1000);
		} else {
			exportsChecksTimeoutId = null;
		}

		pendingExportsChecks = false;
	}

	exportsChecksTimeoutId = setTimeout(check, 60 * 1000);
}

function nowSec() {
	const nowMs = Date.now();
	return Math.floor(nowMs / 1000);
}

function cookieOpts(expired = false) {
	return {
		httpOnly: true,
		expires: expired ? new Date(0) : new Date(Date.now() + SESSION_DURATION_MSEC),
	};
}

async function writeAndDiscardError(db) {
	try {
		await db.write();
	} catch (e) {
		logger.error('Caught discarded exception', e);
	}
}

async function newJwtAdminSessionToken() {
	const jwtSessionsSecret = await jwtSessionsSecretStorage.read();
	const sessionToken = jwt.sign(
		{
			exp: Math.floor(Date.now() / 1000) + SESSION_DURATION_SEC,
			data: { isAdmin: true },
		},
		jwtSessionsSecret
	);

	return sessionToken;
}

async function newJwtUserSessionToken(userId) {
	const jwtSessionsSecret = await jwtSessionsSecretStorage.read();
	const sessionToken = jwt.sign(
		{
			exp: Math.floor(Date.now() / 1000) + SESSION_DURATION_SEC,
			data: { userId },
		},
		jwtSessionsSecret
	);

	return sessionToken;
}

function checkIsAdmin(req) {
	if (!req.session) {
		return false;
	} else if (!req.session.data.isAdmin) {
		return false;
	}

	return true;
}

function checkIsAuth(req) {
	if (!req.session) {
		return false;
	}

	return true;
}

function apiLoader(srvConf) {
	const {
		db: {
			resultsGroups: resultsGroupsDb,
			trials: trialsDb,
			scores: scoresDb,
			users: usersDb,
			config: configDb,
			userProfileFields: userProfileFieldsDb,
		},
		fn: { hashPassword },
	} = srvConf;
	const api = express.Router();

	api.use(async (req, res, next) => {
		res.jsonError = (code, message) => {
			res.status(code).json({ message });
		};
		res.forbidden = () => {
			res.jsonError(403, 'forbidden');
		};
		const sessionCookie = req.cookies[SESSION_COOKIE_NAME];
		if (sessionCookie) {
			try {
				const jwtSessionsSecret = await jwtSessionsSecretStorage.read();
				const decoded = jwt.verify(sessionCookie, jwtSessionsSecret);

				// Cookie refresh when expiration date gets closer
				if (decoded.exp - nowSec() <= SESSION_REFRESH_SEC) {
					if (decoded.data.isAdmin) {
						const sessionToken = await newJwtAdminSessionToken();
						res.cookie(SESSION_COOKIE_NAME, sessionToken, cookieOpts());
					} else {
						const sessionToken = await newJwtUserSessionToken(decoded.data.userId);
						res.cookie(SESSION_COOKIE_NAME, sessionToken, cookieOpts());
					}
				}
				req.session = decoded;
			} catch (e) {
				logger.log('Could not decode session token', e);
				res.cookie(SESSION_COOKIE_NAME, '', cookieOpts(true));
			}
		}

		next();
	});

	api.get('/app/config', (_req, res) => {
		res.json(configDb.data);
	});

	api.get('/app/export', async (req, res) => {
		if (!checkIsAdmin(req)) {
			res.forbidden();
			return;
		}

		const exportedFileName = `Uniscores-export-${new Date().toISOString().replace(/:/g, '-')}.gz`;
		res.setHeader('Content-disposition', `attachment; filename=${exportedFileName}`);
		res.setHeader('Content-type', 'application/octet-stream');

		const jsonConf = JSON.stringify({
			config: configDb.data,
			resultsGroups: resultsGroupsDb.data,
			scores: scoresDb.data,
			trials: trialsDb.data,
			userProfileFields: userProfileFieldsDb.data,
			users: usersDb.data,

			adminPassword: await adminPasswordStorage.read(),
			jwtSessionsSecret: await jwtSessionsSecretStorage.read(),
			passwordsSalt: (await passwordsSaltStorage.read()).toJSON(),
		});
		const cipheredJsonConf = quickSrvCipher(UNISCORES_EXPORT_PASSWORD, jsonConf);
		const gzip = zlib.createGzip();
		gzip.pipe(res);
		gzip.end(Buffer.from(cipheredJsonConf, 'utf8'));
	});

	api.put('/app/import', express.raw({ type: '*/*', limit: '10Mb' }), (req, res) => {
		if (!checkIsAdmin(req)) {
			res.forbidden();
			return;
		}

		zlib.gunzip(req.body, async (err, jsonConf) => {
			if (err) {
				res.jsonError(400, 'badConfExportFormat');
				return;
			}

			try {
				const decipheredJsonConf = quickSrvDecipher(UNISCORES_EXPORT_PASSWORD, jsonConf.toString('utf8'));
				const data = JSON.parse(decipheredJsonConf);
				const dbMap = {
					config: configDb,
					resultsGroups: resultsGroupsDb,
					scores: scoresDb,
					trials: trialsDb,
					userProfileFields: userProfileFieldsDb,
					users: usersDb,
				};

				Object.keys(dbMap).forEach((key) => {
					if (data[key]) {
						dbMap[key].data = data[key];
						writeAndDiscardError(dbMap[key]);
					}
				});

				if (data['adminPassword']) {
					await adminPasswordStorage.write(data['adminPassword']);
				}
				if (data['jwtSessionsSecret']) {
					await jwtSessionsSecretStorage.write(data['jwtSessionsSecret']);
				}
				if (data['passwordsSalt']) {
					await passwordsSaltStorage.write(Buffer.from(data['passwordsSalt']));
				}
			} catch (e) {
				res.jsonError(400, 'badConfExportFormat');
				return;
			}

			res.json({ ok: true });
		});
	});

	api.put('/app/reset', async (req, res) => {
		if (!checkIsAdmin(req)) {
			res.forbidden();
			return;
		}

		const dbs = [configDb, resultsGroupsDb, scoresDb, trialsDb, userProfileFieldsDb, usersDb];

		const promises = dbs.map(async (db) => {
			await db.reset();
		});

		await Promise.all(promises);

		await passwordsSaltStorage.reset();
		await jwtSessionsSecretStorage.reset();
		await adminPasswordStorage.reset();

		res.json({ ok: true });
	});

	api.post('/app/config', (req, res) => {
		if (!checkIsAdmin(req)) {
			res.forbidden();
			return;
		}

		configDb.data = req.body;
		res.json({ ok: true });
		writeAndDiscardError(configDb);
	});

	api.get('/auth/status', (req, res) => {
		if (!checkIsAuth(req)) {
			res.jsonError(400, 'forbidden');
			return;
		}
		if (checkIsAdmin(req)) {
			res.json({
				isAdmin: true,
			});
			return;
		}

		const user = usersDb.data.ids[req.session.data.userId];

		if (!user) {
			res.jsonError(400, 'forbidden');
			return;
		}

		res.json({
			user: {
				id: user.id,
				password: !!user.password,
				username: user.username,
				profile: user.profile,
			},
		});
	});

	api.put('/auth/logout', (req, res) => {
		if (!checkIsAuth(req)) {
			res.json({ ok: true });
			return;
		}
		res.cookie(SESSION_COOKIE_NAME, '', cookieOpts(true));
		res.json({ ok: true });
	});

	api.post('/adminPassword', async (req, res) => {
		if (!checkIsAdmin(req)) {
			res.forbidden();
			return;
		}

		const hashedPassword = await hashPassword(req.body.password);
		await adminPasswordStorage.write(hashedPassword);

		res.json({ ok: true });
	});

	api.post('/auth/login', async (req, res) => {
		const { username, password } = req.body;

		const adminPasswordHash = await adminPasswordStorage.read();

		const hashedPassword = await hashPassword(password);

		if (username === 'admin' && hashedPassword === adminPasswordHash) {
			const sessionToken = await newJwtAdminSessionToken();
			res.cookie(SESSION_COOKIE_NAME, sessionToken, cookieOpts());
			res.json({ admin: true });
			return;
		}

		if (!usersDb.data.usernames[username]) {
			res.jsonError(400, 'badCredentials');
			return;
		}
		const id = usersDb.data.usernames[username];
		const user = usersDb.data.ids[id];

		if (user.password !== null && hashedPassword !== user.password) {
			res.jsonError(400, 'badCredentials');
			return;
		}

		const sessionToken = await newJwtUserSessionToken(id);

		res.cookie(SESSION_COOKIE_NAME, sessionToken, cookieOpts());
		res.json({
			user: {
				id: user.id,
				password: !!user.password,
				profile: user.profile,
				username: user.username,
			},
		});
	});

	api.get('/userProfileFields', (req, res) => {
		if (!checkIsAdmin(req)) {
			res.forbidden();
			return;
		}

		res.json(userProfileFieldsDb.data);
	});

	api.post('/userProfileFields', (req, res) => {
		if (!checkIsAdmin(req)) {
			res.forbidden();
			return;
		}

		userProfileFieldsDb.data = req.body;

		res.json({ ok: true });
		writeAndDiscardError(userProfileFieldsDb);
	});

	api.put('/userProfileFields/applyField', (req, res) => {
		if (!checkIsAdmin(req)) {
			res.forbidden();
			return;
		}

		const { field, value, undefinedOnly = true, notIn = [] } = req.body;

		Object.keys(usersDb.data.ids).forEach((id) => {
			const profile = usersDb.data.ids[id].profile;
			if (undefinedOnly) {
				if (profile[field] === undefined) {
					profile[field] = value;
				}
			} else if (notIn.indexOf(profile[field]) < 0) {
				profile[field] = value;
			}
		});

		res.json({ ok: true });
		writeAndDiscardError(usersDb);
	});

	api.get('/users', (_req, res) => {
		const ret = Object.values(usersDb.data.ids)
			.map(({ id, username, password, profile }) => ({
				id,
				username,
				password: !!password,
				profile,
			}))
			.reduce((acc, user) => {
				acc[user.id] = user;
				return acc;
			}, {});
		res.json(ret);
	});

	api.get('/users/:userId', (req, res) => {
		const user = usersDb.data.ids[req.params.userId];
		res.json({
			id: user.id,
			username: user.username,
			password: !!user.password,
			profile: user.profile,
		});
	});

	api.post('/users', async (req, res) => {
		if (!checkIsAdmin(req)) {
			res.forbidden();
			return;
		}

		const { id, username, password, profile } = req.body;
		const newUser = {
			id,
			password: null,
			profile,
			username,
		};
		if (password) {
			newUser.password = await hashPassword(password);
		}

		if (usersDb.data.usernames[username]) {
			res.jsonError(400, 'usernameExists');
			return;
		}

		usersDb.data.usernames[username] = id;
		usersDb.data.ids[id] = newUser;
		res.json({
			id,
			password: !!password,
			profile,
			username,
		});
		writeAndDiscardError(usersDb);
	});

	api.patch('/users/:userId', async (req, res) => {
		if (!checkIsAdmin(req)) {
			res.forbidden();
			return;
		}

		const oldUser = usersDb.data.ids[req.params.userId];
		if (!oldUser) {
			res.jsonError(404, 'userNotFound');
			return;
		}
		const { username, password, profile } = req.body;
		const id = req.params.userId;
		const newUser = {
			id,
			username,
			password: password ? await hashPassword(password) : oldUser.password,
			profile,
		};

		if (usersDb.data.usernames[username] && usersDb.data.usernames[username] !== id) {
			res.jsonError(400, 'usernameExists');
			return;
		}

		if (username !== oldUser.username) {
			delete usersDb.data.usernames[oldUser.username];
		}

		usersDb.data.usernames[username] = id;
		usersDb.data.ids[id] = newUser;

		res.json({
			id,
			username,
			profile,
		});
		writeAndDiscardError(usersDb);
	});

	api.delete('/users/:userId', (req, res) => {
		if (!checkIsAdmin(req)) {
			res.forbidden();
			return;
		}

		const user = usersDb.data.ids[req.params.userId];
		if (!user) {
			res.jsonError(404, 'userNotFound');
			return;
		}

		delete usersDb.data.ids[req.params.userId];
		delete usersDb.data.usernames[user.username];

		res.json({
			ok: true,
			user: {
				id: user.id,
				username: user.username,
				profile: user.profile,
			},
		});
		writeAndDiscardError(usersDb);
	});

	api.put('/users/:userId/resetPassword', async (req, res) => {
		if (!checkIsAdmin(req)) {
			res.forbidden();
			return;
		}

		const user = usersDb.data.ids[req.params.userId];
		if (!user) {
			res.jsonError(404, 'userNotFound');
			return;
		}

		user.password = null;

		res.json({ ok: true });
		writeAndDiscardError(usersDb);
	});

	api.put('/user/setPassword', async (req, res) => {
		if (!checkIsAuth(req) || checkIsAdmin(req)) {
			res.forbidden();
			return;
		}

		const { password } = req.body;
		const user = usersDb.data.ids[req.session.data.userId];

		if (!password) {
			res.jsonError(400, 'noPasswordSet');
			return;
		}

		if (!user) {
			res.jsonError(404, 'userNotFound');
			return;
		}

		user.password = await hashPassword(password);

		res.json({ ok: true });
		writeAndDiscardError(usersDb);
	});

	api.get('/scores', (_req, res) => {
		res.json(scoresDb.data);
	});

	api.post('/scores/:userId', (req, res) => {
		if (!checkIsAuth(req)) {
			res.forbidden();
			return;
		}

		let userId = null;
		if (checkIsAdmin(req)) {
			userId = req.params.userId;
		} else {
			userId = req.session.data.userId;
		}

		req.body.forEach(({ disciplineId, routeId, score }) => {
			if (trialsDb.data[disciplineId] && trialsDb.data[disciplineId].routes[routeId]) {
				if (!scoresDb.data[userId]) {
					scoresDb.data[userId] = {};
				}
				if (!scoresDb.data[userId][disciplineId]) {
					scoresDb.data[userId][disciplineId] = {};
				}
				scoresDb.data[userId][disciplineId][routeId] = score;
			}
		});

		res.json({
			ok: true,
			userScores: scoresDb.data[req.session.data.userId],
		});
		writeAndDiscardError(scoresDb);
	});

	api.get('/scores/:userId', (req, res) => {
		if (!checkIsAuth(req)) {
			res.forbidden();
			return;
		}

		let userId = null;
		if (checkIsAdmin(req)) {
			userId = req.params.userId;
		} else {
			userId = req.session.data.userId;
		}

		res.json(scoresDb.data[userId] || {});
	});

	api.get('/trials', (_req, res) => {
		res.json(trialsDb.data);
	});

	api.get('/trials/:disciplineId', (req, res) => {
		const trialsDiscipline = trialsDb.data[req.params.disciplineId];
		if (!trialsDiscipline) {
			res.jsonError(404, 'trialsDisciplineNotFound');
			return;
		}
		res.json(trialsDiscipline);
	});

	api.put('/trials/:disciplineId', (req, res) => {
		if (!checkIsAdmin(req)) {
			res.forbidden();
			return;
		}

		const oldDiscipline = trialsDb.data[req.params.disciplineId];
		const newOrEditedDiscipline = {
			...req.body,
			id: req.params.disciplineId,
		};
		if (oldDiscipline) {
			newOrEditedDiscipline.routes = oldDiscipline.routes;
		} else {
			newOrEditedDiscipline.routes = {};
		}
		trialsDb.data[req.params.disciplineId] = newOrEditedDiscipline;
		res.json({ ok: true });
		writeAndDiscardError(trialsDb);
	});

	api.put('/trials/:disciplineId/:routeId', (req, res) => {
		if (!checkIsAdmin(req)) {
			res.forbidden();
			return;
		}

		const trialsDiscipline = trialsDb.data[req.params.disciplineId];
		if (!trialsDiscipline) {
			res.jsonError(404, 'trialsDisciplineNotFound');
			return;
		}

		const newOrEditedRoute = {
			...req.body,
			id: req.params.routeId,
		};
		trialsDb.data[req.params.disciplineId].routes[req.params.routeId] = newOrEditedRoute;
		res.json({ ok: true });
		writeAndDiscardError(trialsDb);
	});

	api.delete('/trials/:disciplineId', (req, res) => {
		if (!checkIsAdmin(req)) {
			res.forbidden();
			return;
		}

		const discipline = trialsDb.data[req.params.disciplineId];
		if (!discipline) {
			res.jsonError(404, 'trialsDisciplineNotFound');
			return;
		}

		delete trialsDb.data[req.params.disciplineId];

		res.json({ ok: true });
		writeAndDiscardError(trialsDb);
	});

	api.delete('/trials/:disciplineId/:routeId', (req, res) => {
		if (!checkIsAdmin(req)) {
			res.forbidden();
			return;
		}

		const discipline = trialsDb.data[req.params.disciplineId];
		if (!discipline) {
			res.jsonError(404, 'trialsDisciplineNotFound');
			return;
		}

		delete trialsDb.data[req.params.disciplineId].routes[req.params.routeId];

		res.json({ ok: true });
		writeAndDiscardError(trialsDb);
	});

	api.get('/resultsGroups', (_req, res) => {
		res.json(resultsGroupsDb.data);
	});

	api.post('/resultsGroups', (req, res) => {
		if (!checkIsAdmin(req)) {
			res.forbidden();
			return;
		}

		if (!(req.body instanceof Array)) {
			res.jsonError(400, 'invalidDataType');
			return;
		}

		resultsGroupsDb.data = req.body;

		res.json({ ok: true });
		writeAndDiscardError(resultsGroupsDb);
	});

	api.post('/export/:uid', (req, res) => {
		if (!checkIsAdmin(req)) {
			res.forbidden();
			return;
		}

		const { name, data, mimeType } = req.body;

		const now = Date.now();
		scheduleExportsCleanup();

		exportsMemDB[req.params.uid] = {
			name,
			data,
			mimeType,
			expires: now + 60 * 1000,
		};

		res.json({ ok: true });
	});

	api.get('/export/:uid', (req, res) => {
		if (!checkIsAdmin(req)) {
			res.forbidden();
			return;
		}

		const toExport = exportsMemDB[req.params.uid];
		if (!toExport) {
			res.status(404).send('NOT FOUND!');
			return;
		}

		res.setHeader('Content-disposition', `attachment; filename=${toExport.name}`);
		res.setHeader('Content-type', toExport.mimeType);

		res.send(toExport.data);

		delete exportsMemDB[req.params.uid];
	});

	if (UNRELIABLE_RTC === 'true') {
		api.post('/rtc/set', (req, res) => {
			const { rtcTimestamp } = req.body;

			if (date.getDelta() === 0) {
				date.setNow(rtcTimestamp);
			}

			res.json({ ok: true });
		});
	}

	api.use((_req, res) => {
		res.jsonError(400, 'apiNotFound');
	});

	return api;
}

module.exports = apiLoader;
