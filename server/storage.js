'use strict';
/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */

const fs = require('fs');
const path = require('path');

const { DATA_DIR } = require('./env.js');

function Storage(type, name, ext, regen = null) {
	this._name = name;
	this._type = type;
	this._ext = ext;

	this._regen = regen;
	this._value = null;

	this._file = `${type}.${name}.${ext}`;
	this._filePath = path.join(DATA_DIR || path.join(__dirname, '..', 'data'), this._file);

	this._readOpts = {};
	this._writeOpts = {};

	if (type === 'secret') {
		this._writeOpts.mode = 0o600;
	}

	if (ext === 'txt') {
		this._readOpts.encoding = 'utf8';
		this._writeOpts.encoding = 'utf8';
	} else {
		this._readOpts.encoding = null;
		this._writeOpts.encoding = null;
	}

	if (this._regen) {
		this.read();
	}
}

Storage.prototype.read = async function read() {
	if (this._value !== null) {
		return this._value;
	}

	try {
		const data = await fs.promises.readFile(this._filePath, this._readOpts);
		this._value = data;
		return data;
	} catch (e) {
		if (e.code !== 'ENOENT') {
			throw e;
		}
	}

	if (!this._regen) {
		throw new Error(`File "data/${this._file}" not found and no generation function defined`);
	}

	const data = await this._regen();

	await fs.promises.writeFile(this._filePath, data, this._writeOpts);
	this._value = data;

	return this._value;
};

Storage.prototype.write = async function write(newValue) {
	this._value = newValue;

	await fs.promises.writeFile(this._filePath, newValue, this._writeOpts);
};

Storage.prototype.reset = async function write() {
	if (!this._regen) {
		throw new Error(`File "data/${this._file}" not found and no generation function defined`);
	}

	const data = await this._regen();

	await fs.promises.writeFile(this._filePath, data, this._writeOpts);
	this._value = data;

	return this._value;
};

const storages = {};

function storageGet(type, name, ext) {
	const key = JSON.stringify([type, name, ext]);

	if (!storages[key]) {
		storages[key] = new Storage(type, name, ext);
	}

	return storages[key];
}

function storageRegister(type, name, ext, regen) {
	const key = JSON.stringify([type, name, ext]);

	if (!storages[key]) {
		storages[key] = new Storage(type, name, ext, regen);
	} else {
		storages[key]._regen = regen;
		if (!storages[key]._value) {
			storages[key].read();
		}
	}

	return storages[key];
}

module.exports.Storage = Storage;
module.exports.storageGet = storageGet;
module.exports.storageRegister = storageRegister;
