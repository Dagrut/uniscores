'use strict';
/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */

const logLevels = {
	log: 4,
	info: 3,
	warn: 2,
	error: 1,
};

let currentLevel = logLevels.log;

function dateStr() {
	const now = new Date();
	return now.toISOString();
}

function Logger(namespace) {
	if (!(this instanceof Logger)) {
		return new Logger(namespace);
	}

	function logWith(levelName, ...args) {
		if (logLevels[levelName] <= currentLevel) {
			const logFunction = console[levelName] || console.log;
			logFunction(`${dateStr()}|${namespace}|${levelName} ${args.join(' ')}`);
		}
	}

	const logLevelsNames = Object.keys(logLevels);

	logLevelsNames.forEach((levelName) => {
		this[levelName] = logWith.bind(null, levelName);
	}, {});
}

function setLevel(newLevelName) {
	currentLevel = logLevels[newLevelName] || logLevels.log;
}

module.exports.Logger = Logger;
module.exports.setLevel = setLevel;
