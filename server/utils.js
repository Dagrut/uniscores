'use strict';
/* Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0, see the README.md file */

const crypto = require('crypto');

const serverUID = crypto.randomBytes(4).toString('hex');

function randomId(randomLength = 12) {
	const id = crypto.randomBytes(randomLength).toString('hex');
	const now = Date.now();

	return `${now}-${serverUID}-${id}`;
}

function randomHexStr(byteLength = 16) {
	const token = crypto.randomBytes(byteLength).toString('hex');
	return token;
}

// The only point of these fonctions is to prevent users from updating data in some exported files. They don't have to be cryptographically secure.
function quickSrvCipher(key, message) {
	const alphabet = '!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~';
	let idxKey = [];
	let ret = '';

	for (let i = 0; i < key.length; i++) {
		const c = key[i];
		const idx = alphabet.indexOf(c);
		if (idx >= 0) {
			idxKey.push(idx + 1);
		}
	}

	for (let i = 0; i < message.length; i++) {
		const c = message[i];
		const idx = alphabet.indexOf(c);
		if (idx >= 0) {
			const shift = idxKey[i % idxKey.length];
			const newC = alphabet[(idx + shift) % alphabet.length];
			ret += newC;
		} else {
			ret += c;
		}
	}

	return ret;
}

function quickSrvDecipher(key, message) {
	const alphabet = '!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~';
	let idxKey = [];
	let ret = '';

	for (let i = 0; i < key.length; i++) {
		const c = key[i];
		const idx = alphabet.indexOf(c);
		if (idx >= 0) {
			idxKey.push(idx + 1);
		}
	}

	for (let i = 0; i < message.length; i++) {
		const c = message[i];
		const idx = alphabet.indexOf(c);
		if (idx >= 0) {
			const shift = idxKey[i % idxKey.length];
			const newC = alphabet[(idx - shift + alphabet.length) % alphabet.length];
			ret += newC;
		} else {
			ret += c;
		}
	}

	return ret;
}

module.exports.randomId = randomId;
module.exports.randomHexStr = randomHexStr;
module.exports.quickSrvCipher = quickSrvCipher;
module.exports.quickSrvDecipher = quickSrvDecipher;
