# Uniscores

Universal Scores (shortened Uniscores) is a climbing competition scoring server with self-assessment, aimed mostly for friendly comps.

## Quickstart

This project requires at least nodejs v16 (tested up to v20).

Before starting, install the project dependencies with `npm i`.

### Development

- `npm run dev` Builds the client-side code and runs the server, all at once (requires bash)
- `npm run build-dev` Builds the client-side code to www

### Production

- `npm run build` Builds the client-side code for production
- `npm run start` Runs the server

### Windows

Execute `bin/run.bat`. It will download everything needed, including nodejs and then run the server. Close the terminal to exit the server.

### MacOS

Execute `bin/run.command`. It will download everything needed, including nodejs and then run the server. Close the terminal to exit the server.

## Structure

The project is split in two main parts :

- The `client` directory which contains all client-related files, and which is built using [vite](https://vitejs.dev/).
- The `server` directory which contains most server-related files and which runs using [express](http://expressjs.com/).

Data is stored in `data` (databases, keys, tokens...). The built client files are in `www`. These two directories do not exists until they are created by the proper tools. `public` contains public files for the client side (loaded by `vite`). The `bin` dir contains executables (nodejs or not). Currently it's mostly scripts to import and export translations between .json and .csv formats.

## License

Uniscores © 2023 by Maxime Ferrino is licensed under CC BY-SA 4.0.

The license text is available in the `LICENSE.txt` file.
